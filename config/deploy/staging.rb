set :branch, ENV.fetch("CAPISTRANO_BRANCH", "development")

set :mb_privileged_user, "deployer"

set :mb_sidekiq_concurrency, 1

server "deployer@staging-tipple-api.southeastasia.cloudapp.azure.com",
       :user => "deployer",
       :roles => %w(app db redis sidekiq web)
