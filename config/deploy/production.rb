set :branch, ENV.fetch("CAPISTRANO_BRANCH", "master")
set :mb_privileged_user, "deployer"
set :mb_sidekiq_concurrency, 1

server "deployer@tipple.southeastasia.cloudapp.azure.com",
       :user => "deployer",
       :roles => %w(app db redis sidekiq web)
