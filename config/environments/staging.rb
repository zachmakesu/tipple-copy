# Staging configuration is identical to production, with some overrides
# for hostname, etc.

require_relative "./production"

Rails.application.configure do

	config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    :address => "smtp.gmail.com",
    :port => 587,
    :user_name => ENV.fetch("GMAIL_USERNAME"),
    :password => ENV.fetch("GMAIL_PASSWORD"),
    :authentication => :plain,
    :domain => "gorated.com"
  }

  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options = {
    :host => "tippleapp.gorated.com"
  }

  config.action_mailer.asset_host = "https://tippleapp.gorated.com"
  config.action_controller.asset_host = "https://tippleapp.gorated.com"
end
