require 'capistrano-db-tasks'

set :application, "tipple"
set :repo_url, "git@bitbucket.org:gorated/tipple.git"

# Project-specific overrides go here.
# For list of variables that can be customized, see:
# https://github.com/mattbrictson/capistrano-mb/blob/master/lib/capistrano/tasks/defaults.rake

fetch(:mb_recipes) << "sidekiq"
fetch(:mb_aptitude_packages).merge!(
  "redis-server@ppa:rwky/redis" => :redis
)

set :mb_recipes, %w(
  user
  aptitude
  dotenv
  logrotate
  migrate
  seed
  nginx
  postgresql
  rake
  rbenv
  ufw
  unicorn
  version
)

set :linked_files, -> {
  [
    fetch(:mb_dotenv_filename)] +
  %w(
    config/google.p12
    config/database.yml
    config/unicorn.rb
    config/apn.pem
    config/apn_sandbox.pem
  )
}

set :mb_dotenv_keys, %w(
  rails_secret_key_base
  sidekiq_web_username
  sidekiq_web_password
  gmail_username
  gmail_password
  facebook_key
  facebook_secret
  facebook_scope
  hmac_secret
  pusher_app_id
  pusher_key
  pusher_secret
  pusher_cluster
  ios_iap_shared_secret
  fcm_key
  google_issuer
  google_key_file
  google_key_secret
)

set :db_local_clean, true
set :db_remote_clean, true
set :locals_rails_env, "production"
set :assets_dir, "public/assets"
