Rails.application.routes.draw do

  mount Sidekiq::Web => "/sidekiq" # monitoring console
  mount API::Base => '/'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  devise_for :users, :controllers => { 
    omniauth_callbacks: "users/omniauth_callbacks",
    registrations: 'registrations' 
  }

  authenticated :user do
     devise_scope :user do
        root "analytics#index"
        get 'users/user_list', to: 'users#user_list', as: 'user_list'
        resources :users
        post "/users/:id/availability", to: "users#availability", as: "update_availability_user"
        delete "/users/:id/photos/:photo_id", to: "users#delete_photo", as: "delete_user_photo"
        get "/ajax_users", to: "users#ajax_users", as: "ajax_users"
        get "users/:id/followed_places", to: "users#followed_places", as: "user_followed_places"


        resources :places
        post "/places/:id/availability", to: "places#availability", as: "update_availability_place"
        delete "/places/:id/photos/:photo_id", to: "places#delete_photo", as: "delete_place_photo"
        get "/ajax_places", to: "places#ajax_places", as: "ajax_places"

        resources :photos
        get "/analytics", to: "analytics#index"
        get "/ajax_analytics_place", to: "analytics#ajax_analytics_place", as: "ajax_analytics_place"
        get "/ajax_analytics_user", to: "analytics#ajax_analytics_user", as: "ajax_analytics_user"
        get "/ajax_analytics_places_followed/:id", to: "analytics#ajax_analytics_places_followed", as: "ajax_analytics_places_followed"

        get "/notifications/new", to: "notifications#new", as: "new_notification"
        get "/notifications", to: "notifications#index", as: "notifications"
        get "/notifications/:id", to: "notifications#show", as: "notification"
        get "/ajax_notifications", to: "notifications#ajax_notifications", as: "ajax_notifications"
        post "/notifications", to: "notifications#create"

     end
  end

  unauthenticated :user do
    devise_scope :user do
      root to: "devise/sessions#new", as: "login"
      get "/users/sign_up", to: "devise/sessions#new"
    end

    get '/faq', to: 'home#faq'
    get '/terms-of-service', to: 'home#terms'
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
