require 'exception_notification/rails'

ExceptionNotification.configure do |config|
  if Rails.env.production?
    config.add_notifier :email, {
      :email_prefix         => "[ERROR] TippleApi",
      :sender_address       => %{"Notifier" <notifier@tipple.com>},
      :exception_recipients => %w{caasi.mirabueno@gmail.com mcdave@gorated.ph ragde@gorated.ph kevinc@gorated.ph}
    }

    config.add_notifier :slack, {
      :webhook_url => "https://hooks.slack.com/services/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",
      :username => 'ExceptionBot',
      :channel => '#tipple',
      :additional_parameters => {
        :icon_emoji => ':rocket:'
      }
    }
  end
end
