# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( style.scss slick.css slick-theme.css jquery-ui.css jquery-ui.js slick.min.js foundation-datepicker.min.js foundation-datepicker.min.css datatables_foundation.css jquery.dataTables.min.js dataTables.foundation.min.js dataTables.buttons.min.js buttons.foundation.min.js)