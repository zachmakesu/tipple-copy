source "https://rubygems.org"

gem "active_type", ">= 0.3.2"
gem "autoprefixer-rails", ">= 5.0.0.1"
gem "bcrypt", "~> 3.1.7"
gem "coffee-rails", "~> 4.1.0"
gem "dotenv-rails", ">= 2.0.0"
gem "jquery-rails"
gem 'jquery-tokeninput-rails', '~> 1.6'
gem "mail", ">= 2.6.3"
gem "marco-polo"
gem "pg", "~> 0.18"
gem "rails", "4.2.1"
gem "redis-namespace"
gem "sass-rails", "~> 5.0"
gem "secure_headers", "~> 3.0"
gem "sidekiq"
gem "sinatra", ">= 1.3.0", :require => false
gem 'foreman', '~> 0.78.0'
gem "paperclip"
gem 'grape'
gem 'grape-entity'
gem 'devise'
gem "omniauth-facebook"
gem 'cancancan', '~> 1.10'
gem 'simple_form'
gem 'geocoder'
gem "koala"
gem 'exception_notification'
gem 'slack-notifier'
gem 'workflow'
gem 'grape_on_rails_routes'
gem 'rspec-rails'
gem 'ci_reporter_rspec'
gem 'faker'
gem 'foundation-rails'
gem 'kaminari' 
gem 'pusher'
gem 'houston'
gem 'unread'
gem 'venice'
gem 'fcm'
gem 'candy_check'

group :production, :staging do
  gem "unicorn"
  gem "unicorn-worker-killer"
end

group :development do
  gem "annotate", ">= 2.5.0"
  gem "awesome_print"
  gem "better_errors"
  gem "binding_of_caller"
  gem "letter_opener"
  gem "listen"
  gem "quiet_assets"
  gem "rack-livereload"
  gem "spring"
end

group :development do
  gem "airbrussh", "~> 1.0", :require => false
  gem "brakeman", :require => false
  gem "bundler-audit", :require => false
  gem "capistrano", "~> 3.4", :require => false
  gem "capistrano-bundler", :require => false
  gem "capistrano-mb", ">= 0.22.2", :require => false
  gem "capistrano-nc", :require => false
  gem "capistrano-rails", :require => false
  gem "capistrano-db-tasks", require: false
  gem "guard", ">= 2.2.2", :require => false
  gem "guard-livereload", :require => false
  gem "rb-fsevent", :require => false
  gem "simplecov", :require => false
  gem "sshkit", "~> 1.8", :require => false
  gem "terminal-notifier", :require => false
  gem "terminal-notifier-guard", :require => false
  gem "thin", :require => false
  gem 'guard-rspec', require: false
end

group :test do
  gem "vcr"
  gem "webmock"
  gem 'shoulda-matchers'
  gem "shoulda-context"
  # gem 'database_rewinder'
  gem 'database_cleaner'
  gem "capybara"
  gem "connection_pool"
  gem "launchy"
  gem "mocha"
  gem "poltergeist"
  gem "test_after_commit"
  gem "factory_girl_rails"
end

group :test, :development do
  gem "uniform_notifier"
  gem "bullet"
  gem "json-schema"
end
