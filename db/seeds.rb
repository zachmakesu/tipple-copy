['all', Rails.env].each do |seed|
  seed_file = "#{Rails.root}/db/seeds/#{seed}.rb"
  if File.exists?(seed_file)
    puts "### Loading seed data from #{seed}.rb"
    require seed_file
  end
end

puts '#' * 80
