# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161005104019) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.string   "activity_type"
    t.string   "source_type"
    t.integer  "source_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "activity_memberships", force: :cascade do |t|
    t.integer  "member_id"
    t.integer  "activity_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "activity_memberships", ["activity_id"], name: "index_activity_memberships_on_activity_id", using: :btree

  create_table "api_keys", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "encrypted_access_token", default: "", null: false
    t.datetime "expires_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "api_keys", ["user_id"], name: "index_api_keys_on_user_id", using: :btree

  create_table "block_lists", force: :cascade do |t|
    t.integer  "blockable_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "user_id"
    t.string   "blockable_type"
  end

  add_index "block_lists", ["user_id"], name: "index_block_lists_on_user_id", using: :btree

  create_table "check_ins", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "place_id"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.datetime "expires_at"
    t.integer  "random_likes"
    t.integer  "shared_likes", default: 0
  end

  add_index "check_ins", ["place_id"], name: "index_check_ins_on_place_id", using: :btree
  add_index "check_ins", ["user_id"], name: "index_check_ins_on_user_id", using: :btree

  create_table "conversation_memberships", force: :cascade do |t|
    t.integer  "member_id"
    t.integer  "conversation_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "conversation_memberships", ["conversation_id"], name: "index_conversation_memberships_on_conversation_id", using: :btree

  create_table "conversations", force: :cascade do |t|
    t.integer  "interaction_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "conversations", ["interaction_id"], name: "index_conversations_on_interaction_id", using: :btree

  create_table "current_check_ins", force: :cascade do |t|
    t.integer  "places_activity_id"
    t.integer  "people_count"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "devices", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "platform"
    t.string   "token"
    t.string   "udid"
    t.boolean  "is_enabled", default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "follows", force: :cascade do |t|
    t.integer  "place_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "follows", ["place_id"], name: "index_follows_on_place_id", using: :btree
  add_index "follows", ["user_id"], name: "index_follows_on_user_id", using: :btree

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "interaction_histories", force: :cascade do |t|
    t.integer  "interaction_id"
    t.string   "state_from"
    t.string   "state_changed_to"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.boolean  "has_unlimited",    default: false
  end

  add_index "interaction_histories", ["interaction_id"], name: "index_interaction_histories_on_interaction_id", using: :btree

  create_table "interactions", force: :cascade do |t|
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "workflow_state"
    t.integer  "initiator_id"
    t.integer  "initiatee_id"
    t.integer  "place_id",                      null: false
    t.boolean  "active",         default: true
  end

  add_index "interactions", ["place_id"], name: "index_interactions_on_place_id", using: :btree

  create_table "manual_notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.text     "message",                null: false
    t.integer  "send_to",    default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "manual_notifications", ["user_id"], name: "index_manual_notifications_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "conversation_id"
    t.integer  "sender_id"
    t.text     "body"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree

  create_table "notification_recipients", force: :cascade do |t|
    t.integer  "manual_notification_id"
    t.integer  "recipient_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "notification_recipients", ["manual_notification_id"], name: "index_notification_recipients_on_manual_notification_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.string   "caption"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.integer  "position"
    t.boolean  "default",            default: false
    t.integer  "default_type"
  end

  create_table "places", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.float    "lat"
    t.float    "lng"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "deleted_at"
    t.string   "description"
  end

  create_table "product_transactions", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.datetime "expires_at"
    t.integer  "product_id"
    t.string   "transaction_id"
    t.json     "receipt"
  end

  add_index "product_transactions", ["product_id"], name: "index_product_transactions_on_product_id", using: :btree
  add_index "product_transactions", ["transaction_id"], name: "index_product_transactions_on_transaction_id", using: :btree
  add_index "product_transactions", ["user_id"], name: "index_product_transactions_on_user_id", using: :btree

  create_table "products", force: :cascade do |t|
    t.integer  "ability"
    t.text     "description"
    t.integer  "duration"
    t.datetime "expires_at"
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.string   "name"
    t.float    "price"
    t.datetime "published_at"
    t.integer  "value"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "read_marks", force: :cascade do |t|
    t.integer  "readable_id"
    t.string   "readable_type", null: false
    t.integer  "reader_id"
    t.string   "reader_type",   null: false
    t.datetime "timestamp"
  end

  add_index "read_marks", ["reader_id", "reader_type", "readable_type", "readable_id"], name: "read_marks_reader_readable_index", unique: true, using: :btree

  create_table "tipple_settings", force: :cascade do |t|
    t.text     "faq"
    t.text     "general_conditions"
    t.text     "privacy_policy"
    t.string   "contact_mail"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                           default: "",    null: false
    t.string   "encrypted_password",              default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "uid",                             default: "",    null: false
    t.integer  "gender",                          default: 0,     null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "birthdate"
    t.integer  "role",                            default: 0,     null: false
    t.boolean  "help",                            default: false
    t.boolean  "prefers_women",                   default: false
    t.boolean  "prefers_men",                     default: false
    t.boolean  "notify_activity",                 default: true
    t.boolean  "notify_cheers",                   default: true
    t.boolean  "notify_messages",                 default: true
    t.datetime "hide_profile_at"
    t.boolean  "hide_profile_with_zero_check_in", default: false
    t.datetime "deleted_at"
    t.string   "image_url"
    t.string   "facebook_url"
    t.boolean  "notify_drinks",                   default: true
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "activity_memberships", "activities"
  add_foreign_key "api_keys", "users"
  add_foreign_key "check_ins", "places"
  add_foreign_key "check_ins", "users"
  add_foreign_key "conversation_memberships", "conversations"
  add_foreign_key "conversations", "interactions"
  add_foreign_key "devices", "users"
  add_foreign_key "follows", "places"
  add_foreign_key "follows", "users"
  add_foreign_key "identities", "users"
  add_foreign_key "interaction_histories", "interactions"
  add_foreign_key "manual_notifications", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "notification_recipients", "manual_notifications"
  add_foreign_key "product_transactions", "products"
  add_foreign_key "product_transactions", "users"
end
