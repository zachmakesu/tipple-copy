def seed_image(file)
  File.open(File.join(Rails.root, "/db/seeds/images/#{file}"))
end


place_list = [
  {
    name:       "KULI ALMA",
    address:    "Mikveh Israel St 10, Tel Aviv-Yafo, Israël",
    background: seed_image("kuli-alma.jpg"),
    lat:        32.062673,
    lng:        34.775084
  },
  {
    name:       "CLARA",
    address:    "מתחם הדולפינריום, Tel Aviv-Yafo, Israël",
    background: seed_image("clara.jpg"),
    lat:        32.067715,
    lng:        34.761858
  },
  {
    name:       "BUXA",
    address:    "Rothschild Blvd 31, Tel Aviv-Yafo, 6688301, Israël",
    background: seed_image("buxa.jpg"),
    lat:        32.063984,
    lng:        34.773118
  },
  {
    name:       "Skyrocket Studios",
    address:    "Building 9 Southway Office and Warehouse Bakawan St, Makati",
    background: seed_image("skyrocket-office.png"),
    lat:        14.560125,
    lng:        121.007266
  },
  {
    name:       "Lazy Bastard",
    address:    "22 Jupiter St. Corner Galaxy, Makati, 1209 Metro Manila",
    background: seed_image("lazybastard.jpg"),
    lat:        14.558302,
    lng:        121.033553
  },
  {
    name:       "Moonshine Bar",
    address:    "Unit 1C, Valdelcon Building, Jupiter Street, Makati, 1209 Metro Manila",
    background: seed_image("moonshine.jpg"),
    lat:        14.557909,
    lng:        121.033809
  },
  {
    name:       "Writers Bar",
    address:    "1 Raffles Drive Makati Avenue at, Antonio Arnaiz Ave, Makati, 1224 Metro Manila",
    background: seed_image("writers_bar.jpg"),
    lat:        14.550744,
    lng:        121.022683
  },
  {
    name:       "The Exit",
    address:    "Ground Floor Corinthian Plaza, 121, Paseo De Roxas, Legaspi Village, Makati, 1229 Metro Manila",
    background: seed_image("the_exit.jpg"),
    lat:        14.553610,
    lng:        121.019325
  },
  {
    name:       "Rouges Sports Bar",
    address:    "Anza Street Corner, P Burgos, Makati, Metro Manila",
    background: seed_image("rouges.jpg"),
    lat:        14.562852,
    lng:        121.029088
  },
  {
    name:       "KYSS Restaurant and Lounge Bar",
    address:    "5343 General Luna Street corner Makati Avenue, Makati, Metro Manila",
    background: seed_image("kyss.jpg"),
    lat:        14.565417,
    lng:        121.029142
  },
  {
    name:       "Blue Onion Bar and Restaurant",
    address:    "Eastwood City Walk, E. Rodiguez Avenue, Bagumbayan, Libis, Quezon City, 1110, Metro Manila, Bagumbayan, Quezon City, Metro Manila",
    lat:        14.607587,
    lng:        121.080147
  },
  {
    name:       "The Distillery",
    address:    "Unit 61 A, Eastwood City Walk 1, Bagumbayan, Quezon City, 1110 Metro Manila",
    background: seed_image("the_distillery.jpg"),
    lat:        14.608866,
    lng:        121.081005
  },
  {
    name:       "Kuwagos Island Grill",
    address:    "Citywalk 1, Eastwood, Bagumbayan, Quezon City, 1110 Metro Manila",
    background: seed_image("kuwagos.jpg"),
    lat:        14.6089998,
    lng:        121.080385
  },
  {
    name:     "OWLS by Book and Borders Cafe",
    address:  "Ground Floor, Olympic Heights Condominium, Eastwood Mall, 10 Orchard Rd, Bagumbayan, Quezon City, 1110 Metro Manila",
    lat:      14.611186,
    lng:      121.079618
  },
  {
    name:     "Grilla Bar and Grill",
    address:  "L6 Bl13 E Rodriguez Avenue Acropolis Subdivisionlibis, Metro Manila",
    lat:      14.609439,
    lng:      121.076617
  },
  {
    name:       "Astralis",
    address:    "Level 2, 117 Lonsdale St.",
    background: URI.parse("https://www.residentadvisor.net/photos/2012/us120826roofto/bymarvin8of53.jpg"),
    lat:        14.57984,
    lng:        121.1169
  },
  {
    name:       "Ninja",
    address:    "Level 10, 117 Lonsdale St.",
    background: URI.parse("http://www.skylinebarvenice.com/public/crop/pool_party_at_skyline_rooftop_bar_1000x0.jpg"),
    lat:        14.57982,
    lng:        121.1165
  },
  {
    name:       "Wanderlust",
    address:    "Thong Lo 13 Alley, Khlong Tan Nuea, Watthana",
    background: URI.parse("https://i.imgur.com/ra0Z50j.png"),
    lat:        14.602984,
    lng:        121.092033
  },
  {
    name:       "TaTaTa",
    address:    "#{Faker::Address.street_address} #{Faker::Address.city}",
    background: URI.parse("https://i.imgur.com/JU27AEw.jpg"),
    lat:        14.602946,
    lng:        121.092133
  },
  {
    name:       "Gorated",
    address:    "Unit 20, Ipark Center, 401 Eulogio Amang Rodriguez Ave",
    background: URI.parse("https://i.imgur.com/DtMkd7J.png"),
    lat:        14.602934,
    lng:        121.092133
  },
  {
    name:       "Wanderful",
    address:    "#{Faker::Address.street_address} #{Faker::Address.city}",
    background: URI.parse("https://i.imgur.com/ra0Z50j.png"),
    lat:        14.602994,
    lng:        121.092043
  },
  {
    name:       "Showcase",
    address:    "5015 Turney Rd, Garfield Heights",
    background: URI.parse("https://i.imgur.com/NW0M06R.png"),
    lat:        14.602954,
    lng:        121.092113
  },
  {
    name:       "Showtime",
    address:    "#{Faker::Address.street_address} #{Faker::Address.city}",
    background: URI.parse("https://i.imgur.com/JU27AEw.jpg"),
    lat:        14.602944,
    lng:        121.092123
  },
  {
    name:       "YoYo",
    address:    "Peterborough, United Kingdom",
    background: URI.parse("https://i.imgur.com/JU27AEw.jpg"),
    lat:        14.603954,
    lng:        121.094113
  },
  {
    name:       "Fnatic",
    address:    "#{Faker::Address.street_address} #{Faker::Address.city}",
    background: URI.parse("https://i.imgur.com/JU27AEw.jpg"),
    lat:        14.602914,
    lng:        121.092125
  },
  {
    name:       "Luminosity",
    address:    "Level 2, 117 Lonsdale St.",
    background: URI.parse("https://i.imgur.com/eORvsod.jpg"),
    lat:        14.601954,
    lng:        121.092213
  },
  {
    name:       "ANNA LOULOU",
    address:    "HaPninim 2, Tel Aviv-Yafo, 6803001",
    background: seed_image("ana loulou 1.jpg"),
    lat:        32.053474,
    lng:        34.753825
  },
  {
    name:       "SHALVATA",
    address:    "Hangar 28 , Tel Aviv Port",
    background: seed_image("Shalvata-club-tel-aviv.jpg"),
    lat:        32.101357,
    lng:        34.775089
  },
  {
    name:       "THE CAT & DOG",
    address:    "Carlebach St 23, Tel Aviv-Yafo",
    background: seed_image("THE CAT & DOG.jpg"),
    lat:        32.068870,
    lng:        34.782860
  },
  {
    name:       "Cofix Lilienblum",
    address:    "Lilienblum St 21, Tel Aviv-Yafo",
    background: seed_image("cofix.jpg"),
    lat:        32.062287,
    lng:        34.769813
  },
  {
    name:       "TANGIER",
    address:    "Yehuda ha-Levi St 93, Tel Aviv-Yafo",
    background: seed_image("TANGIER.jpg"),
    lat:        32.065349,
    lng:        34.777890
  },
]

ActiveRecord::Base.transaction do
  if Place.all.empty?
    place_list.each do |place|
      new_place = Place.new
      new_place.name = place[:name]
      new_place.address = place[:address]
      new_place.lat = place[:lat]
      new_place.lng = place[:lng]
      if new_place.save
        new_place.photos.create(image: place[:background], position: 0)
        print '✓'
      else
        puts new_place.errors.inspect
        break
      end
    end
    print "\nTotal : #{Place.all.count}\n"
  else
    print "Skipped seeding places table.\n"
  end
end
