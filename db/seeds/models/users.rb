user_list = [
  {
    gender: 1,
    prefers_men: false,
    prefers_women: true,
    avatar: 'https://i.imgur.com/EZVlugw.jpg'
  },
  {
    gender: 1,
    prefers_men: true,
    prefers_women: false,
    avatar: 'https://i.imgur.com/nh3FDXk.jpg'
  },
  {
    gender: 1,
    prefers_men: true,
    prefers_women: true,
    avatar: 'https://i.imgur.com/RZANzpd.jpg'
  },
  {
    gender: 2,
    prefers_men: false,
    prefers_women: true,
    avatar: 'https://i.imgur.com/oBiRoST.jpg'
  },
  {
    gender: 2,
    prefers_men: true,
    prefers_women: false,
    avatar: 'https://i.imgur.com/Bnjkcn0.jpg'
  },
  {
    gender: 2,
    prefers_men: true,
    prefers_women: true,
    avatar: 'https://i.imgur.com/fIhs2HG.jpg'
  },
  {
    gender: 2,
    prefers_men: true,
    prefers_women: false,
    avatar: 'https://i.imgur.com/sCaggrp.png'
  },
  {
    gender: 2,
    prefers_men: false,
    prefers_women: true,
    avatar: 'https://i.imgur.com/xmPS6y2.png'
  },
  {
    gender: 2,
    prefers_men: true,
    prefers_women: true,
    avatar: 'https://i.imgur.com/EumNdFI.png'
  }
]

ActiveRecord::Base.transaction do
  if User.all.empty?
    user_list.each do |user|
      new_user = User.new
      new_user.email          = Faker::Internet.email
      new_user.first_name     = Faker::Name.first_name
      new_user.last_name      = Faker::Name.last_name
      new_user.gender         = user[:gender]
      new_user.prefers_men    = user[:prefers_men]
      new_user.prefers_women  = user[:prefers_women]
      new_user.password       = 'password'
      new_user.role           = 1
      if new_user.save
        new_user.photos.create(image: URI.parse(user[:avatar]), position: 0)
        print '✓'
      else
        puts new_user.errors.inspect
        break
      end
    end
    print "\nTotal : #{User.all.count}\n"
  else
    print "Skipped seeding users table.\n"
  end
end
