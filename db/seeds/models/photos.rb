def seed_image(file)
  File.open(File.join(Rails.root, "/db/seeds/images/#{file}"))
end
photo_list = [
  {
    image: seed_image("Men.png"),
    default: true,
    default_type: 0,
    position: 0
  },
  {
    image: seed_image("Women.png"),
    default: true,
    default_type: 1,
    position: 1
  },
  {
    image: seed_image("Places.png"),
    default: true,
    default_type: 2,
    position: 0
  }
]

ActiveRecord::Base.transaction do
  if Photo.all.empty?
    photo_list.each do |photo|
      new_photo = Photo.new
      new_photo.image = photo[:image]
      new_photo.default = true
      new_photo.default_type = photo[:default_type]
      new_photo.position = photo[:position]
      if !new_photo.save
        puts new_photo.errors.inspect
        break
      end
    end
    print "\nTotal : #{Photo.all.count}\n"
  else
    print "Skipped seeding photos table.\n"
  end
end
