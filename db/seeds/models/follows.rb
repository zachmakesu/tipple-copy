ActiveRecord::Base.transaction do
  if Follow.all.empty?
    all_users = User.all
    Place.all.each do |place|
      if place.followers << all_users
        all_users.length.times { print '✓' }
      else
        puts "Error pushing all users to place(#{place}) followers"
        break
      end
    end
    print "\nTotal : #{Follow.all.count}\n"
  else
    print "Skipped seeding follows table.\n"
  end
end
