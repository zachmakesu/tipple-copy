product_list = [
  {
    published_at: DateTime.now - 2.hours,
    duration: 0,
    value: 5,
    ability: 1,
    description: "Like tipple on facebook"
  },
  {
    published_at: DateTime.now - 3.hours,
    duration: 0,
    value: 10,
    ability: 1,
    description: "Share tipple on facebook"
  },
  {
    published_at: DateTime.now - 4.hours,
    duration: 0,
    value: 15,
    ability: 1,
    description: "Follow tipple on twitter"
  },
  {
    published_at: DateTime.now - 5.hours,
    duration: 0,
    value: 5,
    ability: 1,
    description: "Tweet about tipple"
  },
  {
    published_at: DateTime.now - 6.hours,
    duration: 0,
    value: 5,
    ability: 1,
    description: "Follow tipple on instagram"
  },
  {
    published_at: DateTime.now - 7.hours,
    duration: 0,
    value: 10,
    ability: 1,
    description: "Pin tipple on pinterest"
  },
  {
    published_at: DateTime.now - 8.hours,
    duration: 0,
    value: 10,
    price: 1.99,
    ability: 0, name: "Baby Tippler"
  },

  {
    published_at: DateTime.now - 9.hours,
    duration: 0,
    value: 50,
    price: 9.99,
    ability: 0, name: "Tippler"
  },

  {
    published_at: DateTime.now - 10.hours,
    duration: 0,
    value: 100,
    price: 19.99,
    ability: 0, name: "Heavy Tippler"
  },
  {
    published_at: DateTime.now - 11.hours,
    duration: 3,
    value: 0,
    price: 15.99,
    ability: 0, name: "Tipple addict"
  }
]


ActiveRecord::Base.transaction do
  if Product.all.empty?
    product_list.each do |product|
      new_product = Product.new(product)
      if new_product.save
        print '✓'
      else
        puts new_product.errors.inspect
        break
      end
    end
    print "\nTotal : #{Product.all.count}\n"
  else
    print "Skipped seeding products table.\n"
  end
end
