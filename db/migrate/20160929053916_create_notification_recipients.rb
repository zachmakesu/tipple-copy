class CreateNotificationRecipients < ActiveRecord::Migration
  def change
    create_table :notification_recipients do |t|
      t.references :manual_notification, index: true, foreign_key: true
      t.integer    :recipient_id
      t.timestamps null: false
    end
  end
end
