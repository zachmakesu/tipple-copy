class RenameLikeEntriesToTransactions < ActiveRecord::Migration
  def change
    rename_table :like_entries, :product_transactions
  end
end
