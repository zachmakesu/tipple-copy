class RemoveEarnLikeIdAndAddProductIdInLikeEntry < ActiveRecord::Migration
  def change
  	add_reference :like_entries, :product, index: true, foreign_key: true
  end
end
