class AddSenderToInteractionHistory < ActiveRecord::Migration
  def change
    add_column :interaction_histories, :sender_id,    :integer, index: true, foreign_key: true
    add_column :interaction_histories, :recipient_id, :integer, index: true, foreign_key: true
  end
end
