class AddRandomLikesAndSharedLikesOnCheckIns < ActiveRecord::Migration
  def change
    add_column :check_ins, :random_likes, :integer
    add_column :check_ins, :shared_likes, :integer, default: 0
  end
end
