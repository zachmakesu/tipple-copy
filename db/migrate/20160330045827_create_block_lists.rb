class CreateBlockLists < ActiveRecord::Migration
  def change
    create_table :block_lists do |t|
      t.references :user_setting, index: true, foreign_key: true
      t.integer :block_id

      t.timestamps null: false
    end
  end
end
