class Change < ActiveRecord::Migration
  def change
  	rename_column :interactions, :type, :interact_type
  end
end
