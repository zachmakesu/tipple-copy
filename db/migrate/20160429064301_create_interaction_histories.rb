class CreateInteractionHistories < ActiveRecord::Migration
  def change
    create_table :interaction_histories do |t|
      t.references :interaction, index: true, foreign_key: true
      t.references :place, index: true, foreign_key: true
      t.string :state_from
      t.string :state_changed_to

      t.timestamps null: false
    end
  end
end
