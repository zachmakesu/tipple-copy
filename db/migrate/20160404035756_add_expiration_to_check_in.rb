class AddExpirationToCheckIn < ActiveRecord::Migration
  def change
    add_column :check_ins, :expires_at, :datetime
  end
end
