class RemoveAcquireTypeFromTransactions < ActiveRecord::Migration
  def up
    remove_column :product_transactions, :acquire_type
  end

  def down
    add_column :product_transactions, :acquire_type, :integer
  end
end
