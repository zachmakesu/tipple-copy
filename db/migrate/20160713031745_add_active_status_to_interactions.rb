class AddActiveStatusToInteractions < ActiveRecord::Migration
  def change
    add_column :interactions, :active, :bool, default: true
  end
end
