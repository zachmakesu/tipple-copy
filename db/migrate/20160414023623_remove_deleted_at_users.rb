class RemoveDeletedAtUsers < ActiveRecord::Migration
  def change
  	remove_column :users, :deleted_at, :date
  end
end
