class CreateManualNotifications < ActiveRecord::Migration
  def change
    create_table :manual_notifications do |t|
      t.references :user, index: true, foreign_key: true
      t.text     :message, null: false
      t.integer  :send_to, null: false, default: 0
      t.timestamps null: false
    end
  end
end
