class AddReceiptInfoToProductTransactions < ActiveRecord::Migration
  def change
    add_column :product_transactions, :transaction_id, :string
    add_column :product_transactions, :receipt, :json
    add_index :product_transactions, :transaction_id
  end
end
