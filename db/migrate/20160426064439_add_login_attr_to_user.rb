class AddLoginAttrToUser < ActiveRecord::Migration
  def change
    add_column :users, :image_url, :string
    add_column :users, :provider, :string
    add_column :users, :facebook_url, :string
    add_column :users, :token, :string
  end
end
