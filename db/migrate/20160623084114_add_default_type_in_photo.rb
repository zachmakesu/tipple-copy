class AddDefaultTypeInPhoto < ActiveRecord::Migration
  def change
  	add_column :photos, :default_type, :integer, default: nil
  end
end
