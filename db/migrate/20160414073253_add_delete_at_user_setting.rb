class AddDeleteAtUserSetting < ActiveRecord::Migration
  def change
  	add_column :user_settings, :deleted_at, :datetime
  end
end
