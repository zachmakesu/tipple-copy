class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string    :activity_type
      t.string    :source_type
      t.integer   :source_id
      t.timestamps null: false
    end
  end
end
