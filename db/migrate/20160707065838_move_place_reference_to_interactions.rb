class MovePlaceReferenceToInteractions < ActiveRecord::Migration
  def up
    remove_column :interaction_histories, :place_id
    add_column :interactions, :place_id, :int, null: false
    add_index :interactions, :place_id
  end

  def down
    remove_column :interactions, :place_id
    add_column :interaction_histories, :place_id, :int, null: false
    add_index :interaction_histories, :place_id
  end
end
