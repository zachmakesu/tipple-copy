class CreateTippleSettings < ActiveRecord::Migration
  def change
    create_table :tipple_settings do |t|
      t.text :faq
      t.text :general_conditions
      t.text :privacy_policy
      t.string :contact_mail

      t.timestamps null: false
    end
  end
end
