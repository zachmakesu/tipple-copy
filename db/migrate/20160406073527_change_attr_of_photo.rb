class ChangeAttrOfPhoto < ActiveRecord::Migration
  def change
  	remove_column :photos, :user_id,				:integer
  	add_column		:photos, :imageable_id, 	:integer
  	add_column		:photos, :imageable_type, :string
  end
end
