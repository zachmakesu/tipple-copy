class RenameColumnBlockLists < ActiveRecord::Migration
  def change
  	rename_column :block_lists, :blocked_user_id , :blocked_profile_id
  end
end
