class CreateInteractions < ActiveRecord::Migration
  def change
    create_table :interactions do |t|
      t.references :check_in, index: true, foreign_key: true
      t.integer :recipient_id
      t.string :type
      t.date :interacted_date

      t.timestamps null: false
    end
  end
end
