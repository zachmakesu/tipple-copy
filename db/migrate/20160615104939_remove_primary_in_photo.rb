class RemovePrimaryInPhoto < ActiveRecord::Migration
  def change
  	remove_column :photos, :primary, :boolean
  end
end
