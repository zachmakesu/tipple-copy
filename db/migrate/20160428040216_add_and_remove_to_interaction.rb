class AddAndRemoveToInteraction < ActiveRecord::Migration
  def change
    add_column :interactions, :workflow_state, :string
    add_column :interactions, :initiator_id, :integer, index: true, foreign_key: true
    add_column :interactions, :initiatee_id, :integer, index: true, foreign_key: true
    remove_column :interactions, :recipient_id, :integer
    remove_column :interactions, :interact_type, :string
    remove_column :interactions, :interacted_date, :datetime
    remove_reference :interactions, :check_in, index: true, foreign_key: true
  end
end
 