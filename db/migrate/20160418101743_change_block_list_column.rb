class ChangeBlockListColumn < ActiveRecord::Migration
  def change
  	rename_column :block_lists, :block_id, :blocked_user_id
  end
end
