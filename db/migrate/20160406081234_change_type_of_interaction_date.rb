class ChangeTypeOfInteractionDate < ActiveRecord::Migration
  def change
  	change_column :interactions, :interacted_date, :datetime
  end
end
