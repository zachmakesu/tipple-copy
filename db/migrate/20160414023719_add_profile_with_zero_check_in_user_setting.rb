class AddProfileWithZeroCheckInUserSetting < ActiveRecord::Migration
  def change
  	add_column :user_settings, :hide_profile_with_zero_check_in, :datetime
  end
end
