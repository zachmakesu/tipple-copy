class AddAttrToUser < ActiveRecord::Migration
  def change
    add_column :users, :uid, :string
    add_column :users, :gender, :integer, default: 0, null: false
    add_attachment :users, :avatar
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :birthdate, :string
    add_column :users, :role, :integer, default: 0, null: false
    add_column :users, :deleted_at, :date
  end
end
