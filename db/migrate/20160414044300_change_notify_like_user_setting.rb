class ChangeNotifyLikeUserSetting < ActiveRecord::Migration
  def change
  	rename_column :user_settings, :notify_like, :notify_likes
  end
end
