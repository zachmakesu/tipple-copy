class ChangeNotifyLikesToNotifyDrinksOnUsers < ActiveRecord::Migration
  def up
    add_column :users, :notify_drinks, :bool, default: false
    remove_column :users, :notify_likes
  end

  def down
    add_column :users, :notify_likes, :bool, default: false
    remove_column :users, :notify_drinks
  end
end
