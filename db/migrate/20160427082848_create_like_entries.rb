class CreateLikeEntries < ActiveRecord::Migration
  def change
    create_table :like_entries do |t|
      t.integer   :acquire_type

      t.references :user, index: true, foreign_key: true
      
      t.timestamps null: false
    end
  end
end
