class AddPositionInPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :position, :integer, default: nil
  end
end
