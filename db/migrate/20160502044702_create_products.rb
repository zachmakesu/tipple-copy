class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer       :ability
      t.text          :description
      t.integer       :duration
      t.datetime      :expires_at
      t.attachment    :icon
      t.string        :name
      t.float         :price
      t.datetime      :published_at
      t.integer       :value

      t.timestamps null: false
    end
  end
end
