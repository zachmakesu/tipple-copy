class AddIndecesOnBlockedLists < ActiveRecord::Migration
  def change
  	add_index :block_lists, :user_id
    add_index :block_lists, :blocked_user_id
    add_index :block_lists, [:user_id, :blocked_user_id], unique: true
  end
end
