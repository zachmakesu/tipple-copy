class CreateUserSettings < ActiveRecord::Migration
  def change
    create_table :user_settings do |t|
      t.references :user, index: true, foreign_key: true
      t.boolean :preferred_men, default: false
      t.boolean :preferred_women, default: false
      t.boolean :notify_like, default: false
      t.boolean :notify_cheers, default: false
      t.boolean :notify_messages, default: false
      t.boolean :notify_activity, default: false
      t.boolean :help, default: false
      t.time :hide

      t.timestamps null: false
    end
  end
end
