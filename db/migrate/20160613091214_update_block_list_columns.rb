class UpdateBlockListColumns < ActiveRecord::Migration
  def change
    remove_index  :block_lists, column: :blocked_profile_id
    remove_index  :block_lists, column: [:user_id, :blocked_profile_id], unique: true
    rename_column :block_lists, :blocked_profile_id, :blockable_id
    add_column    :block_lists, :blockable_type, :string
  end
end
