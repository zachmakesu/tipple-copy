class RemovePhotosBackgroundAndAddPrimary < ActiveRecord::Migration
  def change
    remove_attachment :users,   :avatar
    remove_attachment :places,  :background
    add_column        :photos,  :primary, :boolean, default: false
  end
end
