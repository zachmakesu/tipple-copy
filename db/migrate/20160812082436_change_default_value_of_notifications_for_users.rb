class ChangeDefaultValueOfNotificationsForUsers < ActiveRecord::Migration
  def change
  change_column :users, :notify_activity, :boolean, default: true
  change_column :users, :notify_cheers, :boolean, default: true
  change_column :users, :notify_drinks, :boolean, default: true
  change_column :users, :notify_messages, :boolean, default: true
  end
end
