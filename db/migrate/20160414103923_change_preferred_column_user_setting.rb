class ChangePreferredColumnUserSetting < ActiveRecord::Migration
  def change
		rename_column :user_settings, :preferred_men, :prefers_men
		rename_column :user_settings, :preferred_women, :prefers_women
  end
end
