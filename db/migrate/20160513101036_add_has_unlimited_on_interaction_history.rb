class AddHasUnlimitedOnInteractionHistory < ActiveRecord::Migration
  def change
  	add_column :interaction_histories, :has_unlimited, :boolean, default: false
  end
end
