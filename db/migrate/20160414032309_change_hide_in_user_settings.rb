class ChangeHideInUserSettings < ActiveRecord::Migration
  def change
  	remove_column :user_settings, :hide, :time
  	add_column :user_settings, :hide, :datetime
  	remove_column :user_settings, :hide_profile_with_zero_check_in, :datetime
  	add_column :user_settings, :hide_profile_with_zero_check_in, :boolean, default: false
  end
end
