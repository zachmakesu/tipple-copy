class CreateConversationMemberships < ActiveRecord::Migration
  def change
    create_table :conversation_memberships do |t|
      t.integer :member_id
      t.references :conversation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
