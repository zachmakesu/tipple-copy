class PutUserSettingToUser < ActiveRecord::Migration
  def change
  	add_column :users, :help, :boolean, default: false
  	add_column :users, :prefers_women, :boolean, default: false
  	add_column :users, :prefers_men, :boolean, default: false
  	add_column :users, :notify_activity, :boolean, default: false
  	add_column :users, :notify_cheers, :boolean, default: false
  	add_column :users, :notify_likes, :boolean, default: false
  	add_column :users, :notify_messages, :boolean, default: false
  	add_column :users, :hide_profile_at, :datetime
  	add_column :users, :hide_profile_with_zero_check_in, :boolean, default: false

  	add_column :block_lists, :user_id, :integer
  	remove_column :block_lists, :user_setting_id, :integer

  	drop_table :user_settings

  end
end
