class ChangeHideUserSettings < ActiveRecord::Migration
  def change
  	rename_column :user_settings, :hide, :hide_profile_24hrs
  end
end
