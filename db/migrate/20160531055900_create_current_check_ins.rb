class CreateCurrentCheckIns < ActiveRecord::Migration
  def change
    create_table :current_check_ins do |t|
      t.integer :places_activity_id
      t.integer :people_count

      t.timestamps null: false
    end
  end
end
