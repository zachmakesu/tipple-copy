class CreateActivityMemberships < ActiveRecord::Migration
  def change
    create_table :activity_memberships do |t|
      t.integer :member_id
      t.references :activity, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
