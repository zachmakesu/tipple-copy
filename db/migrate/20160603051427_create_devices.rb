class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.string :platform
      t.string :token
      t.string :udid
      t.boolean :is_enabled, default: true

      t.timestamps null: false
    end
  end
end
