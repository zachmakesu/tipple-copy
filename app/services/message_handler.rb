class MessageHandler

  attr_accessor :conversation,:sender,:body

  def initialize(conversation_id,sender,body)
    @conversation = Conversation.find(conversation_id)
    @sender       = sender
    @body         = body
  end

  def create
    return nil if body.blank?
    @conversation = conversation.messages.create(message_options)
  end

  private
  def message_options
    {
      sender: sender,
      body: body
    }
  end
end
