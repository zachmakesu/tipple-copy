class UsersDatatableHandler 
  delegate :params, :link_to, :user_path, :edit_user_path, :update_availability_user_path, to: :@view 

  def initialize(view) 
    @view = view 
  end 

  def as_json(options = {}) 
    { 
      data: data, 
      recordsTotal: my_search.count, 
      recordsFiltered: sort_order_filter.count 
    } 
  end 

private 

  def data 
    users = [] 
    display_on_page.map do |record| 
      user = [] 
      user << record.id
      user << (link_to record.email, user_path(record), data: {remote: true})
      user << (link_to record.first_name, user_path(record), data: {remote: true})
      user << (link_to record.last_name, user_path(record), data: {remote: true})
      user << record.gender
      user << record.age
      user << record.role 

      user_avatar = if record.avatar.present? 
        record.avatar.url(:large)
      else
        record.male? ? Photo.default_photo_men.image.url(:large) : Photo.default_photo_women.image.url(:large)
      end 
      
      user << '<div style="background-image: url('+user_avatar+'); background-position: center center; background-repeat: no-repeat; background-size: cover; width: 100px; height: 100px; margin-left: auto; margin-right: auto; border-radius: 5px;">
            </div>'
      
      record.deleted_at.present? ? availability = "ACTIVATE" : availability = "DEACTIVATE"
      user << (link_to "EDIT", edit_user_path(record), class: "small button action_button")+' '+ (link_to availability, update_availability_user_path(record), method: :post, data: {remote: true}, id: "availability_#{record.id}", class: "small button alert action_button")

      users << user 
    end 
    users 
  end 

  def my_search
    @filtered_users = User.order(first_name: :asc)
  end 

  def sort_order_filter 
    records = my_search.order("#{sort_column} #{sort_direction}") 
    if params[:search][:value].present? 
      records = records.where(" email iLIKE :search or first_name iLIKE :search or last_name iLIKE :search", search: "%#{params[:search][:value]}%") 
    end 
    records 
  end 

  def display_on_page 
    sort_order_filter.page(page).per(per_page) 
  end 

  def page 
    params[:start].to_i/per_page + 1 
  end 

  def per_page 
    params[:length].to_i > 0 ? params[:length].to_i : 10 
  end 

  def sort_column 
    columns = %w[email first_name last_name gender birthdate role]
    columns[params[:order][:'0'][:column].to_i - 1] 
  end 

  def sort_direction 
    params[:order][:'0'][:dir] == "desc" ? "desc" : "asc" 
  end 
end 