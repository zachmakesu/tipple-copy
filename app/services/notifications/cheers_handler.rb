class Notifications::CheersHandler
  NOTIFICATION_TYPE = 4

  def initialize current_user, object
    @interaction = object
    @current_user = current_user
  end

  def deliver
    return unless recipient.notify_cheers
    IOSHandler.new(recipient, alert, payload).deliver
    AndroidHandler.new(recipient, payload, alert: alert).deliver
  end

  private
  def recipient
    (@interaction.conversation.members - [@current_user]).first
  end

  def alert
    "#{@interaction.place.name} - Cheers let’s have a drink with #{@current_user.first_name}!"
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      conversation_id: @interaction.conversation.id
    }
  end
end
