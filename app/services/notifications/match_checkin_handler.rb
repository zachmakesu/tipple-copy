class Notifications::MatchCheckinHandler
  NOTIFICATION_TYPE = 3

  def initialize current_user, object
    @checkin = object
    @current_user = current_user
  end

  def deliver
    return if !recipient.notify_activity || recipient.blocked_profiles.include?(@checkin.user)
    IOSHandler.new(recipient, alert, payload).deliver
    AndroidHandler.new(recipient, payload, alert: alert).deliver
  end

  private
  def recipient
    @current_user
  end

  def alert
    alert = "#{@checkin.user.first_name} is at #{@checkin.place.name} why not join "
    alert += if @checkin.user.female?
               "her?"
             else
               "him?"
             end
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      profile_uid: @checkin.user.uid,
      place_id: @checkin.place.id
    }
  end
end
