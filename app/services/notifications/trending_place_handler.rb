class Notifications::TrendingPlaceHandler
  NOTIFICATION_TYPE = 5

  def initialize current_user, object
    @place_activity = object
    @current_user = current_user
  end

  def deliver
    return unless recipient.notify_activity
    IOSHandler.new(recipient, alert, payload).deliver
    AndroidHandler.new(recipient, payload, alert: alert).deliver
  end

  private
  def recipient
    @current_user
  end

  def alert
    "#{@place_activity.place.name} - #{@place_activity.current_check_in.people_count} Girls/Guys are there! Join them now!"
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      place_id: @place_activity.place.id
    }
  end
end
