class Notifications::NewMessageHandler
  NOTIFICATION_TYPE = 1

  def initialize current_user, object
    @conversation = object
    @current_user = current_user
  end

  def deliver
    return unless recipient.notify_messages
    IOSHandler.new(recipient, alert, payload).deliver
    AndroidHandler.new(recipient, payload, alert: alert).deliver
  end

  private
  def recipient
    (@conversation.members - [@current_user]).first
  end

  def alert
    "#{@current_user.first_name} sent you a message."
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      conversation_id: @conversation.id
    }
  end
end
