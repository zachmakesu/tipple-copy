class Notifications::ManualNotificationHandler
  NOTIFICATION_TYPE = 6

  def initialize current_user, object
    @manual_notification = object
    @current_user = current_user
  end

  def deliver
    IOSHandler.new(recipient, alert, payload).deliver
    AndroidHandler.new(recipient, payload, alert: alert).deliver
  end

  private
  def recipient
    @current_user
  end

  def alert
    "#{@manual_notification.message}"
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      manual_notification_id: @manual_notification.id
    }
  end
end
