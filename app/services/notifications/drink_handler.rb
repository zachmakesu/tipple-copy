class Notifications::DrinkHandler
  NOTIFICATION_TYPE = 2

  def initialize current_user, object
    @interaction = object
    @current_user = current_user
  end

  def deliver
    return unless recipient.notify_drinks
    IOSHandler.new(recipient, alert, payload).deliver
    AndroidHandler.new(recipient, payload, alert: alert).deliver
  end

  private
  def recipient
    @interaction.initiatee
  end

  def alert
    "Someone wants to have a Drink @#{@interaction.place.name}!"
  end

  def payload
    {
      notification_type: NOTIFICATION_TYPE,
      profile_uid: @interaction.initiator.uid,
      place_id: @interaction.place.id
    }
  end
end
