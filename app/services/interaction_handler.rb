class InteractionHandler
  NO_CREDITS_ERROR_MESSAGE = 'Check-in to win Drinks or buy them in the Drinks page.'
  attr_accessor :initiator, :initiatee, :place, :refuse
  attr_reader :interaction, :response

  def initialize(place_id,initiator_uid,initiatee_uid, opts={})
    @place      = Place.find(place_id)
    @initiator  = User.find_by(uid: initiator_uid)
    @initiatee  = User.find_by(uid: initiatee_uid)
    @refuse     = opts[:refuse]
    set_interaction
  end

  def create_or_update
    if @interaction.cheers?
      deactivate_other_interactions
      return @interaction
    end
    ActiveRecord::Base.transaction do
      before_action
      case @before_state
      when "new"    then handle_new_interaction
      when "like"   then handle_like_state
      when "drink"  then handle_drink_state
      end
      after_action if @interaction.persisted?
    end
    send_notifications
    @interaction.persisted? ? @interaction : nil
  end

  def success?
    can_send_drink? ? (create_or_update && @response.blank?) : false
  end

  private
  def can_send_drink?
    # Only check credits if interaction step is for sending drink
    return true unless (@interaction.like? && same_initiator?)
    if @initiator.has_credits?
      true
    else
      @response = NO_CREDITS_ERROR_MESSAGE
      false
    end
  end

  def handle_new_interaction
    unless @interaction.save
      @response = @interaction.errors.full_messages.join('; ')
    end
  end

  def handle_like_state
    if same_initiator?
      @interaction.drink! unless has_zero_credits_left?
    else
      @interaction.like_back!
    end
  end

  def handle_drink_state
    unless same_initiator?
      refuse ? @interaction.refuse! : handle_cheers
    end
  end

  def handle_cheers
    deactivate_other_interactions
    @interaction.like_back!
  end

  def deactivate_other_interactions
    interactions = Interaction.where("id != :interaction AND ((initiator_id = :initiator AND initiatee_id = :initiatee) OR (initiator_id = :initiatee AND initiatee_id = :initiator))",initiator: initiator.id, initiatee: initiatee.id, interaction: @interaction.id)
    interactions.update_all(active: false)
  end

  def has_zero_credits_left?
    !initiator.has_credits?
  end

  def same_initiator?
    @interaction.histories.empty? ? false : @interaction.histories.last.sender == initiator
  end

  def before_action
    @before_state = @interaction.new_record? ? "new" : @interaction.current_state.to_s
  end

  def after_action
    @interaction.update(initiator: initiator, initiatee: initiatee) unless refuse
    @after_state = refuse ? "refuse" : @interaction.current_state
    if @interaction.cheers?
      generate_conversation_and_members
    end
    create_history
    Rails.logger.error "[InteractionHandlerError] #{@interaction.errors.full_messages.join('; ')}" unless @interaction.errors.empty?
  end

  def send_notifications
    if @interaction.cheers?
      send_cheers_notification
    elsif @interaction.drink?
      send_drink_notification 
    else
    end
  end

  def create_history
    if refuse
      @interaction.generate_history(@before_state,@after_state,initiatee,initiator)
    else
      @interaction.generate_history(@before_state,@after_state,initiator,initiatee)
    end
  end

  def set_interaction
    last_interaction = Interaction.where("((initiator_id = :initiator AND initiatee_id = :initiatee) OR (initiator_id = :initiatee AND initiatee_id = :initiator)) AND workflow_state = 'cheers'",initiator: initiator.id, initiatee: initiatee.id).first
    place_interaction = Interaction.where("place_id = :place_id AND ((initiator_id = :initiator AND initiatee_id = :initiatee) OR (initiator_id = :initiatee AND initiatee_id = :initiator))",initiator: initiator.id, initiatee: initiatee.id, place_id: place.id).first

    @interaction = last_interaction || place_interaction
    @interaction ||= Interaction.new(interaction_options)
  end

  def generate_conversation_and_members
    conversation = @interaction.create_conversation
    conversation.members << [initiator,initiatee] if conversation.members.empty? 
  end

  def send_drink_notification
    Notifications::DrinkWorker.perform_async(@initiator.id, @interaction.id)
    Pusher::ActivityNotificationHandler.new(nil, @initiatee.id).deliver
  end

  def send_cheers_notification
    Notifications::CheersWorker.perform_async(@initiator.id, @interaction.id)
  end

  def interaction_options
    {
      place: place,
      initiator: initiator,
      initiatee: initiatee,
    }    
  end

end
