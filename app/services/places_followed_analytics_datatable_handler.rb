class PlacesFollowedAnalyticsDatatableHandler 
  delegate :params, :link_to, to: :@view 

  def initialize(view, user) 
    @view = view 
    @user = user
  end 

  def as_json(options = {}) 
    { 
      data: data, 
      recordsTotal: my_search.map(&:id).count, 
      recordsFiltered: sort_order_filter.map(&:id).count
    } 
  end 

private 

  def data 
    places = [] 
    display_on_page.map do |record| 
      place = [] 
      place << record.id 
      place << record.name
      place << record.check_ins_here_count
      places << place 
    end 
    places 
  end 


  def my_search
    user = User.find(@user.id)
    @filtered_places =  user.place_followed.for_places_followed_analytics(@user)
  end 

  def sort_order_filter 
    records = my_search.order("#{sort_column} #{sort_direction}") 
    if params[:search][:value].present? 
      records = records.where(" name iLIKE :search", search: "%#{params[:search][:value]}%") 
    end 
    records 
  end 

  def display_on_page 
    sort_order_filter.page(page).per(per_page) 
  end 

  def page 
    params[:start].to_i/per_page + 1 
  end 

  def per_page 
    params[:length].to_i > 0 ? params[:length].to_i : 10 
  end 

  def sort_column 
    columns = %w[name check_ins_here_count]
    columns[params[:order][:'0'][:column].to_i - 1] 
  end 

  def sort_direction 
    params[:order][:'0'][:dir] == "desc" ? "desc" : "asc" 
  end 
end 