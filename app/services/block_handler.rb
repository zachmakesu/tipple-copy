class BlockHandler
  MISSING_PARAMS_RESPONSE = "Missing parameters"
  DOES_NOT_EXIST_RESPONSE = "It doesn't exist in block list"
  attr_accessor :blocker, :blockee
  attr_reader   :response

  def initialize(blocker,blockee,opts={})
    @blocker  = blocker
    @blockee  = blockee
    @response = ""
  end

  def block
    if params_not_empty?
      ActiveRecord::Base.transaction do
        @blocked = blocker.block_lists.create(blockable: blockee)
      end
      @response = @blocked.errors.full_messages.join(', ')
    else
      @response = MISSING_PARAMS_RESPONSE
    end
    @response.blank? ? self : nil
  end

  def unblock
    if params_not_empty?
      blockee_is_in_blacklist? ? do_unblocking : @response =  DOES_NOT_EXIST_RESPONSE
    else
      @response = MISSING_PARAMS_RESPONSE
    end
    @response.blank? ? self : nil
  end

  private
  def params_not_empty?
    blocker.present? && blockee.present?
  end

  def blockee_is_in_blacklist?
    blocker.blocked_profiles.include?(blockee) || blocker.blocked_places.include?(blockee)
  end

  def do_unblocking
    blocker.blocked_places.delete(blockee) if blocker.blocked_places.include?(blockee)
    blocker.blocked_profiles.delete(blockee) if blocker.blocked_profiles.include?(blockee)
  end
end
