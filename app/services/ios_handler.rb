class IOSHandler
  attr_reader :client
  def initialize recipient, alert, payload
    @recipient = recipient
    @alert = alert
    @payload = payload
    @errors = []
  end

  def client
    cert, houston_client = if Rails.env.production? || Rails.env.staging?
                             ['config/apn.pem', Houston::Client.production]
                           else
                             ['config/apn_sandbox.pem', Houston::Client.development]
                           end
    houston_client.certificate = File.read(cert)
    houston_client
  end

  def deliver
    return if @recipient.devices.enabled.ios.empty?
    send_notifications
    @errors.empty?
  end

  private
  def send_notifications
    for device in @recipient.devices.enabled.ios
      notification = create_notification(device)
      client.push notification
      after_action_for notification
    end
  end

  def create_notification device
    notification = Houston::Notification.new(device: device.token)
    notification.alert = @alert
    notification.custom_data = @payload
    notification
  end

  def after_action_for notification
    if notification.error
      Rails.logger.error(notification.error)
      @errors << notification.error
      # must be an expired/invalid device so we should disable it
      device.update(is_enabled: false)
    end
  end
end
