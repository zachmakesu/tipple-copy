class ActivityHandler

  attr_accessor :source

  def initialize(source,opts={})
    @source = source.reload
  end

  def create

    ActiveRecord::Base.transaction do
      case source.class.name
      when "CheckIn"
        @activity = Activities::CheersCheckInActivity.create(activity_options) if has_user_cheers_with?
      when "Place"
        @activity = Activities::PlacesActivity.create(activity_options) if more_than_15? && within_8_hours? && multiple_by_15?
      end
    end

    after_action if @activity.present?

  end

  def has_user_cheers_with?
    source.user.cheers_with.count > 0
  end

  def more_than_15?
    source.check_ins.currently_checked_in.count >= 15
  end

  def within_8_hours?
    return true if source.activities.blank?
    source.activities.last.created_at + 8.hours <= DateTime.now
  end

  def multiple_by_15?
    source.check_ins.currently_checked_in.count % 15 == 0
  end

  def status_is_like_or_cheers?
    source.state_changed_to == 'like' || source.state_changed_to == 'cheers'
  end

  def after_action
    case source.class.name
    when "CheckIn" then checkin_after_action
    when "Place" then place_after_action
    end
  end

  def checkin_after_action
    @activity.members << source.user.cheers_with
    @activity.members.each do |member|
      Notifications::MatchCheckinWorker.perform_async(member.id, source.id)
      Pusher::ActivityNotificationHandler.new(@activity.id, member.id).deliver
    end
  end

  def place_after_action
    @activity.members << source.followers
    @activity.members.each do |member|
      Notifications::TrendingPlaceWorker.perform_async(member.id, @activity.id)
      Pusher::ActivityNotificationHandler.new(@activity.id, member.id).deliver
    end
  end

  def activity_options
    {
      source_id:    source.id,
      source_type:  source.class.name
    }
  end

end
