FollowHandlerError = Class.new(StandardError)

class FollowHandler

  attr_accessor :user, :place, :response

  def initialize(user,place_id)
    @user     = user
    @place    = Place.not_deleted.find_by(id: place_id) 
    @response = Hash.new
  end

  def follow
    return place_not_found if place.nil?

    ActiveRecord::Base.transaction do
      @follow = place.follows.create(user: user)
    end

    create_response
    self

  end

  def unfollow
    return place_not_found if place.nil?

    ActiveRecord::Base.transaction do
      @follow = place.follows.find_by(user: user)
      @follow ? @follow.destroy : @error = "User not found"
    end

    create_response(@error)
    self

  end

  private
  def place_not_found
    response[:success] = false
    response[:details] = "Place not found"
    self
  end
  def create_response(error=nil)
    if !@follow.nil? && @follow.errors.blank?
      response[:success] = true
      response[:details] = place
    else
      response[:success] = false
      response[:details] = error || @follow.errors.full_messages.join(', ')
    end
  end

end
