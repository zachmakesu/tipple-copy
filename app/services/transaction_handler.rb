class TransactionHandler
  VALID_PLATFORMS           = ['ios', 'android']
  INVALID_PLATFORM_MESSAGE  = 'Please provide valid platform'
  INVALID_PRODUCT_MESSAGE   = 'Please provide valid product'
  INVALID_RECEIPT_MESSAGE   = 'Please provide valid receipt'

  def initialize params
    @platform = params[:platform]
    @product = Product.find(params[:product_id]) if params[:product_id]
    @receipt_data = params[:receipt_data]
    validate_receipt
    self
  end

  def associate_with current_user
    duration = DateTime.now + 1.day if @product.day?
    duration = DateTime.now.next_week if @product.week?
    duration = DateTime.now.next_month if @product.month?
    duration = nil if @product.not_applicable?

    expiring_transactions = current_user.transactions.with_expiration
    if expiring_transactions.empty? || (expiring_transactions.last.try(:expires_at) < DateTime.now)
      current_user.transactions.create(
        product_id: @product.id,
        expires_at: duration,
        transaction_id: dynamic_transaction_id,
        receipt: @receipt.to_json
      )
    end
  end

  def valid_platform?
    VALID_PLATFORMS.include?(@platform)
  end

  def valid_product?
    @product && @product.can_be_purchase?
  end

  def valid_receipt?
    @receipt.present?
  end

  def validate_receipt
    case @platform
    when 'ios' then validate_ios_receipt
    when 'android' then validate_android_receipt
    else false
    end
  end

  private
  def validate_ios_receipt
    return false if @receipt_data.blank?
    @receipt = if @product.recurring?
                 Venice::Receipt.verify @receipt_data, { shared_secret: ENV['IOS_IAP_SHARED_SECRET']}
               else
                 Venice::Receipt.verify @receipt_data
               end
    if @receipt
      case @receipt.original_json_response['status'].to_i
      when 0     then true
      else false
      end
    end
  end

  def validate_android_receipt
    return false if @receipt_data.blank?
    @receipt = PlayStoreHandler.instance.verify(@receipt_data)
  end

  def dynamic_transaction_id
    case @platform
    when 'ios'        then @receipt.in_app.last.transaction_id
    when 'android'    then PlayStoreHandler.parse_receipt_from(@receipt_data)['orderId']
    else              nil
    end
  end
end
