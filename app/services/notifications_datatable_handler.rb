class NotificationsDatatableHandler 
  include ActionView::Helpers::TextHelper
  include NotificationsHelper
  delegate :params, :link_to, :notification_path, to: :@view 

  def initialize(view) 
    @view = view 
  end 

  def as_json(options = {}) 
    { 
      data: data, 
      recordsTotal: my_search.count, 
      recordsFiltered: sort_order_filter.count 
    } 
  end 

private 

  def data 
    notifications = [] 
    display_on_page.map do |record| 
      notification = [] 
      notification << record.id 
      notification << (link_to truncate(record.message), notification_path(record), data: {remote: true})
      
      notification << truncate(send_to(record))
      notification << record.created_at.to_date
      notifications << notification 
    end 
    notifications 
  end 

  def my_search
    @filtered_notifications = ManualNotification.all
  end 

  def sort_order_filter 
    records = my_search.order("#{sort_column} #{sort_direction}") 
    if params[:search][:value].present? 
      records = records.where("message iLIKE :search", search: "%#{params[:search][:value]}%") 
    end 
    records 
  end 

  def display_on_page 
    sort_order_filter.page(page).per(per_page) 
  end 

  def page 
    params[:start].to_i/per_page + 1 
  end 

  def per_page 
    params[:length].to_i > 0 ? params[:length].to_i : 10 
  end 

  def sort_column 
    columns = %w[message]
    columns[params[:order][:'0'][:column].to_i - 1] 
  end 

  def sort_direction 
    params[:order][:'0'][:dir] == "desc" ? "desc" : "asc" 
  end 
end 