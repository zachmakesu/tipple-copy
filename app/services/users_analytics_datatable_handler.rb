class UsersAnalyticsDatatableHandler 
  delegate :params, :link_to, :user_path, :user_followed_places_path, to: :@view 

  def initialize(view) 
    @view = view 
  end 

  def as_json(options = {}) 
    { 
      data: data, 
      recordsTotal: my_search.map(&:id).count, 
      recordsFiltered: sort_order_filter.map(&:id).count
    } 
  end 

private 

  def data 
    users = [] 
    display_on_page.map do |record| 
      user = [] 
      user << record.id 
      user << (link_to record.first_name, user_path(record), data: {remote: true})
      user << (link_to record.followed_place_count, user_followed_places_path(record), data: {remote: true})
      user << record.check_ins_count
      user << record.cheers_with_count 
      users << user 
    end 
    users 
  end 


  def my_search
    @filtered_users = User.for_analytics

  end 

  def sort_order_filter 
    records = my_search.order("#{sort_column} #{sort_direction}") 
    if params[:search][:value].present? 
      records = records.where(" first_name iLIKE :search", search: "%#{params[:search][:value]}%") 
    end 
    records 
  end 

  def display_on_page 
    sort_order_filter.page(page).per(per_page) 
  end 

  def page 
    params[:start].to_i/per_page + 1 
  end 

  def per_page 
    params[:length].to_i > 0 ? params[:length].to_i : 10 
  end 

  def sort_column 
    columns = %w[first_name followed_place_count check_ins_count cheers_with_count]
    columns[params[:order][:'0'][:column].to_i - 1] 
  end 

  def sort_direction 
    params[:order][:'0'][:dir] == "desc" ? "desc" : "asc" 
  end 
end 