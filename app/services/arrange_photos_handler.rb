class ArrangePhotosHandler
  NO_PHOTO_RESPONSE       = "No photos to arrange"
  INVALID_PARAMS_RESPONSE = "Invalid given params"
  MISSING_PARAMS_RESPONSE = "Missing parameters"
  attr_accessor :imageable, :photos, :positions
  attr_reader   :response
  def initialize(imageable,photos,positions,opts={})
    @imageable  = imageable
    @photos     = photos
    @positions  = positions
    @response   = ""
  end

  def arrange_photos
    if params_not_empty?
      if empty_photos?
        @response = NO_PHOTO_RESPONSE
      elsif one_photo?
        imageable.photos.first.update(position: 0)
      else
        if valid_params?
          photos.each_with_index { |v, i| imageable.photos.find(v).update(position: positions[i].to_i) }
        else
          @response = INVALID_PARAMS_RESPONSE
        end
      end
    else
      @response = MISSING_PARAMS_RESPONSE
    end
    @response.blank? ? self : false 
  end

  private
  def one_photo?
    imageable.photos.count == 1
  end

  def empty_photos?
    imageable.photos.empty?
  end

  def params_not_empty?
    imageable.present? && photos.present? && positions.present?

  end

  def valid_params?
    pos = positions.map(&:to_i)
    ids = photos.map(&:to_i)

    if imageable_type == "User"
      pos.length == ids.length && ids.uniq.length == ids.length && pos.uniq.length == pos.length && pos.include?(0) && pos.max <= 4
    else
      pos.length == ids.length && ids.uniq.length == ids.length && pos.uniq.length == pos.length && pos.include?(0)
    end
  end

  def imageable_type
    imageable.photos.first.imageable_type
  end
end
