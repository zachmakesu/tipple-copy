class PlayStoreHandler
  def initialize
    config = CandyCheck::PlayStore::Config.new(
      application_name: 'Tipple',
      application_version: 'v1.6.d11232016',
      issuer: ENV['GOOGLE_ISSUER'],
      key_file: ENV['GOOGLE_KEY_FILE'],
      key_secret: ENV['GOOGLE_KEY_SECRET'],
      cache_file: 'tmp/candy_check_play_store_cache'
    )
    @verifier = CandyCheck::PlayStore::Verifier.new(config)
    @verifier.boot!
  end

  @@instance = PlayStoreHandler.new

  def self.instance
    return @@instance
  end

  def verify(receipt_data)
    receipt = PlayStoreHandler.parse_receipt_from(receipt_data)
    package, product_id, token = receipt["packageName"], receipt["productId"], receipt["purchaseToken"]
    return nil if [package, product_id, token].any?(&:blank?)
    response = @verifier.verify(package, product_id, token)
    if response.is_a?(CandyCheck::PlayStore::Receipt)
      response.consumption_state == 0 ? response : nil
    else
      nil 
    end
  end

  def self.parse_receipt_from receipt_data
    decoded_string = Base64.decode64(receipt_data)
    decoded_string.blank? ? {} : JSON.parse(decoded_string)
  end

  private_class_method :new
end
