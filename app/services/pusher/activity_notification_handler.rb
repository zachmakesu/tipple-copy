class Pusher::ActivityNotificationHandler
  def initialize activity_id, recipient_id
    @activity = Activity.find(activity_id) if activity_id
    @recipient = User.find(recipient_id)
  end

  def deliver
    begin
      Pusher.trigger(
        channel_name,
        event_name,
        payload
      )
    rescue Pusher::Error => e
      Rails.logger.error(e)
    end
  end

  private
  def channel_name
    "notification-#{@recipient.uid}"
  end

  def event_name
    "new-activity"
  end

  def payload
    {}
  end
end
