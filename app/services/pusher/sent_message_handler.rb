class Pusher::SentMessageHandler
  def initialize conversation_id, message_id
    @conversation = Conversation.find(conversation_id)
    @message = Message.find(message_id)
    @sender = User.find(@message.sender_id)
    @recipient = @conversation.members.reject{|u| u.id == @sender.id}.first
  end

  def deliver
    begin
      Pusher.trigger(
        channel_name,
        event_name,
        payload
      )
    rescue Pusher::Error => e
      Rails.logger.error(e)
    end
  end

  private
  def channel_name
    "chat-#{@recipient.uid}"
  end

  def event_name
    "new-message-from-#{@sender.uid}"
  end

  def payload
    {
      body: @message.body,
      sender: { uid: @sender.uid },
      sent_at: @message.created_at.iso8601
    }
  end
end
