class Pusher::MessageNotificationHandler
  def initialize conversation_id, message_id
    @conversation = Conversation.find(conversation_id)
    @message = Message.find(message_id)
    @sender = User.find(@message.sender_id)
    @recipient = @conversation.members.reject{|u| u.id == @sender.id}.first
  end

  def deliver
    begin
      Pusher.trigger(
        channel_name,
        event_name,
        payload
      )
    rescue Pusher::Error => e
      Rails.logger.error(e)
    end
  end

  private
  def channel_name
    "notification-#{@recipient.uid}"
  end

  def event_name
    "new-message"
  end

  def payload
    {}
  end
end
