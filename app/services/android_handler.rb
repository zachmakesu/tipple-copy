AndroidHandlerError = Class.new(StandardError)

class AndroidHandler

  def initialize recipient, payload, options={}
    payload[:alert] = options[:alert] if options.has_key?(:alert)

    @fcm        = FCM.new(ENV["FCM_KEY"])
    @recipient  = recipient
    @payload    = payload
  end

  def deliver
    @fcm.send(registration_ids,options)
  end

  private

  def registration_ids
    @recipient.devices.enabled.pluck(:token)
  end

  def options
    {
      data: @payload
    }
  end

end
