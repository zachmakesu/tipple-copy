IdentityHandlerError = Class.new(StandardError)

class IdentityHandler
  attr_reader :current_user, :identity_options
  def initialize current_user, identity_options = {}, opts = {}
    @current_user = current_user
    @identity_options = identity_options
    @opts = opts
  end

  def create_or_update
    identity = current_user.identities.find_or_initialize_by({
      provider: provider,
      uid: uid
    })
    identity.update(token: token) ? identity : nil
  end

  private
  def provider
    identity_options[:provider]
  end
  def uid
    identity_options[:uid]
  end
  def token
    identity_options[:token]
  end
end
