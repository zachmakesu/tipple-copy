class PlacesDatatableHandler 
  delegate :params, :link_to, :place_path, :edit_place_path, :update_availability_place_path, to: :@view 

  def initialize(view) 
    @view = view 
  end 

  def as_json(options = {}) 
    { 
      data: data, 
      recordsTotal: my_search.count, 
      recordsFiltered: sort_order_filter.count 
    } 
  end 

private 

  def data 
    places = [] 
    display_on_page.map do |record| 
      place = [] 
      place << record.id 
      place << (link_to record.name, place_path(record), data: {remote: true})
      place << record.address 
      place << record.lat
      place << record.lng 

      if record.background.present? 
        place_background = record.background.url(:large)
      else
        place_background = Photo.default_photo_place.try(:image).url(:large)
      end
      place << '<div style="background-image: url('+place_background+'); background-position: center center; background-repeat: no-repeat; background-size: cover; width: 100px; height: 100px; margin-left: auto; margin-right: auto; border-radius: 5px;">
            </div>'
      
      record.deleted_at.present? ? availability = "ACTIVATE" : availability = "DEACTIVATE"
      place << (link_to "EDIT", edit_place_path(record), class: "small button action_button")+' '+ (link_to availability, update_availability_place_path(record), method: :post, data: {remote: true}, id: "availability_#{record.id}", class: "small button alert action_button")

      places << place 
    end 
    places 
  end 

  def my_search
    @filtered_places = Place.all
  end 

  def sort_order_filter 
    records = my_search.order("#{sort_column} #{sort_direction}") 
    if params[:search][:value].present? 
      records = records.where(" name iLIKE :search or address iLIKE :search", search: "%#{params[:search][:value]}%") 
    end 
    records 
  end 

  def display_on_page 
    sort_order_filter.page(page).per(per_page) 
  end 

  def page 
    params[:start].to_i/per_page + 1 
  end 

  def per_page 
    params[:length].to_i > 0 ? params[:length].to_i : 10 
  end 

  def sort_column 
    columns = %w[name]
    columns[params[:order][:'0'][:column].to_i - 1] 
  end 

  def sort_direction 
    params[:order][:'0'][:dir] == "desc" ? "desc" : "asc" 
  end 
end 