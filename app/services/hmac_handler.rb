class HmacHandler
  attr_accessor :endpoint, :params, :version
  WHITELISTED_ATTR = %w{
    avatar
  }

  # https://instagram.com/developer/secure-api-requests/?hl=en
  def self.signature_from(endpoint, params, api_version=1)
    secret = if api_version == 1
               ENV['HMAC_SECRET']
             else
               ENV["HMAC_SECRET_#{api_version}"]
             end
    sig = endpoint
    params.sort.map do |key, val|
      next if WHITELISTED_ATTR.include?(key)
      sig += '|%s=%s' % [key, val]
    end
    digest = OpenSSL::Digest.new('sha256')
    # Rails.logger.debug "sigstr: #{sig}" if Rails.env.staging?
    return OpenSSL::HMAC.hexdigest(digest, secret, sig)
  end

  # http://www.rubydoc.info/github/plataformatec/devise/Devise.secure_compare
  def self.secure_compare(a, b)
    return false if a.blank? || b.blank? || a.bytesize != b.bytesize
    l = a.unpack "C#{a.bytesize}"

    res = 0
    b.each_byte { |byte| res |= byte ^ l.shift }
    res == 0
  end

  def initialize endpoint, params, version=1
    @endpoint = endpoint
    @params = params
    @version = version
  end

  def digest
    HmacHandler.signature_from(@endpoint, @params, @version)
  end

end
