# == Schema Information
#
# Table name: manual_notifications
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  message    :text             not null
#  send_to    :integer          default(0), not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_manual_notifications_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_cef6ef939e  (user_id => users.id)
#

class ManualNotification < ActiveRecord::Base
  belongs_to :user
  has_many :notification_recipients, dependent: :destroy
  has_many :recipients, through: :notification_recipients, source: :user

  enum send_to: { all_users: 0, all_males: 1, all_females: 2, specific_users: 3 }

  validates :user_id, :message, :send_to, presence: true
end
