# == Schema Information
#
# Table name: user_settings
#
#  created_at                      :datetime         not null
#  deleted_at                      :datetime
#  help                            :boolean          default(FALSE)
#  hide_profile_24hrs              :datetime
#  hide_profile_with_zero_check_in :boolean          default(FALSE)
#  id                              :integer          not null, primary key
#  notify_activity                 :boolean          default(FALSE)
#  notify_cheers                   :boolean          default(FALSE)
#  notify_likes                    :boolean          default(FALSE)
#  notify_messages                 :boolean          default(FALSE)
#  prefers_men                     :boolean          default(FALSE)
#  prefers_women                   :boolean          default(FALSE)
#  updated_at                      :datetime         not null
#  user_id                         :integer
#
# Indexes
#
#  index_user_settings_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_d1371c6356  (user_id => users.id)
#

class UserSetting < ActiveRecord::Base
  belongs_to :user
  has_many :blocked_lists, class_name: "BlockList"
  has_many :blocked_users, through: :blocked_lists, source: :user


  ATTRIBUTE_METHODS = ["prefers_men", "prefers_women", "notify_likes", "notify_cheers", "notify_messages", "notify_activity", "hide_profile_with_zero_check_in", "hide_profile_24hrs", "deleted_at"]


  ["prefers_men","prefers_women"].each do |p|
    define_method("is_#{p.downcase}") do
      p.downcase == true
    end
  end

  ATTRIBUTE_METHODS.each do |t|
    define_method("toggle_#{t.downcase}") do
      if(t == "hide_profile_24hrs")
        self.hide_profile_24hrs? ? self.update_attributes(hide_profile_24hrs: nil) : self.update_attributes(hide_profile_24hrs: DateTime.now + 24.hour)
      elsif (t == "deleted_at")
        self.deleted_at? ? self.update_attributes(deleted_at: nil) : self.update_attributes(deleted_at: DateTime.now)
      else
        self.send("#{t}?") ? self.update_attribute(t.to_sym, false) : self.update_attribute(t.to_sym, true)
      end
    end
  end

  def preferences
    g = []
    g << 1 if prefers_men?
    g << 2 if prefers_women? 
    g
  end


end
