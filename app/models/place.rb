# == Schema Information
#
# Table name: places
#
#  address     :string
#  created_at  :datetime         not null
#  deleted_at  :datetime
#  description :string
#  id          :integer          not null, primary key
#  lat         :float
#  lng         :float
#  name        :string
#  updated_at  :datetime         not null
#

class Place < ActiveRecord::Base
  DEFAULT_DESCRIPTION = 'No description has been added for this place just yet, try checking back again later!'
  VALID_DISTANCE = 0.1 # 100 meters
  geocoded_by :address, latitude: :lat, longitude: :lng
  has_many :follows
  has_many :followers, through: :follows, source: :user
  has_many :check_ins
  has_many :user_check_ins, through: :check_ins, source: :user
  has_many :photos, as: :imageable
  has_many :interactions
  # define method  for this since it's now under interaction
  # has_many :interaction_histories
  has_many :activities, class_name: "Activities::PlacesActivity", foreign_key: :source_id

  scope :not_deleted, ->{ where(deleted_at: nil)}
  scope :filtered_by_name, -> (place_name) { where("places.name iLIKE ?", place_name) }
  scope :filtered_by_address, -> (place_address) { where("places.address iLIKE ?", place_address) }

  #My goal for this is to have names for the counts of 
  #followers, total_check_ins, currently_checked_in and cheers 
  #that will be used in column sorting on place_analytics_handler
  scope :for_analytics, ->  { joins("LEFT OUTER JOIN follows ON follows.place_id = places.id 
                                     LEFT OUTER JOIN check_ins ON check_ins.place_id = places.id 
                                     LEFT OUTER JOIN interactions ON interactions.place_id = places.id
                                     LEFT OUTER JOIN interaction_histories ON interactions.id = interaction_histories.interaction_id AND interaction_histories.state_changed_to = 'cheers'
                                     ")
                              .select('places.id, places.name,
                                     count(distinct(follows.id)) as followers_count, 
                                     count(distinct(check_ins.id)) as check_ins_count,
                                     count(distinct(check_ins.expires_at >= now())) as currently_check_ins_count,
                                     count(distinct(interaction_histories.id)) as cheers_count')
                              .group('places.id')
                            }
  scope :for_places_followed_analytics, -> (user)  {
                              joins("LEFT OUTER JOIN check_ins ON check_ins.place_id = places.id and check_ins.user_id = " + user.id.to_s )
                              .select('places.id, places.name, 
                                     count(distinct(check_ins.id)) as check_ins_here_count
                                     ')
                              .group('places.id')
                            }          
 
  validates :name, uniqueness: true
  validates_presence_of :name,:address,:lat,:lng   
  validates :description, length: { maximum: 140 }, allow_blank: true

  def background
    self.photos.find_by(position: 0).try(:image)
  end

  def self.search(search)
    where('name iLIKE :search', search: "%#{search}%").order(:name)  
  end

  def interaction_histories
    history_ids = interactions.map{|i| i.histories.map(&:id)}.flatten
    InteractionHistory.where(id: history_ids)
  end

  def self.get_near_places(lat,lng)
    near([lat, lng], 100.0, { units: :km })
  end

  def user_interactions(sender, recipient)
    check_ins.availability.where(user_id: sender).last.interactions.where(recipient_id: recipient)
  end

  def unfollow_by(user)
    follows.find_by(user: user) ? follows.find_by(user: user).destroy : false
  end

  def follow_by(user)
    follows.create(user: user)
  end

  def followed_by?(user)
    self.followers.include?(user)
  end

  def description
    if super.blank?
      DEFAULT_DESCRIPTION
    else
      super
    end
  end


  def check_in(user,random_likes,lat,lng)
    #0.010 = 10meters
    if Place.valid_random_likes?(random_likes.to_i) && Place.valid_distance?(self.distance_to([lat,lng]))
      if user.check_ins.availability.empty?
        follows.build(user: user).save unless followers.include?(user)
        check_ins.build(user: user, random_likes: random_likes).save
      else
        if user.check_ins.availability.last.place.id != self.id
          user.check_ins.availability.last.update(expires_at: DateTime.now)
          follows.build(user: user).save unless followers.include?(user)
          check_ins.build(user: user, random_likes: random_likes).save
        end
      end
    end
  end

  def self.valid_distance?(distance)
    (0..VALID_DISTANCE).include?(distance)
  end

  def self.valid_random_likes?(random_likes)
    (1..10).include?(random_likes)
  end

  before_save do
    self.name.capitalize!
  end
end
