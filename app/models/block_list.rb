# == Schema Information
#
# Table name: block_lists
#
#  blockable_id   :integer
#  blockable_type :string
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  updated_at     :datetime         not null
#  user_id        :integer
#
# Indexes
#
#  index_block_lists_on_user_id  (user_id)
#

class BlockList < ActiveRecord::Base
  belongs_to :blockable, polymorphic: true
  belongs_to :user
  belongs_to :place

  validates_presence_of :blockable_id, :user_id
  validates_uniqueness_of :blockable_id, { scope: [:user_id, :blockable_type] }

  before_validation do
    self.errors.add(:base,"Invalid profile") if self.user.id == self.blockable_id && self.blockable_type == "User"
  end

end
