# == Schema Information
#
# Table name: interactions
#
#  active         :boolean          default(TRUE)
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  initiatee_id   :integer
#  initiator_id   :integer
#  place_id       :integer          not null
#  updated_at     :datetime         not null
#  workflow_state :string
#
# Indexes
#
#  index_interactions_on_place_id  (place_id)
#

class Interaction < ActiveRecord::Base
  include Workflow

  validates :initiator_id, :initiatee_id, presence: true

  scope :all_cheers_history, -> { where(interaction_histories: {state_changed_to: "cheers"}) }
  scope :all_likes_history, -> { where(interaction_histories: {state_changed_to: "like"}) }
  scope :drinks, -> { where(workflow_state: 'drink') }
  scope :within, -> (start_date, end_date){  where(interaction_histories: {created_at: start_date..end_date}) }

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }

  has_many :histories, class_name: "InteractionHistory", dependent: :destroy
  has_one :conversation, dependent: :destroy
  belongs_to :initiator, class_name: "User", foreign_key: :initiator_id
  belongs_to :initiatee, class_name: "User", foreign_key: :initiatee_id
  belongs_to :place

  validates :workflow_state, uniqueness: { scope: [:place_id, :initiator_id, :initiatee_id] }

  workflow do
    state :like do
      event :like_back, :transitions_to => :cheers
      event :drink, :transitions_to => :drink
    end
    state :drink do
      event :like_back, :transitions_to => :cheers
      event :refuse, :transitions_to => :like
    end
    state :cheers

    on_error do |error, from, to, event, *args|
      Log.info "Exception(#error.class) on #{from} -> #{to}"
    end
  end

  def generate_history(before_state,after_state, initiator,initiatee)
    histories.create(state_from: before_state, state_changed_to: after_state, sender: initiator, recipient: initiatee)
  end

  before_validation do
    self.errors.add(:base, "Invalid Initiator or Initiatee") if self.initiatee == self.initiator
  end
end
