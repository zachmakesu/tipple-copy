# == Schema Information
#
# Table name: activities
#
#  activity_type :string
#  created_at    :datetime         not null
#  id            :integer          not null, primary key
#  source_id     :integer
#  source_type   :string
#  updated_at    :datetime         not null
#

module Activities
  class CheersCheckInActivity < Activity
  end
end
