# == Schema Information
#
# Table name: activities
#
#  activity_type :string
#  created_at    :datetime         not null
#  id            :integer          not null, primary key
#  source_id     :integer
#  source_type   :string
#  updated_at    :datetime         not null
#

module Activities
  class PlacesActivity < Activity

    before_validation(on: :create) do
      self.build_current_check_in(people_count: self.place.check_ins.availability.count)
    end

  end
end
