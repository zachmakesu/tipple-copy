# == Schema Information
#
# Table name: messages
#
#  body            :text
#  conversation_id :integer
#  created_at      :datetime         not null
#  id              :integer          not null, primary key
#  sender_id       :integer
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_messages_on_conversation_id  (conversation_id)
#
# Foreign Keys
#
#  fk_rails_7f927086d2  (conversation_id => conversations.id)
#
class Message < ActiveRecord::Base
  acts_as_readable on: :created_at
  belongs_to :conversation
  belongs_to :sender, class_name: "User", foreign_key: "sender_id"

  scope :oldest, -> { order(created_at: :asc) }

  before_validation do
    receiver = self.conversation.members.all_except(self.sender).first

    unless self.conversation.members.include?(self.sender)
      self.errors.add(:base, "User is not belong to this conversation")
    end

    self.errors.add(:base, "Unable to send message") if self.sender.blacklist.include?(receiver)
  end

end
