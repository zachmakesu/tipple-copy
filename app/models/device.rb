# == Schema Information
#
# Table name: devices
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  is_enabled :boolean          default(TRUE)
#  name       :string
#  platform   :string
#  token      :string
#  udid       :string
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_devices_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_410b63ef65  (user_id => users.id)
#

class Device < ActiveRecord::Base
  belongs_to :user

  PLATFORMS = %w{ android ios }
  PLATFORMS.each do |platform|
    scope platform.to_sym, ->{ where('platform = ?', platform) }
  end

  scope :enabled, ->{where("is_enabled IS TRUE")}
  scope :disabled, ->{where("is_enabled IS FALSE")}

  validates :token, :platform, :udid, presence: true
  validates :token, uniqueness: { scope: :user_id }
  validates :udid,  uniqueness: { scope: :user_id } 
  validates :platform, inclusion: { in: PLATFORMS }
end
