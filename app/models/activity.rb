# == Schema Information
#
# Table name: activities
#
#  activity_type :string
#  created_at    :datetime         not null
#  id            :integer          not null, primary key
#  source_id     :integer
#  source_type   :string
#  updated_at    :datetime         not null
#

class Activity < ActiveRecord::Base
  belongs_to :user
  self.inheritance_column = 'activity_type'

  has_many :activity_memberships, dependent: :destroy
  has_many :members, through: :activity_memberships

  belongs_to :check_in, class_name: "CheckIn", foreign_key: "source_id"
  belongs_to :interaction_history, class_name: "InteractionHistory", foreign_key: "source_id"
  belongs_to :place, foreign_key: "source_id"

  has_one :check_in_user, through: :check_in, source: :user
  has_one :check_in_place, through: :check_in, source: :place
  
  has_one :current_check_in, foreign_key: :places_activity_id

  has_one :sender, through: :interaction_history
  has_one :liker_place, through: :interaction_history, source: :place

  def place?
    self.activity_type == 'Activities::PlacesActivity'
  end

  def cheers_check_in?
    self.activity_type == 'Activities::CheersCheckInActivity'
  end
end
