# == Schema Information
#
# Table name: product_transactions
#
#  created_at     :datetime         not null
#  expires_at     :datetime
#  id             :integer          not null, primary key
#  product_id     :integer
#  receipt        :json
#  transaction_id :string
#  updated_at     :datetime         not null
#  user_id        :integer
#
# Indexes
#
#  index_product_transactions_on_product_id      (product_id)
#  index_product_transactions_on_transaction_id  (transaction_id)
#  index_product_transactions_on_user_id         (user_id)
#
# Foreign Keys
#
#  fk_rails_71e3ed2a94  (user_id => users.id)
#  fk_rails_a7ba0d1265  (product_id => products.id)
#

class ProductTransaction < ActiveRecord::Base
  belongs_to :user
  belongs_to :product
  validates_presence_of :user_id, :product_id
  scope :with_expiration, -> { where.not(expires_at: nil) }
  validates_presence_of :transaction_id, if: lambda { self.product.can_be_purchase? }
  validates_presence_of :receipt, unless: lambda { self.transaction_id.blank? }
  validates_uniqueness_of :transaction_id, if: lambda { self.product.can_be_purchase? }
end
