# == Schema Information
#
# Table name: follows
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  place_id   :integer
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_follows_on_place_id  (place_id)
#  index_follows_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_1b739c64d4  (place_id => places.id)
#  fk_rails_32479bd030  (user_id => users.id)
#

class Follow < ActiveRecord::Base
  belongs_to :place
  belongs_to :user

  validates :user, uniqueness: { scope: :place, message: "already followed this place." }
end
