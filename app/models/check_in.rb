# == Schema Information
#
# Table name: check_ins
#
#  created_at   :datetime         not null
#  expires_at   :datetime
#  id           :integer          not null, primary key
#  place_id     :integer
#  random_likes :integer
#  shared_likes :integer          default(0)
#  updated_at   :datetime         not null
#  user_id      :integer
#
# Indexes
#
#  index_check_ins_on_place_id  (place_id)
#  index_check_ins_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_85b6121b49  (place_id => places.id)
#  fk_rails_b15c016c97  (user_id => users.id)
#
class CheckIn < ActiveRecord::Base
  MIN_RANDOM_LIKES = 1
  MAX_RANDOM_LIKES = 3

  scope :availability, -> { where("created_at <= expires_at AND expires_at >= ?", DateTime.now).order("created_at DESC").limit(1) }
  scope :currently_checked_in, -> { where("created_at <= expires_at AND expires_at >= ?", DateTime.now).order("created_at DESC") }
  scope :filtered_by_place, ->(place) { where("place_id = ?", place.id)}
  validates_presence_of :user_id, :place_id
  validates :random_likes, numericality: { only_integer: true, greater_than_or_equal_to: MIN_RANDOM_LIKES, less_than_or_equal_to: MAX_RANDOM_LIKES }


  has_many :interactions
  has_many :likes
  has_many :drinks
  belongs_to :user
  belongs_to :place

  def self.available_users
    currently_checked_in.map{|u| u.user }.compact.uniq
  end

  def self.current_check_in_place
    self.availability.first.place
  end

  def self.valid_share_count?(share_count)
    (1..10).include?(share_count)
  end

  def shareable?
    self.shared_likes == 0
  end

  before_create do
    self.expires_at = DateTime.now + 8.hours
  end

  after_create do
    ActivityHandler.new(self).create
    ActivityHandler.new(self.place).create
  end

end
