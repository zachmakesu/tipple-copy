# == Schema Information
#
# Table name: identities
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  provider   :string
#  token      :string
#  uid        :string
#  updated_at :datetime         not null
#  user_id    :integer
#
# Indexes
#
#  index_identities_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_5373344100  (user_id => users.id)
#

class Identity < ActiveRecord::Base
  PROVIDERS = %w{ facebook instagram }
  belongs_to :user

  validates :user, uniqueness: { scope: [:uid,:provider], message: "has already been saved with given uid and provider" }
  validates :user, uniqueness: { scope: :provider, message: "has already been saved with given provider" }
  validates :provider, inclusion: { in: PROVIDERS, message: "%{value} is not recognized as provider" }
  validates :uid, uniqueness: { scope: :provider }
  validates_presence_of :uid, :provider, :token
end
