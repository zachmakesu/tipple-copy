# == Schema Information
#
# Table name: users
#
#  birthdate                       :string
#  created_at                      :datetime         not null
#  current_sign_in_at              :datetime
#  current_sign_in_ip              :inet
#  deleted_at                      :datetime
#  email                           :string           default(""), not null
#  encrypted_password              :string           default(""), not null
#  facebook_url                    :string
#  first_name                      :string
#  gender                          :integer          default(0), not null
#  help                            :boolean          default(FALSE)
#  hide_profile_at                 :datetime
#  hide_profile_with_zero_check_in :boolean          default(FALSE)
#  id                              :integer          not null, primary key
#  image_url                       :string
#  last_name                       :string
#  last_sign_in_at                 :datetime
#  last_sign_in_ip                 :inet
#  notify_activity                 :boolean          default(TRUE)
#  notify_cheers                   :boolean          default(TRUE)
#  notify_drinks                   :boolean          default(TRUE)
#  notify_messages                 :boolean          default(TRUE)
#  prefers_men                     :boolean          default(FALSE)
#  prefers_women                   :boolean          default(FALSE)
#  remember_created_at             :datetime
#  reset_password_sent_at          :datetime
#  reset_password_token            :string
#  role                            :integer          default(0), not null
#  sign_in_count                   :integer          default(0), not null
#  uid                             :string           default(""), not null
#  updated_at                      :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  DEFAULT_DRINK_CREDITS = 5
  include UID

  acts_as_reader
  after_create :generate_user_setting
  devise :database_authenticatable, :omniauthable,
    :recoverable, :rememberable, :trackable, :validatable

  scope :except_self, ->(user) { where("users.id != ? and users.deleted_at IS NULL", user.id)}
  scope :with_total_check_ins_here, -> { select('users.*, (SELECT count(*) from check_ins where check_ins.user_id = users.id)  AS total_check_ins_here') }
  scope :with_total_check_ins_for_place, -> (place_id) { select('users.*, (SELECT count(*) from check_ins where check_ins.user_id = users.id)  AS total_check_ins_here') }
  scope :who_checked_in_here, -> (place_id){ joins(:check_ins).where('check_ins.place_id = ? AND check_ins.created_at <= check_ins.expires_at AND check_ins.expires_at >= ?', place_id ,DateTime.now).order("check_ins.expires_at DESC") }

  enum gender: { not_specified: 0, male: 1, female: 2 }
  enum role: { normal: 0, admin: 1 }

  validates_presence_of     :email
  validates_uniqueness_of   :email

  has_many :follows, dependent: :destroy
  has_many :place_followed, through: :follows, source: :place do
    def recent
      order('follows.created_at DESC')
    end
  end
  has_many :check_ins
  has_many :place_check_ins, through: :check_ins, source: :place
  has_many :conversation_memberships, foreign_key: "member_id"
  has_many :conversations, through: :conversation_memberships
  has_many :photos, as: :imageable, dependent: :destroy

  has_many :transactions, class_name: 'ProductTransaction'
  has_many :products, through: :transactions 

  has_many :api_keys, dependent: :destroy

  has_many :block_lists, dependent: :destroy
  has_many :blocked_profiles, through: :block_lists, source: :blockable, source_type: "User"
  has_many :blocked_places, through: :block_lists, source: :blockable, source_type: "Place"

  has_many :blocked_me_lists, source: :blockable, source_type: "User", class_name: "BlockList", foreign_key: "blockable_id"
  has_many :blocked_me_profiles, through: :blocked_me_lists, class_name: "User", source: :user

  has_many :cheers_with_initiatees, -> { where workflow_state: "cheers" }, class_name: "Interaction", foreign_key: :initiator_id
  has_many :cheers_with_initiators, -> { where workflow_state: "cheers" }, class_name: "Interaction", foreign_key: :initiatee_id
  has_many :contacts_initiatee, through: :cheers_with_initiatees, source: :initiatee
  has_many :contacts_initiator, through: :cheers_with_initiators, source: :initiator

  has_many :devices, dependent: :destroy

  has_many :activity_memberships, foreign_key: :member_id, dependent: :destroy
  has_many :places_activities, through: :activity_memberships, source: :places_activity
  has_many :cheers_check_in_activities, through: :activity_memberships, source: :cheers_check_in_activity
  has_many :tipple_notifications, through: :activity_memberships, source: :tipple_notification

  has_many :identities, dependent: :destroy

  has_many :manual_notifications, dependent: :destroy #User who sent the notif

  scope :all_except, ->(user) { where.not(id: user) }
  scope :not_deleted, -> { where(deleted_at: nil) }

  scope :for_analytics, ->  { joins("LEFT OUTER JOIN follows ON follows.user_id = users.id 
                                     LEFT OUTER JOIN check_ins ON check_ins.user_id = users.id 
                                     LEFT OUTER JOIN interactions ON (interactions.initiator_id = users.id OR interactions.initiatee_id = users.id) AND interactions.workflow_state = 'cheers'")
    .select('users.id, users.first_name,
                                     count(distinct(follows.id)) as followed_place_count, 
                                     count(distinct(check_ins.id)) as check_ins_count,
                                     count(distinct(interactions.id)) as cheers_with_count
  ')
    .group('users.id')
  }

  ATTRIBUTE_METHODS = ["prefers_men", "prefers_women", "notify_drinks", "notify_cheers", "notify_messages", "notify_activity", "hide_profile_with_zero_check_in", "hide_profile_at", "deleted_at"]

  ["prefers_men","prefers_women"].each do |p|
    define_method("is_#{p.downcase}") do
      p.downcase == true
    end
  end

  ATTRIBUTE_METHODS.each do |t|
    define_method("toggle_#{t.downcase}") do
      if(t == "hide_profile_at")
        self.hide_profile_at? ? self.update_attributes(hide_profile_at: nil) : self.update_attributes(hide_profile_at: (DateTime.now + 24.hour).iso8601)
      elsif (t == "deleted_at")
        self.deleted_at? ? self.update_attributes(deleted_at: nil) : self.update_attributes(deleted_at: DateTime.now.iso8601)
      else
        self.send("#{t}?") ? self.update_attribute(t.to_sym, false) : self.update_attribute(t.to_sym, true)
      end

      # if both prefers_men && prefers_women is false, preselect the opposite
      # of the toggled attribute i.e. prefers_men=false, prefers_women=true
      if !prefers_women? && !prefers_men?
        self.update_attribute(:prefers_men, true) if t == 'prefers_women'
        self.update_attribute(:prefers_women, true) if t == 'prefers_men'
      end
    end
  end

  def avatar
    self.photos.find_by(position: 0).try(:image)
  end

  def delete_previous_api_keys
    api_keys.first.delete if api_keys.count > 5
  end

  def active_for_authentication?
    super && self.admin?
  end

  def inactive_message
    "Please enter an Admin account"
  end

  def self.reader_scope
    not_deleted
  end

  def has_read? message
    message.sender_id == self.id || self.have_read?(message)
  end

  def self.search(search)
    where('first_name iLIKE :search OR last_name iLIKE :search OR email iLIKE :search', search: "%#{search}%").order(:first_name)  
  end

  def prefers? (users)
    pass = []
    users.each do | user |
      pass << user if user.preferences.include?(self[:gender]) && self.preferences.include?(user[:gender])
    end
    pass
  end

  def preferences
    g = []
    g << 1 if prefers_men?
    g << 2 if prefers_women?
    if g.empty?
      (male?) ? g << 2 : g << 1
    end
    g
  end

  def age
    if self.birthdate.present?
      now = Time.now.utc.to_date
      dob = self.birthdate.to_date
      now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
    end
  end

  def is_deleted?
    !deleted_at.nil?
  end

  def has_credits?
    credits_left_count > 0
  end

  def credits_left
    if has_unlimited_likes?
      "Unlimited"
    else
      credits_left_count.to_s
    end
  end

  def credits_left_count
    likes_purchased = self.products.map { |entry| entry.value }.sum
    check_in_likes = self.check_ins.map {|check_in| check_in.random_likes + check_in.shared_likes }.sum
    total_likes = likes_purchased + check_in_likes
    DEFAULT_DRINK_CREDITS + (total_likes - used_credits) #total_people_i_liked minus people_i_liked when im in unlimited subscription.
  end

  def used_credits
    group1 = self.interactions.joins(:histories).where('(interaction_histories.state_changed_to = ? AND interaction_histories.state_from = ?)', 'drink', 'like').count
    entries = self.transactions.with_expiration
    group2 = entries.map { |entry| self.interactions.joins(:histories).all_likes_history.within(entry.created_at,entry.expires_at).count }.sum
    group1 - group2
  end

  def total_cheers
    self.interactions.joins(:histories).all_cheers_history.count
  end

  def interactions
    Interaction.where("initiator_id = :id OR initiatee_id = :id",id: self.id)
  end

  def interaction_status_with(current_user,place)
    last_interaction = self.interactions.where("(initiator_id = :id OR initiatee_id = :id) AND workflow_state = 'cheers'",id: current_user).last
    last_interaction || self.interactions.where("(initiator_id = :id OR initiatee_id = :id) AND place_id = :place",id: current_user,place: place).last
  end

  def likers
    interactions.includes(:place).drinks.active.map{|interaction| interaction.histories.received_by(self.id).latest.first }.compact
  end

  def cheers_with
    self.contacts_initiatee + self.contacts_initiator
  end

  def activities
    cheers_check_ins    = self.cheers_check_in_activities.map{|a| a unless self.blacklist.include?(a.check_in_user)}.compact
    place_notifications = self.places_activities.map{|a| a unless self.blocked_places.include?(a.place)}.compact

    (cheers_check_ins+place_notifications+tipple_notifications).sort{|a, b| b.created_at <=> a.created_at}
  end

  def currently_checked_into(place_id)
    !check_ins.availability.where(place_id: place_id).empty?
  end

  def currently_checked_in_at?(place_id)
    self.check_ins.availability.where(place_id: place_id).last.present?
  end

  def check_ins_at(place_id)
    self.check_ins.where(place_id: place_id)
  end

  def has_checked_in_at?(place_id)
    !check_ins_at(place_id).count.zero?
  end

  def blocked_by?(uid)
    self.blocked_me_profiles.map(&:uid).include?(uid)
  end

  def blocked?(uid)
    self.blocked_profiles.map(&:uid).include?(uid)
  end

  def blacklist
    (self.blocked_me_profiles + self.blocked_profiles).compact.uniq
  end

  def facebook_token
    self.identities.find_by(provider: 'facebook').try(:token)
  end

  def instagram_uid
    self.identities.find_by(provider: 'instagram').try(:uid)
  end

  def facebook_uid
    self.identities.find_by(provider: 'facebook').try(:uid)
  end

  def unnotify_by_place?(place)
    self.place_followed.include?(place) ? self.blocked_places.include?(place) : false 
  end

  def mutual_friends_with(user)
    return [] if facebook_token.nil? || user.facebook_token.nil?
    begin
    @graph = Koala::Facebook::API.new(facebook_token)
    [].tap do |mutual_friends|
      @graph.batch do |batch_api|
        context_id = @graph.get_object(user.facebook_uid, {fields: ["context"]}) { |data| data["context"]["id"] if data["context"]["id"]}
        mutual_friends << @graph.get_object("#{context_id}/mutual_friends", { fields: ['first_name'] })
      end
    end.flatten
    rescue Koala::Facebook::APIError => e
      []
    end
  end

  private
  def has_unlimited_likes?
    self.transactions.with_expiration.any? do |entry|
      entry.expires_at >= DateTime.now
    end
  end

  def generate_user_setting
    self.male? ? self.update_attributes(prefers_women: true) : self.update_attributes(prefers_men: true)  
  end

  after_create do
    self.photos.create(position: 0, image: URI.parse(self.image_url)) unless self.photos.count > 0 || self.image_url.nil?
  end
end
