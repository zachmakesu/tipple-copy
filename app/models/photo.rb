# == Schema Information
#
# Table name: photos
#
#  caption            :string
#  created_at         :datetime         not null
#  default            :boolean          default(FALSE)
#  default_type       :integer
#  id                 :integer          not null, primary key
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string
#  position           :integer
#  updated_at         :datetime         not null
#

class Photo < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true

  has_attached_file :image, styles: { xlarge: "1000x1000>" , large: "800x800>", medium: "300x300>", thumb: "100x100>" }, default_url: "/images/missing.png"
  validates_attachment_content_type :image,:content_type => /^image\/(jpg|jpeg|png)$/, :message => 'not allowed.'

  validates :image, presence: {:on => :create} 
  validates :position, presence: true
  validates :position, numericality: { less_than: 5 }, if: Proc.new { |p| p.imageable_type == 'User' }
  validates :position, numericality: { only_integer: true }, if: Proc.new { |p| p.imageable_type == 'Place' }
  validates :position, uniqueness: {:on => :create, scope: [:imageable_type, :imageable_id, :default] } 

  enum default_type: { men: 0, women: 1 , place: 2}

  def self.default
    find_by(default: true).image unless find_by(default: true).nil?
  end

  def self.default_photo_men
    where(default: true, default_type: 0).first
  end

  def self.default_photo_women
    where(default: true, default_type: 1).first
  end

  def self.default_photo_place
    where(default: true, default_type: 2).first
  end

  def set_default_primary
    self.position = 0 
  end
  
  before_create do
    set_default_primary if !self.imageable.nil? && self.imageable.photos.count <= 0
  end

  before_validation do
    if self.imageable_type == "User" && User.find(self.imageable_id).photos(:reload).count >= 6
      self.errors.add(:base, "photos should be maximum of 5")
    end
    self.errors.add(:base, "Default Photo is already exist") if default.nil?
  end

end
