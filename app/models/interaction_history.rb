# == Schema Information
#
# Table name: interaction_histories
#
#  created_at       :datetime         not null
#  has_unlimited    :boolean          default(FALSE)
#  id               :integer          not null, primary key
#  interaction_id   :integer
#  recipient_id     :integer
#  sender_id        :integer
#  state_changed_to :string
#  state_from       :string
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_interaction_histories_on_interaction_id  (interaction_id)
#
# Foreign Keys
#
#  fk_rails_da8974d5cc  (interaction_id => interactions.id)
#

class InteractionHistory < ActiveRecord::Base
  scope :all_cheers, -> { where(state_changed_to: "cheers") }
  scope :all_likes, -> { where(state_changed_to: "drink") }
  scope :used_likes, -> (user) { where("sender_id = ? AND has_unlimited = FALSE AND (state_changed_to = 'drink') ", user.id) }
  scope :they_liked_you, -> (user) { where("recipient_id = ? AND (state_changed_to = 'drink') ", user.id) }
  scope :within, -> (start_date, end_date){  where(interaction_histories: {created_at: start_date..end_date}) }
  scope :get_likers, -> (id) { where("recipient_id = ? AND (state_changed_to = 'drink')", id).order(updated_at: :desc) }
  scope :latest, -> { order(created_at: :desc) }
  scope :received_by, -> (user_id) { where('recipient_id = ?', user_id) }

  belongs_to :interaction
  belongs_to :sender,    class_name: "User", foreign_key: :sender_id
  belongs_to :recipient, class_name: "User", foreign_key: :recipient_id
end
