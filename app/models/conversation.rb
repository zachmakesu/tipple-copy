# == Schema Information
#
# Table name: conversations
#
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  interaction_id :integer
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_conversations_on_interaction_id  (interaction_id)
#
# Foreign Keys
#
#  fk_rails_601a3fdcae  (interaction_id => interactions.id)
#

class Conversation < ActiveRecord::Base
  has_many :messages
  has_many :memberships, class_name: "ConversationMembership", dependent: :destroy
  has_many :members, through: :memberships
  belongs_to :interaction

  validates :interaction_id, uniqueness: true
end
