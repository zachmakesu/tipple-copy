# == Schema Information
#
# Table name: conversation_memberships
#
#  conversation_id :integer
#  created_at      :datetime         not null
#  id              :integer          not null, primary key
#  member_id       :integer
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_conversation_memberships_on_conversation_id  (conversation_id)
#
# Foreign Keys
#
#  fk_rails_67a38991f3  (conversation_id => conversations.id)
#

class ConversationMembership < ActiveRecord::Base
  belongs_to :member, class_name: "User", foreign_key: "member_id"
  belongs_to :conversation

  validates :member_id, uniqueness: { scope: :conversation_id }

  before_validation do
    if self.conversation.members(:reload).count == 2
      self.errors.add(:base, "Too much members in conversation")
    end
  end

end
