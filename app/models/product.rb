# == Schema Information
#
# Table name: products
#
#  ability           :integer
#  created_at        :datetime         not null
#  description       :text
#  duration          :integer
#  expires_at        :datetime
#  icon_content_type :string
#  icon_file_name    :string
#  icon_file_size    :integer
#  icon_updated_at   :datetime
#  id                :integer          not null, primary key
#  name              :string
#  price             :float
#  published_at      :datetime
#  updated_at        :datetime         not null
#  value             :integer
#

class Product < ActiveRecord::Base
  has_attached_file :icon, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/missing.png"
  validates_attachment_content_type :icon, content_type: /\Aimage\/.*\Z/

  enum ability: { can_be_purchase: 0, can_be_share: 1 }
  enum duration: { not_applicable: 0, day: 1, week: 2, month: 3 }
  
  scope :available_products, -> { where("published_at is NOT NULL and ( expires_at is NUll OR expires_at < ?)", DateTime.now) }

  def recurring?
    day? || week? || month?
  end
end
