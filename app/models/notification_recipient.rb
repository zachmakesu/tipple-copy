# == Schema Information
#
# Table name: notification_recipients
#
#  created_at             :datetime         not null
#  id                     :integer          not null, primary key
#  manual_notification_id :integer
#  recipient_id           :integer
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_notification_recipients_on_manual_notification_id  (manual_notification_id)
#
# Foreign Keys
#
#  fk_rails_dc51073824  (manual_notification_id => manual_notifications.id)
#

class NotificationRecipient < ActiveRecord::Base
  belongs_to :manual_notification
  belongs_to :user, class_name: "User", foreign_key: "recipient_id"

  validates :recipient_id, uniqueness: { scope: :manual_notification_id }
end
