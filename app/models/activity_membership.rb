# == Schema Information
#
# Table name: activity_memberships
#
#  activity_id :integer
#  created_at  :datetime         not null
#  id          :integer          not null, primary key
#  member_id   :integer
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_activity_memberships_on_activity_id  (activity_id)
#
# Foreign Keys
#
#  fk_rails_c61ac4ee0e  (activity_id => activities.id)
#

class ActivityMembership < ActiveRecord::Base
  belongs_to :places_activity, class_name: "Activities::PlacesActivity", foreign_key: :activity_id
  belongs_to :cheers_check_in_activity, class_name: "Activities::CheersCheckInActivity", foreign_key: :activity_id
  belongs_to :likers_activity, class_name: "Activities::LikersActivity", foreign_key: :activity_id
  belongs_to :tipple_notification, class_name: "Activities::TippleNotificationsActivity", foreign_key: :activity_id
  belongs_to :member, class_name: "User", foreign_key: :member_id
end
