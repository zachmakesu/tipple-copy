class NotificationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_notification, only: [:show]

  def index
    @notifications = if params[:search]
      ManualNotification.search(params[:search]).order("message ASC")
    else
      ManualNotification.order('message ASC')
    end
    @create_path = notifications_path
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @notifications }
    end
  end

  def new
    @notification = ManualNotification.new
    respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @notification }
      end
  end

  def create
    @notification = current_user.manual_notifications.build(notification_params)
    respond_to do |format|
      if @notification.save
        recipients = params[:manual_notification][:recipients]
        if recipients.present? && @notification.send_to == "specific_users"
          recipients.split(",").each { |recipient| @notification.notification_recipients.create(recipient_id: recipient) }
        end
        send_notification
        format.html { redirect_to notifications_path, notice: 'Notification was successfully sent.' }
        format.json { render json: @notification, status: :created, location: @notification }
      else
        format.html { render action: "new" }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    respond_to do |format|
      format.html { render nothing: true }
      format.js 
    end
  end

  def ajax_notifications
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: NotificationsDatatableHandler.new(view_context) }
    end
  end

  private

  def set_notification
      @notification = ManualNotification.find(params[:id])
  end

  def notification_params
    params.require(:manual_notification).permit(:message, :send_to)
  end

  def send_notification
    @users =  case @notification.send_to
    when "all_males"
      User.male.not_deleted
    when "all_females"
      User.female.not_deleted
    when "specific_users"
      recepient_ids ||= []
      recepient_ids = @notification.recipients.map(&:id) if @notification.recipients.present?
      User.where(id: recepient_ids).not_deleted
    when "all_users"
      User.not_deleted
    else
      User.not_deleted
    end
    
    @users.each do |user|
      Notifications::ManualNotificationWorker.perform_async(user.id, @notification.id)
    end
  end
end
