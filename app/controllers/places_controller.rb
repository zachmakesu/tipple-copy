class PlacesController < ApplicationController
  load_and_authorize_resource
  check_authorization
  before_action :authenticate_user!
  before_action :set_place, only: [:show, :edit, :update, :delete, :delete_photo]

  def index
    @places = if params[:search] 
      Place.search(params[:search]).order("name ASC")
    else
      Place.order('name ASC')
    end
    @create_path = places_path
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @places }
    end
  end

  def new
    @place = Place.new
    respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @place }
      end
  end

  def create
    respond_to do |format|
      if @place.save
        if params[:place][:add_photo].present?
          params[:place][:add_photo].each do | photo |
            @place.photos.create(image: photo, position: @place.photos.count + 1)
          end
        end
        format.html { redirect_to places_path, notice: 'Place was successfully created.' }
        format.json { render json: @place, status: :created, location: @place }
      else
        format.html { render action: "new" }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @place.update(place_params)
        params[:place][:photos].each_with_index { |v, k| @place.photos.find(v).update(position: k)} if params[:place][:photos].present?
        
        if params[:place][:add_photo].present?
          params[:place][:add_photo].each do | photo |
            @place.photos.create(image: photo, position: @place.photos.count + 1)
          end
        end

        format.html { redirect_to places_path, notice: 'Place was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    respond_to do |format|
      format.html { render nothing: true }
      format.js 
    end
  end

  def destroy
    @place.destroy
    respond_to do |format|
        format.html { redirect_to places_path }
        format.json { head :no_content }
      end
  end

  def availability
    @place.deleted_at.present? ? @place.update(deleted_at: nil) : @place.update(deleted_at: DateTime.now)
    respond_to do |format|
      format.html { render nothing: true }
      format.js 
    end
  end

  def delete_photo
    @photo = @place.photos.find(params[:photo_id])
    @photo.destroy
    @place.photos.order(position: :asc).map(&:id).each_with_index { |v, k| @place.photos.find(v).update(position: k) }
    respond_to do |format|
      format.html { redirect_to ponies_url }
      format.json { head :no_content }
      format.js   { render :layout => false }
    end
  end

  def ajax_places
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: PlacesDatatableHandler.new(view_context) }
    end
  end

  private

  def set_place
      @place = Place.find(params[:id])
    end

  def place_params
    params[:place][:photos] ||= []
    params[:place][:add_photo] ||= []
    params.require(:place).permit(:name, :address, :description, :lat, :lng)
  end
end
