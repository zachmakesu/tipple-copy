class AnalyticsController < ApplicationController
  before_action :authenticate_user!
  
  def index
    
  end

  def ajax_analytics_place
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: PlacesAnalyticsDatatableHandler.new(view_context) }
    end
  end

  def ajax_analytics_user
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: UsersAnalyticsDatatableHandler.new(view_context) }
    end
  end

  def ajax_analytics_places_followed
    @user = User.find(params[:id])
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: PlacesFollowedAnalyticsDatatableHandler.new(view_context, @user) }
    end
  end

end
