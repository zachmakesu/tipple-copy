class UsersController < ApplicationController
  load_and_authorize_resource
  check_authorization
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :delete, :delete_photo, :followed_places]

  def index 
    @users = if params[:search]
      User.all_except(current_user).search(params[:search]).order("first_name ASC")
    else
      User.all_except(current_user).order('first_name ASC')
    end
    @create_path = users_path 
    respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @user }
      end
  end


  def new
    @user = User.new
    respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @user }
      end
  end

  def create
        respond_to do |format|
        if @user.save
          @user.photos.create(image: params[:user][:add_photo], position: @user.photos.count + 1) if params[:user][:add_photo].present?
          format.html { redirect_to users_path, notice: 'User was successfully created.' }
          format.json { render json: @user, status: :created, location: @user }
        else
          format.html { render action: "new" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
  end

  def edit
    
  end

  def update
    if user_params[:password].blank?
      update_params = user_params_without_password
    else
      update_params = user_params
    end
    if @user.birthdate.present? 
      update_params[:birthdate] = update_params[:birthdate] + @user.birthdate[10..-1]
    else
      update_params[:birthdate] = update_params[:birthdate] + DateTime.now.to_s[10..-1]
    end

    respond_to do |format|
        if @user.update(update_params)
          params[:user][:photos].each_with_index { |v, k| @user.photos.find(v).update(position: k) } if params[:user][:photos].present?
          @user.photos.create(image: params[:user][:add_photo], position: @user.photos.count) if params[:user][:add_photo].present?
          format.html { redirect_to users_path, notice: 'User was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: "edit" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
  end

  def show
    respond_to do |format|
      format.html { render nothing: true }
      format.js 
    end
  end

  def destroy
    @user.update(deleted_at: DateTime.now)
    respond_to do |format|
        format.html { redirect_to users_path }
        format.json { head :no_content }
      end
  end

  def availability
    @user.deleted_at.present? ? @user.update(deleted_at: nil) : @user.update(deleted_at: DateTime.now)
    respond_to do |format|
      format.html { render nothing: true }
      format.js 
    end
  end

  def delete_photo
    @photo = @user.photos.find(params[:photo_id])
    @photo.destroy
    @user.photos.order(position: :asc).map(&:id).each_with_index { |v, k| @user.photos.find(v).update(position: k) }
    respond_to do |format|
      format.html { redirect_to ponies_url }
      format.json { head :no_content }
      format.js   { render :layout => false }
    end
  end

  def ajax_users
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: UsersDatatableHandler.new(view_context) }
    end
  end

  def followed_places
    respond_to do |format|
      format.html { render nothing: true }
      format.js 
    end
  end

  def user_list
    @users = User.not_deleted.all.where("email iLike ?", "%#{params[:q]}%").order(email: "ASC").limit(5)
    render json: @users.map { |hash|  { id: hash[:id] ,email: hash[:email] } }
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params[:user][:photos] ||= []
    params[:user][:add_photo] ||= nil
    params.require(:user).permit(:email, :first_name, :last_name, :gender, :birthdate, :avatar, :role, :password, :password_confirmation)
  end

  def user_params_without_password
    params[:user][:photos] ||= []
    params[:user][:add_photo] ||= nil
    params.require(:user).permit(:email, :first_name, :last_name, :gender, :birthdate, :avatar, :role)
  end
end
