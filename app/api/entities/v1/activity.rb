module Entities
  module V1
    module Activity

      class Place < Grape::Entity
        expose :id, :name, :current_people
        expose :background, format_with: :original_photo_url

        def id
          object.place.id
        end
        def name
          object.place.name
        end
        def background
          object.place.background
        end
        def current_people
          object.current_check_in.people_count
        end
      end

      class CheckIn < Grape::Entity
        expose :id, :user_uid, :user_first_name, :user_last_name, :place_id, :place_name
        expose :user_avatar, format_with: :original_photo_url

        def id
          object.check_in.id
        end
        def user_uid
          object.check_in_user.uid
        end
        def place_id
          object.check_in_place.id
        end
        def user_first_name
          object.check_in_user.first_name
        end
        def user_last_name
          object.check_in_user.last_name
        end
        def user_avatar
          object.check_in_user.avatar
        end
        def place_name
          object.check_in_place.name
        end
      end

      class Activities < Grape::Entity
        expose :created_at, format_with: :utc
        expose :type
        expose :place,               using: Place,   if: lambda { |activity, options| activity.place? } 
        expose :check_in,            using: CheckIn, if: lambda { |activity, options| activity.cheers_check_in? } 

        def interaction_history
          object
        end
        def place
          object
        end
        def check_in
          object
        end
        def type
          object.activity_type.gsub("Activities::",'')
        end
      end

      class Index < Grape::Entity
        expose :data, using: Activities
        def data
          #          eager_variables = []
          #
          #          activity_ids = object.activities.includes(:sender,:check_in_user).map do |activity|
          #            if activity.likers?
          #              activity.id unless object.blacklist.include?(activity.sender)
          #            elsif activity.cheers_check_in?
          #              activity.id unless object.blacklist.include?(activity.check_in_user)
          #            else
          #              activity.id
          #            end
          #          end
          #
          #          all_activities = object.activities.find(activity_ids)
          #
          #          all_activities.map(&:activity_type).uniq.each do |a|
          #            eager_variables << [:interaction_history, :sender, :liker_place]  if a == "Activities::LikersActivity"
          #            eager_variables << [:check_in, :check_in_user, :check_in_place]   if a == "Activities::CheersCheckInActivity"
          #            eager_variables << [:place, :current_check_in]                    if a == "Activities::PlacesActivity"
          #          end
          #
          #          object.activities.where(id: [activity_ids]).includes(eager_variables)

          #          filtered_activities = object.activities.includes(eager_variables).map do |activity|
          #            if activity.likers? 
          #              activity unless object.blacklist.include?(activity.sender)
          #            elsif activity.cheers_check_in?
          #              #activity.check_in_place #to prevent unused eager loading
          #              activity unless object.blacklist.include?(activity.check_in_user)
          #            else
          #              activity
          #            end
          #          end
          #          filtered_activities.compact

          object.activities
        end
      end

    end
  end
end
