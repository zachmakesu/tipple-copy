module Entities
  module V1
    module Product

      class ProductsInfo < Grape::Entity
        expose :id,:name, :value, :price, :duration
      end

      class Index < Grape::Entity
        expose  :data do  
          expose :credits_left
          expose :credits_left_count
          expose :tipple_products, using: ProductsInfo
        end

        private

        def tipple_products
          object.available_products.can_be_purchase.order(price: :asc)
        end

        def credits_left
          options[:user].credits_left
        end

        def credits_left_count
          options[:user].credits_left_count
        end

      end

    end
  end
end
