module Entities
  module V1
    module Conversation

      class MessageSender < Grape::Entity
        expose :uid,:first_name, :age
        expose :avatar, format_with: :original_photo_url
        def age
          object.age
        end
      end

      class MessageView < Grape::Entity
        format_with(:iso_timestamp) { |dt| dt.iso8601 }

        expose :sender, using: MessageSender
        expose :body
        expose :is_read

        with_options(format_with: :iso_timestamp) do
          expose :created_at, as: :sent_at
        end

        private
        def is_read
          options[:current_user].has_read?(object)         
        end
      end

      class UserDetails < MessageSender
        expose :last_check_in_place,:current_check_in_place

        def last_check_in_place
          object.check_ins.present? ? object.check_ins.last.place.name : ""
        end
        def current_check_in_place
          object.check_ins.availability.present? ? object.check_ins.availability.last.place.name : nil
        end
      end

      class CheersDetails < Grape::Entity
        expose :place
        expose :created_at, as: :cheers_at, format_with: :utc
        expose :place_id

        def place
          object.interaction.place.name
        end

        def place_id
          object.interaction.place.id
        end
      end

      class Conversation < Grape::Entity
        expose :id
        expose :dynamic_initiatee do
          expose :last_check_in_place
          expose :current_check_in_place
          expose :uid, :first_name, :age
          expose :avatar, format_with: :original_photo_url
        end
        expose :message, using: MessageView

        private
        def last_check_in_place
          object.interaction.place.name
        end
        def current_check_in_place
          initiatee.check_ins.availability.present? ? initiatee.check_ins.availability.last.place.name : nil
        end
        def initiatee
          object.members.all_except(options[:current_user]).first
        end
        def uid
          initiatee.uid
        end
        def first_name
          initiatee.first_name
        end
        def age
          initiatee.age
        end
        def avatar
          initiatee.avatar
        end
        def message
          object.messages.last
        end
      end

      class Lists < Grape::Entity
        expose :data, using: Conversation

        def data
          object.conversations.reject{|conversation| !(conversation.members & object.blocked_profiles).empty? }
        end
      end

      class FullConversation < Grape::Entity
        expose :data do
          expose :blocked 
          expose :dynamic_initiatee, using: MessageSender
          expose :cheers, using: CheersDetails
          expose :messages, using: MessageView
        end
        def dynamic_initiatee
          object.members.all_except(options[:current_user]).first
        end
        def cheers
          object.interaction.histories.last
        end
        def messages
          object.messages.oldest.includes(:sender)
        end

        def blocked
          options[:current_user].blacklist.include?(object.members.all_except(options[:current_user]).first)
        end
      end

    end
  end
end
