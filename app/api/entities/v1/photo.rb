module Entities
  module V1
    module Photo
      class Attr < Grape::Entity
        expose :id, :caption, :position
        expose :photo, format_with: :original_photo_url
        def photo
          object.image
        end
      end
    end
  end
end