module Entities
  module V1
    module ShareTipple

      class ShareTippleInfo < Grape::Entity
        expose :id, :value, :description, :icon, :is_shared
        
        def icon
          URI.join(ActionController::Base.asset_host, object.icon.url).to_s
        end

        def is_shared
           options[:user].products.can_be_share.map(&:id).include?(object.id)
        end
      end

      class Index < Grape::Entity
        expose  :data do
          expose :shared_count , :shareables_count, :total_likes_acquired    
          expose :share_tipple_list, using: ShareTippleInfo
        end

        def share_tipple_list
          object.can_be_share
        end

        def shared_count
           options[:user].products.can_be_share.count
        end

        def shareables_count
          object.can_be_share.count
        end

        def total_likes_acquired
          options[:user].products.can_be_share.map { |s| s.value }.sum
        end   

      end

    end
  end
end