module Entities
  module V1
    module Place
      class Followed < Grape::Entity
        expose :name, :id
        expose :background, format_with: :original_photo_url
      end
      class PlaceListing < Grape::Entity

        expose :id, :name, :lat, :lng, :followers_count, :following,:unnotify, :check_ins, :available_check_ins
        expose :background, format_with: :original_photo_url
        expose :my_check_in_ends_on, format_with: :utc

        private
        def available_check_ins #show available check in user based on User's Preference
          currently_checked_in = options[:user].prefers?(object.check_ins.includes(:user).available_users)
          ((currently_checked_in - options[:user].blacklist)).compact.uniq.count
        end
        def check_ins #show all people based on User's Preference who checked_in the place
          total_unique_check_ins = options[:user].prefers?(object.user_check_ins.except_self(options[:user]))
          ((total_unique_check_ins - options[:user].blacklist)).compact.uniq.count
        end

        def my_check_in_ends_on
          duration = options[:user].check_ins.availability.where(place_id: object.id)
          (!duration.empty?) ? duration.last.expires_at : nil
        end

        def following #boolean check is current user is following the place
          options[:user].place_followed.not_deleted.map(&:id).include?(object.id)
        end

        def unnotify
          options[:user].unnotify_by_place?(object)
        end

        def followers_count #followers based on User's Preference
          male, female = 0, 0
          total_unique_followers = options[:user].prefers?(object.followers.except_self(options[:user]))
          unfiltered_followers = ((total_unique_followers - options[:user].blacklist)).compact.uniq
          unfiltered_followers.select!{|follower| follower.has_checked_in_at?(object.id)} if options[:user].hide_profile_with_zero_check_in 
          unfiltered_followers.each do |user|
            male+=1 if user.male?
            female+=1 if user.female?
          end
          counts = {}
          counts[:female] = female
          counts[:male] = male
          counts
        end

      end

      class Index < Grape::Entity
        expose :data, using: PlaceListing
        private
        def data
          object.not_deleted.get_near_places(options[:params][:lat],options[:params][:lng])
        end
      end

      class Search < Grape::Entity
        expose :data, using: PlaceListing
        private
        def data
          object.not_deleted.search(options[:params][:query])
        end
      end

      class Follower < Grape::Entity
        expose :followers, as: :data , using: Entities::V1::User::UserData

        def followers 
          unfiltered_followers = if options[:user].currently_checked_in_at?(object.id)
                                   currently_checked_in = options[:user].prefers?(object.followers.who_checked_in_here(object.id).except_self(options[:user]).with_total_check_ins_here)
                                   remaining_followers = options[:user].prefers?(object.followers.except_self(options[:user]).with_total_check_ins_for_place(options[:place].id)).sort_by { |u| u.check_ins.where(place_id: options[:place].id).count }.reverse
                                   ((currently_checked_in + remaining_followers) - options[:user].blacklist).compact.uniq 
                                 else
                                   total_unique_followers = options[:user].prefers?(object.followers.except_self(options[:user]).with_total_check_ins_for_place(options[:place].id)).sort_by { |u| u.check_ins.where(place_id: options[:place].id).count }.reverse
                                   ((total_unique_followers - options[:user].blacklist)).compact.uniq 
                                 end
          return unfiltered_followers unless options[:user].hide_profile_with_zero_check_in 
          unfiltered_followers.select{|follower| follower.has_checked_in_at?(options[:place].id)}
        end

      end

      class Get < Grape::Entity
        expose :data do
          expose :id, :name , :available_check_ins, :address, :following, :unnotify, :total_check_ins, :total_cheers, :followers_count, :description
          expose :background, format_with: :original_photo_url
          expose :photos, using: Entities::V1::Photo::Attr
          expose :my_check_in_ends_on, format_with: :utc
          expose :lat, :lng
        end

        private
        def following #boolean check is current user is following the place
          options[:user].place_followed.not_deleted.map(&:id).include?(object.id)
        end

        def followers_count #followers based on User's Preference
          male, female = 0, 0
          total_unique_followers = options[:user].prefers?(object.followers.except_self(options[:user]))
          unfiltered_followers = ((total_unique_followers - options[:user].blacklist)).compact.uniq
          unfiltered_followers.select!{|follower| follower.has_checked_in_at?(object.id)} if options[:user].hide_profile_with_zero_check_in 
          unfiltered_followers.each do |user|
            male+=1 if user.male?
            female+=1 if user.female?
          end
          counts = {}
          counts[:female] = female
          counts[:male] = male
          counts
        end

        def total_cheers
          object.interaction_histories.all_cheers.count
        end

        def total_check_ins
          total_unique_check_ins = options[:user].prefers?(object.user_check_ins.except_self(options[:user]))
          ((total_unique_check_ins - options[:user].blacklist)).compact.uniq.count
        end

        def available_check_ins
          currently_checked_in = options[:user].prefers?(object.check_ins.includes(:user).available_users)
          ((currently_checked_in - options[:user].blacklist)).compact.uniq.count
        end

        def photos
          object.photos.order(position: :asc)
        end

        def my_check_in_ends_on
          duration = options[:user].check_ins.availability.where(place_id: object.id)
          (!duration.empty?) ? duration.last.expires_at : nil
        end

        def unnotify
          options[:user].unnotify_by_place?(object)
        end
      end

      class FollowedPlaces < Grape::Entity
        expose :data, using: PlaceListing
        private
        def data
          object.place_followed.not_deleted
        end
      end

    end
  end
end
