module Entities
  module V1
    module User

      class InteractionStatus < Grape::Entity
        expose :id, :conversation_id, :state
        expose :initiator_uid, :initiatee_uid
        private
        def conversation_id
          object.conversation.id if object.cheers?
        end
        def state
          object.workflow_state
        end
        def initiator_uid
          object.initiator.uid
        end
        def initiatee_uid
          object.initiatee.uid
        end
      end

      class Interaction < Grape::Entity
        expose :data, using: InteractionStatus
        private
        def data
          object
        end
      end

      class UserData < Grape::Entity
        expose :uid, :first_name, :last_name, :gender, :age, :total_check_ins_here, :current_check_in_place_id, :current_check_in_place_name
        expose :avatar, as: :profile_image, format_with: :original_photo_url
        expose :current_check_in_place_background, format_with: :original_photo_url
        expose :interactions, using: InteractionStatus
        expose :current_check_in_place_ends_on, format_with: :utc
        expose :id
        expose :profile_image, format_with: :original_photo_url
        private
        def age
          object.age
        end

        def current_check_in_place_id
          (!object.check_ins.availability.empty?) ? object.check_ins.availability.last.place.id : nil
        end

        def current_check_in_place_name
          (!object.check_ins.availability.empty?) ? object.check_ins.availability.last.place.name : nil
        end

        def current_check_in_place_ends_on
          (!object.check_ins.availability.empty?) ? object.check_ins.availability.last.expires_at : nil
        end

        def current_check_in_place_background
          (!object.check_ins.availability.empty?) ? object.check_ins.availability.last.place.background : nil
        end

        def interactions
          object.interaction_status_with(options[:user], options[:place])
        end

        def total_check_ins_here
          object.check_ins.filtered_by_place(options[:place]).count
        end

        def profile_image
          object.avatar
        end
      end

      class BlockedUserData < Grape::Entity
        expose :uid, :first_name, :last_name, :gender
        expose :profile_image, format_with: :original_photo_url

        private
        def profile_image
          object.avatar
        end
      end

      class GetUserProfile < Grape::Entity
        expose :data do
          expose :place_id, :place_name ,:first_name,:last_name, :age, :total_check_ins_here, :total_check_ins, :mutual_place_followed, :total_place_followed, :current_check_in_place_name, :mutual_friends, :uid
          expose :interactions, using: InteractionStatus
          expose :photos, using: V1::Photo::Attr
          expose :id, :gender
          expose :profile_image, format_with: :original_photo_url
          expose :instagram_uid
          expose :followed_places, using: Entities::V1::Place::Followed
        end
        private
        def followed_places
          object.place_followed.recent
        end

        def mutual_friends
          options[:user].mutual_friends_with(object)
        end

        def place_id
          options[:place].id
        end

        def place_name
          options[:place].name
        end

        def age
          object.age
        end

        def interactions
          object.interaction_status_with(options[:user], options[:place])
        end

        def total_check_ins_here
          object.check_ins.filtered_by_place(options[:place]).count
        end

        def total_check_ins
          object.check_ins.count
        end

        def mutual_place_followed
          (object.follows.map(&:place_id) & options[:user].follows.map(&:place_id)).count
        end

        def total_place_followed
          object.follows.count
        end

        def current_check_in_place_name
          (!object.check_ins.availability.empty?) ? object.check_ins.availability.last.place.name : nil
        end

        def photos
          object.photos.order(position: :asc)
        end

        def profile_image
          object.avatar
        end
      end

      class Dashboard < Grape::Entity
        expose :data do
          expose :uid, :first_name, :last_name, :gender, :age, :total_check_ins, :total_place_followed, :total_cheers, :current_check_in_place_id, :current_check_in_place_name
          expose :avatar, as: :profile_image, format_with: :original_photo_url
          expose :current_check_in_place_background, format_with: :original_photo_url
          expose :current_check_in_place_ends_on, format_with: :utc
          expose :credits_left
        end

        private
        def credits_left
          object.credits_left
        end

        def age
          object.age
        end

        def current_check_in_place_background
          object.check_ins.availability.last.place.background unless (object.check_ins.availability.empty?)
        end

        def current_check_in_place_id
          (!object.check_ins.availability.empty?) ? object.check_ins.availability.last.place.id : nil
        end

        def current_check_in_place_name
          (!object.check_ins.availability.empty?) ? object.check_ins.availability.last.place.name : nil
        end

        def current_check_in_place_ends_on
          (!object.check_ins.availability.empty?) ? object.check_ins.availability.last.expires_at : nil
        end

        def total_check_ins
          object.check_ins.count
        end

        def total_place_followed
          object.follows.count
        end

        def total_cheers
          object.total_cheers
        end
      end

      class Likers < Grape::Entity
        expose :place_background, format_with: :original_photo_url
        expose :place_name, :first_name, :last_name, :age, :total_check_ins
        expose :interactions, using: InteractionStatus
        expose :place_id
        expose :avatar, format_with: :original_photo_url
        expose :liked_at, format_with: :utc

        private
        def avatar
          object.sender.avatar
        end

        def place_background #background display
          object.interaction.place.background
        end

        def place_id
          object.interaction.place.id
        end

        def place_name
          object.interaction.place.name
        end

        def liked_at
          object.created_at
        end

        def first_name
          object.sender.first_name
        end

        def last_name
          object.sender.last_name
        end

        def age
          object.sender.age
        end

        def total_check_ins
          object.sender.check_ins.filtered_by_place(object.interaction.place).count
        end

        def interactions
          object.sender.interaction_status_with(options[:user], object.interaction.place)
        end
      end

      class LikersPage < Grape::Entity
        expose :data, using: Likers
        def data
          options[:likers]
        end
      end
    end
  end
end
