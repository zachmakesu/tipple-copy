module Entities
  module V1
    module UserSetting
      class Index < Grape::Entity
        expose :data do
          expose :uid, :prefers_men, :prefers_women, :notify_drinks, :notify_cheers, :notify_messages, :notify_activity, :hide_profile_with_zero_check_in
          expose :hide_profile_at, :deleted_at, format_with: :utc
        end
      end

      class BlockedList < Grape::Entity
        expose :data, using: Entities::V1::User::BlockedUserData
        private
          def data
            object.blocked_profiles
          end
      end
      class ProfilePhotos < Grape::Entity
        expose :data do
          expose :photos, using: Entities::V1::Photo::Attr
        end

        def photos
          options[:user].photos.order("position ASC")
        end
      end
    end
  end
end
