module API
  module V1
    class Accounts < Grape::API
      resource :accounts do
        desc 'Link instagram account'
        post "/instagram" do
          if IdentityHandler.new(current_user, params).create_or_update
            { message: 'Successfully linked instagram account' }
          else
            error!("Cannot link instagram account", 400)
          end
        end
      end
    end
  end
end
