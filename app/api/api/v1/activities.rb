module API
  module V1
    class Activities < Grape::API
      resource :activities do
        desc "Activities"
        get do
          present current_user, with: Entities::V1::Activity::Index, user: current_user
        end
      end
    end
  end
end
