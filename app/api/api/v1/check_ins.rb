module API
  module V1
    class CheckIns < Grape::API

      resource :places do
        desc 'Check in to place'
        post "/:id/check_in" do
          place = Place.not_deleted.find(params[:id])
          if place.check_in(current_user, params[:random_likes], params[:lat], params[:lng])
            present place, with: Entities::V1::Place::Get, user: current_user, place: place
          else
            error!('Cannot check-in to place', 400)
          end
        end

        desc 'Check in to place'
        post "/:id/shared" do
          place = Place.not_deleted.find(params[:id])
          if check_in = current_user.check_ins.availability.where(place_id: params[:id]).last
            if CheckIn.valid_share_count?(params[:shared_likes].to_i) && check_in.shareable?
              current_user.check_ins.last.update(shared_likes: params[:shared_likes]) 
              present place, with: Entities::V1::Place::Get, user: current_user, place: place
            else
              error!('Cannot share check-in on Facebook', 400)
            end
          else
            error!('Cannot share check-in on Facebook', 400)
          end

        end
      end
    end
  end
end