module API
  module V1
    class MySettings < Grape::API

      resource :my_settings do
        desc "List User's Setting"
        get do
          present current_user, with: Entities::V1::UserSetting::Index, user: current_user
        end

        desc 'Toggle Attribute Method'
        post "/:toggle_method" do 
          if(User::ATTRIBUTE_METHODS.include?(params[:toggle_method]))
            current_user.send("toggle_#{params[:toggle_method]}")
            present current_user, with: Entities::V1::UserSetting::Index, user: current_user
          else
            error!("401 Error: Invalid method", 401)
          end
        end

        desc "User's blocked listed profiles"
        get "blacklist"do
          present current_user, with: Entities::V1::UserSetting::BlockedList, user: current_user
        end

        desc "Block a user"
        post "/blacklist/:uid" do
          profile = User.find_by(uid: params[:uid])
          handler = BlockHandler.new(current_user, profile)
          if handler.block
            present current_user, with: Entities::V1::UserSetting::BlockedList, user: current_user
          else
            error!({error: handler.response},400)
          end
        end

        desc "Unblock user"
        delete "/blacklist/:uid" do
          profile = User.find_by(uid: params[:uid])
          handler = BlockHandler.new(current_user, profile)
          if handler.unblock
            present current_user, with: Entities::V1::UserSetting::BlockedList, user: current_user
          else
            error!({error: handler.response},400)
          end
        end
      end

    end
  end
end
