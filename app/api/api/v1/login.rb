module API
  module V1
    class Login < Grape::API

      resource :login do
        desc "Facebook Login"
        post do
          fb_token = params[:access_token]
          if FacebookSessionHandler.valid_token?(fb_token)
            handler = FacebookSessionHandler.new(fb_token)
            user = handler.find_or_create_user
            if user.is_deleted?
              error!(FacebookSessionHandler::INVALID_USER_MESSAGE, 403)
            else
              user.delete_previous_api_keys
              key = user.api_keys.new
              token = key.access_token = SecureRandom.hex(16)
              key.save
              {
                token: token,
                uid: user.uid
              }
            end
          else
            error!(FacebookSessionHandler::INVALID_TOKEN_MESSAGE, 400) 
          end
        end
      end
    end
  end
end
