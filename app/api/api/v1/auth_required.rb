module API
  module V1
    class AuthRequired < Grape::API

      helpers do
        def authenticate!
          auth_header = headers['Authorization']
          # Check if Authorization header is present, else return 401
          error!("Missing Authorization header", 401) if auth_header.blank?

          # Authorization header is present, check if it conforms to our specs
          error!("Invalid Authorization header", 401) unless is_header_valid?(auth_header)

          # Authorization header is valid, but user is deleted
          error!("Inactive account", 401) unless is_user_valid?(auth_header)
        end

        def is_header_valid? auth
          if /\ATipple ([\w]+):([\w\+\=]+)\z/ =~ auth
            uid = $1
            signature = $2
            token = params[:access_token]
            return true if Rails.env.development? || Rails.env.test?
            is_token_valid?(uid, token) && is_signature_authentic?(signature)
          else
            false
          end
        end

        def is_token_valid?(uid, token)
          if user = User.find_by(uid: uid)
            # Return first instance of valid api_key that matches 'token',
            # if not returns nil which will be considered as invalid token
            !(user.api_keys.valid.detect{|a| ApiKey.secure_compare(token, a.encrypted_access_token) }.nil?)
          else
            return false
          end
        end

        def is_signature_authentic?(sig)
          generated_sig = HmacHandler.signature_from(request.path, params)
          Rails.logger.debug "#{request.path} expected: #{generated_sig}; actual: #{sig}" if Rails.env.staging?
          Rails.logger.debug "params: #{params}" if Rails.env.staging?
          HmacHandler.secure_compare(generated_sig, sig)
        end

        def is_user_valid? auth
          if /\ATipple ([\w]+):([\w\+\=]+)\z/ =~ auth
            uid = $1
            !User.not_deleted.find_by(uid: uid).nil?
          else
            false
          end
        end

        def current_user
          auth = headers['Authorization']
          return nil unless is_header_valid? auth
          if /\ATipple ([\w]+):([\w\+\=]+)\z/ =~ auth
            uid = $1
            if @current_user
              @current_user
            else
              (@current_user = User.not_deleted.find_by(uid: uid)) ? @current_user : nil
            end
          end
        end
      end

      before do
        authenticate!
      end

      # Mount all endpoints that require authentication
      mount API::V1::Accounts
      mount API::V1::Places
      mount API::V1::CheckIns
      mount API::V1::MySettings
      mount API::V1::ProfilePhotos
      mount API::V1::Dashboard
      mount API::V1::ShareTipple
      mount API::V1::Products
      mount API::V1::Conversations
      mount API::V1::Interactions
      mount API::V1::Activities
      mount API::V1::Devices

    end
  end
end
