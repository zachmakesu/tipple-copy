module API
  module V1
    class Places < Grape::API
      resource :places do
        desc 'List all places'
        get do
          present Place, with: Entities::V1::Place::Index, user: current_user, params: params
        end

        desc 'Search for places'
        params do
          optional :query, type: String
        end

        get "/search" do
          present Place, with: Entities::V1::Place::Search, user: current_user, params: params
        end

        desc 'View place'
        get "/:id" do
          place = Place.not_deleted.find(params[:id])
          present place, with: Entities::V1::Place::Get, user: current_user, place: place
        end

        desc 'View followers in place'
        get "/:id/followers" do
          place = Place.not_deleted.find(params[:id])
          present place, with: Entities::V1::Place::Follower, user: current_user, place: place
        end

        desc 'Follow place'
        post "/:id/follows" do
          handler = FollowHandler.new(current_user,params[:id]).follow
          if handler.response[:success]
            present handler.response[:details], with: Entities::V1::Place::Get, user: current_user, place: handler.place
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc 'Unfollow place'
        delete "/:id/follows" do 
          handler = FollowHandler.new(current_user,params[:id]).unfollow
          if handler.response[:success]
            present handler.response[:details], with: Entities::V1::Place::Get, user: current_user, place: handler.place
          else
            error!({messages: handler.response[:details]},400)
          end
        end

        desc "Show place follower's profile"
        get "/:place_id/profile/:profile_uid" do 
          if current_user.blocked?(params[:profile_uid])
            error!('This profile is currently blocked by you', 400)
          elsif current_user.blocked_by?(params[:profile_uid])
            error!('This profile is currently blocked you', 400)
          else
            place = Place.not_deleted.find(params[:place_id])
            profile = User.not_deleted.find_by(uid: params[:profile_uid])
            has_cheers = current_user.cheers_with.include?(profile)
            if place.followed_by?(profile) || has_cheers
              if current_user.prefers?([profile]).present? || has_cheers
                present profile, with: Entities::V1::User::GetUserProfile, user: current_user, place: place
              else
                error!('This profile is not in your preferences',400)
              end
            else
              error!('This profile is not yet a follower of this place', 400)
            end
          end
        end
      end

      resource :my_places do
        desc 'List all places'
        get do
          present current_user, with: Entities::V1::Place::FollowedPlaces, user: current_user
        end

        desc 'disable notification'
        post '/:id/unnotify' do
          place = current_user.place_followed.not_deleted.find(params[:id])
          handler = BlockHandler.new(current_user, place)
          if handler.block
            present place, with: Entities::V1::Place::Get, user: current_user, place: place
          else
            error!({error: handler.response},400)
          end
        end

        desc 'enable notification'
        delete '/:id/unnotify' do
          place = current_user.place_followed.not_deleted.find(params[:id])
          handler = BlockHandler.new(current_user, place)
          if handler.unblock
            present place, with: Entities::V1::Place::Get, user: current_user, place: place
          else
            error!({error: handler.response},400)
          end
        end
      end

    end
  end
end
