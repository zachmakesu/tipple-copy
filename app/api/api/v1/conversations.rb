module API
  module V1
    class Conversations < Grape::API
      resource :messages do
        desc "Conversation lists"
        get do
          present current_user, with: Entities::V1::Conversation::Lists, current_user: current_user
        end

        desc "View full conversation"
        get "/:id" do
          conversation = Conversation.find(params[:id])
          if last_message = conversation.messages.last
            if current_user != last_message.sender
              last_message.mark_as_read! for: current_user
            end
          end
          present conversation, with: Entities::V1::Conversation::FullConversation, current_user: current_user
        end

        desc "Create Message"
        post "/:id" do
          conversation = Conversation.find(params[:id])
          if message = MessageHandler.new(params[:id],current_user,params[:body]).create
            Pusher::SentMessageHandler.new(conversation.id, message.id).deliver
            Pusher::MessageNotificationHandler.new(conversation.id, message.id).deliver
            Notifications::NewMessageWorker.perform_async(current_user.id, conversation.id)
            present conversation, with: Entities::V1::Conversation::FullConversation, current_user: current_user
          else
            error!({messages: response},400)
          end
        end
      end
    end
  end
end
