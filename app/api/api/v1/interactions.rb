module API
  module V1

    class Interactions < Grape::API

      resource :interaction do

        desc "Create interaction"
        post do
          handler = InteractionHandler.new(params[:place],current_user.uid,params[:initiatee], refuse: params[:refuse])
          if handler.success?
            present handler.interaction, with: Entities::V1::User::Interaction
          else
            error!({ error: handler.response }, 400)
          end
        end

      end

      resource :likers do
        desc "They liked you page"
        get do
          present current_user, with: Entities::V1::User::LikersPage, user: current_user, likers: current_user.likers
        end
      end
    end
  end
end
