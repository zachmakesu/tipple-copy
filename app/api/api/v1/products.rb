module API
  module V1
    class Products < Grape::API
      resource :products do
        desc 'List all Tipple Like Products'
        get do
          present Product, with: Entities::V1::Product::Index, user: current_user
        end

        desc "Create like enty after buying a product"
        post do
          transaction = TransactionHandler.new(params)
          error!(TransactionHandler::INVALID_PLATFORM_MESSAGE, 400) unless transaction.valid_platform?
          error!(TransactionHandler::INVALID_PRODUCT_MESSAGE, 400) unless transaction.valid_product?
          error!(TransactionHandler::INVALID_RECEIPT_MESSAGE, 400) unless transaction.valid_receipt?

          transaction.associate_with current_user
          present Product, with: Entities::V1::Product::Index, user: current_user
        end

      end

    end
  end
end
