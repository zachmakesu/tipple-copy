module API
  module V1
    class Devices < Grape::API
      resource :devices do
        desc "Create a new device"
        post do
          response = DeviceHandler.new(params, current_user).create
          response.blank? ? { messages: 'Successfully created device' } : error!({ messages: response }, 400)
        end
      end
    end
  end
end
