module API
  module V1
    class ShareTipple < Grape::API

      resource :share_tipple do
        desc "Current user's share page"
        get do
          present Product, with: Entities::V1::ShareTipple::Index, user: current_user
        end

        desc "Create like enty after sharing it"
        post do
          Product.can_be_share.find(params[:id])
          current_user.transactions.create(product_id: params[:id])
          present Product, with: Entities::V1::ShareTipple::Index, user: current_user
        end

      end
    end
  end
end
