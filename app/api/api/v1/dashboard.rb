module API
	module V1
	  class Dashboard < Grape::API

	  	resource :dashboard do
	  		desc "Current user's dashboard"
	  		get do
	  			present current_user, with: Entities::V1::User::Dashboard
	  		end

	  	end
	  end
	end
end
