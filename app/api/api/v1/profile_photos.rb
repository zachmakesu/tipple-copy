module API
  module V1
    class ProfilePhotos < Grape::API
      resource :profile_photos do
        desc "Show profile photos"
        get do
          present current_user, with: Entities::V1::UserSetting::ProfilePhotos, user: current_user
        end

        desc "Add an image"
        post do
          photo = current_user.photos.build()
          if params[:image].present? && params[:position].present?
            photo.image = URI.parse("#{params[:image]}")
            photo.position = params[:position].to_i
            if photo.save             
              present current_user, with: Entities::V1::UserSetting::ProfilePhotos, user: current_user
            else
              error!( photo.errors.full_messages.join(', '), 400)
            end
          end
        end

        post "/arrange_photos" do
          handler = ArrangePhotosHandler.new(current_user,params[:id],params[:position])
          if handler.arrange_photos
            present current_user, with: Entities::V1::UserSetting::ProfilePhotos, user: current_user
          else
            error!({error: handler.response},400)
          end

        end

        desc "Delete an image"
        delete do
          current_user.photos.find(params[:id]).destroy
          current_user.photos.order(position: :asc).first.update(position: 0) if current_user.photos.find_by(position: 0).nil? && current_user.photos.count >= 1
          present current_user, with: Entities::V1::UserSetting::ProfilePhotos, user: current_user
        end
      end 
    end
  end
end