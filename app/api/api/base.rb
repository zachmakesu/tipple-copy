module API
  class Base < Grape::API
    prefix 'api'
    helpers ApiHelper
    mount API::V1::Base
  end
end
