class Notifications::CheersWorker
  include Sidekiq::Worker

  def perform user_id, object_id
    @user = User.find(user_id)
    @object = Interaction.find(object_id)
    Notifications::CheersHandler.new(@user, @object).deliver
  end
end
