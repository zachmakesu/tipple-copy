class Notifications::TrendingPlaceWorker
  include Sidekiq::Worker

  def perform user_id, object_id
    @user = User.find(user_id)
    @object = Activities::PlacesActivity.find(object_id)
    Notifications::TrendingPlaceHandler.new(@user, @object).deliver
  end
end
