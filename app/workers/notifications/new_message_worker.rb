class Notifications::NewMessageWorker
  include Sidekiq::Worker

  def perform user_id, object_id
    @user = User.find(user_id)
    @object = Conversation.find(object_id)
    Notifications::NewMessageHandler.new(@user, @object).deliver
  end
end
