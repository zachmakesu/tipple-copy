class Notifications::MatchCheckinWorker
  include Sidekiq::Worker

  def perform user_id, object_id
    @user = User.find(user_id)
    @object = CheckIn.find(object_id)
    Notifications::MatchCheckinHandler.new(@user, @object).deliver
  end
end
