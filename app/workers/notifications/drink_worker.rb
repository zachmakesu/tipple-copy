class Notifications::DrinkWorker
  include Sidekiq::Worker

  def perform user_id, object_id
    @user = User.find(user_id)
    @object = Interaction.find(object_id)
    Notifications::DrinkHandler.new(@user, @object).deliver
  end
end
