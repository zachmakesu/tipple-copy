class Notifications::ManualNotificationWorker
  include Sidekiq::Worker

  def perform user_id, object_id
    @user = User.find(user_id)
    @object = ManualNotification.find(object_id)
    Notifications::ManualNotificationHandler.new(@user, @object).deliver
  end
end
