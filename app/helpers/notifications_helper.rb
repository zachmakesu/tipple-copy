module NotificationsHelper
  def send_to(record)
    if  record.send_to == "specific_users" 
      record.notification_recipients.present? ? record.recipients.map(&:email).join(", ") : "No User/s selected"
    else
      record.send_to.humanize.capitalize
    end
  end

end
