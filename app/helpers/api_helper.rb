module ApiHelper
  extend Grape::API::Helpers

  # TODO: Change references to utc
  # Why name format :utc if it returns :iso8601?
  Grape::Entity.format_with :utc do |date|
    date.iso8601 if date
  end

  ['original','thumb','medium','large','xlarge'].each do |size|
    Grape::Entity.format_with "#{size}_photo_url".to_sym do |photo|
      photo = photo || Photo.default
      URI.join(ActionController::Base.asset_host, photo.url(size)).to_s if photo
    end
  end

end
