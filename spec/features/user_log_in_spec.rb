require 'rails_helper'

RSpec.feature 'User Log In', :type => :feature do
 
  let(:current_user) { create(:user) }

  scenario 'with invalid email and password' do
    login_in_with 'invalid_email', 'password'

    expect(page).to have_content('Invalid email or password.')
  end

  scenario 'with blank email or password' do
    login_in_with 'valid@example.com', ''

    expect(page).to have_content('Invalid email or password.')
  end

  scenario 'with not admin valid email and password' do
    login_in_with 'mcdave@yahoo.com', 'password123'

    expect(page).to have_content('Invalid email or password.')
  end


  def login_in_with(email, password)
    visit "users/sign_in"
    fill_in 'user[email]', with: email
    fill_in 'user[password]', with: password
    click_button 'LOGIN'
  end
end