require 'rails_helper'

RSpec.describe ConversationMembership, type: :model do

  let!(:users)  { create_list(:user,3) }

  describe :create do 

    context "with valid member credentials" do 

      it "should ensure members of one conversation is not greater then 2" do
        conversation = Conversation.create
        conversation.members << [users.first,users.second]
        third_user = conversation.memberships.new(member: users.third)

        expect(third_user.save).to be false
      end

    end

  end

end
