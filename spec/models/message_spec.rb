require 'rails_helper'

RSpec.describe Message, type: :model do

  let!(:users) { create_list(:user,3) }
  let!(:place) { create(:place) }
  let!(:product)  { create(:product) }

  describe :create do

    before(:each) do
      users.first.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
      users.last.transactions.create(product_id: product.id, transaction_id: 'transaction2', receipt: {valid: true})
      InteractionHandler.new(place,users.first.uid,users.last.uid).create_or_update
      InteractionHandler.new(place,users.last.uid,users.first.uid).create_or_update
      InteractionHandler.new(place,users.first.uid,users.last.uid).create_or_update
      @interaction = InteractionHandler.new(place,users.last.uid,users.first.uid).create_or_update
    end

    context "message using valid member" do

      it "should save message" do 
        msg = MessageHandler.new(@interaction.conversation.id,users.last,"Hey!").create

        expect(@interaction.conversation.messages.last.body).to eq("Hey!")
      end

    end

    context "message using invalid member" do

      it "should not save message" do
        msg = MessageHandler.new(@interaction.conversation.id,users.second,"Hey!").create

        expect(@interaction.conversation.messages).to be_empty
      end

    end

  end

end
