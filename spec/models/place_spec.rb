require 'rails_helper'

RSpec.describe Place, type: :model do

  let!(:current_user) { create(:user) }
  let!(:place)        { create(:place, {name: "Go Rated Ninjas", address: "Unit 20, Ipark Center,401 Eulogio Amang Rodriguez Ave,Pasig,1611 Metro Manila, Pilipinas", lat: 14.6030568, lng: 121.0917855 } ) }
  let!(:place_b)      { create(:place, {name: "Chooks to Go", address: "403 Eulogio Amang Rodriguez Ave,Mangahan, Pasig,1611 Metro Manila,Pilipinas", lat: 14.6027872, lng: 121.0920404, deleted_at: DateTime.now } ) }

  lgbt = %w{straight_male gay_male bi_male straight_female lesbian_female bi_female}
  lgbt.each_with_index do |g,i|
    let(g) { create(:user, email: "#{g}@yahoo.com", gender: (i > 2 ? 2 : 1)) }
  end

  describe "Place" do
    it "creates a place if valid attributes" do
      checkin = Place.new(name: "The Manila Major", address: "Mall of Asia Complex,Pasay,Kalakhang Maynila,Pilipinas", lat: 14.5321546, lng: 120.9833194)
      expect(checkin).to be_valid
      expect{checkin.save}.to change{Place.count}.from(2).to(3)
    end
    it "does not creates a place if missing attributes" do
      checkin = Place.new(name: "The Manila Major", lat: 14.5321546)
      expect(checkin).not_to be_valid
      expect{checkin.save}.to change{Place.count}.by(0)
    end

    it "return places order by distance" do
      expect(Place.get_near_places(14.6027872,121.0920404)).to start_with(place_b, place)
    end

    it "returns number of not_deleted places" do
      expect(Place.not_deleted.count).to eq(1)
    end
  end

  describe "Place followers" do
    it "display current user places followed" do 
      place.follow_by(current_user)
      place_b.follow_by(current_user)
      expect(current_user.place_followed).to include(place, place_b)
    end

    it "follows the place" do 
      expect{place.follow_by(current_user)}.to change{place.followers.count}.from(0).to(1)
    end

    it "unfollows the place" do 
      place.follow_by(current_user)
      expect{place.unfollow_by(current_user)}.to change{place.followers.count}.from(1).to(0)
    end

    it "display all place's followers" do
      place.follow_by(current_user)
      place.follow_by(straight_male)
      place.follow_by(straight_female)
      place.follow_by(lesbian_female)
      place.follow_by(bi_female)
      expect(place.followers).to include(current_user, straight_male, straight_female, lesbian_female, bi_female)
    end

    it "Display all place's followers based on user's gender and preference" do
      straight_male.update_attributes(prefers_men: false, prefers_women: true)
      gay_male.update_attributes(prefers_men: true, prefers_women: false)
      bi_male.update_attributes(prefers_men: true, prefers_women: true)
      straight_female.update_attributes(prefers_men: true, prefers_women: false)
      lesbian_female.update_attributes(prefers_men: false, prefers_women: true)
      bi_female.update_attributes(prefers_men: true, prefers_women: true)

      place.follow_by(straight_male)
      place.follow_by(gay_male)
      place.follow_by(bi_male)
      place.follow_by(straight_female)
      place.follow_by(lesbian_female)
      place.follow_by(bi_female)

      expect(straight_male.prefers?(place.followers.except_self(straight_male))).to include(straight_female, bi_female)
    end
  end

  describe "Place check in" do
    context "if in below 10m near" do
      it "creates check ins for that place if check_in for that place is nil or expired" do
        expect{place.check_in(current_user, 3, 14.6030568, 121.091870)}.to change{CheckIn.count}.from(0).to(1)
        expect(current_user.check_ins.count).to eq(1)
        expect(current_user.check_ins.availability.count).to eq(1)
      end

      it "does not creates check ins for that place if check_in for that place is not expired" do
        expect{place.check_in(current_user, 3, 14.6030568, 121.091870)}.to change{CheckIn.count}.from(0).to(1)
        place.check_in(current_user, 3, 14.6030568, 121.091870)
        expect(current_user.check_ins.count).to eq(1)
        expect(current_user.check_ins.availability.count).to eq(1)
      end

      it "creates check ins for Place B and cancel check in in Place A" do
        expect{place.check_in(current_user, 3, 14.6030568, 121.091870)}.to change{CheckIn.count}.from(0).to(1)
        expect{place_b.check_in(current_user, 3, 14.6027872, 121.091970)}.to change{CheckIn.count}.from(1).to(2)
        expect(current_user.check_ins.count).to eq(2)
        expect(current_user.check_ins.availability.count).to eq(1)
      end
    end

    context "if in above 10m near" do
      it "does not creates check ins for that place" do
        place.check_in(current_user, 3, 14.6030568, 121.091970) #20 meters distance
        expect(current_user.check_ins.count).to eq(1)
      end
    end

    context 'if distance is above 100m' do
      it 'should not allow check in for that place' do
        expect do
          place.check_in(current_user, 3, place.lat + 2.0, place.lng + 2.0)
        end.to_not change(CheckIn, :count)
      end
    end

    context "if params are missing" do
      it "does not creates check ins for that place" do
        expect{place.check_in(current_user, 3, 14.6030568)}.to raise_error(ArgumentError)
      end
    end

    context "if random_likes is greater than 10" do
      it "does not creates check ins for that place" do
        expect(place.check_in(current_user, 14, 14.6030568, 121.091870)).to be_falsey
      end
    end
  end

  describe ".valid_distance?" do
    context "validate condition" do
      it "should return true if distance is <= #{Place::VALID_DISTANCE} && >= 0" do
        expect(Place.valid_distance?(0.009)).to be_truthy
      end

      it "should return false if distance is > #{Place::VALID_DISTANCE}" do
        expect(Place.valid_distance?(0.11)).to be_falsey
      end
    end 
  end

  describe ".valid_random_likes?" do
    context "validate condition" do
      it "should return true if random_likes is <= 10 && >= 0" do
        expect(Place.valid_random_likes?(1)).to be_truthy
      end

      it "should return false if random_likes is > 10" do
        expect(Place.valid_random_likes?(11)).to be_falsey
      end
    end 
  end

  describe '.description' do
    let!(:gorated) { build(:place, name: 'Gorated') }
    it 'should allow blank value' do
      gorated.description = nil
      expect(gorated).to be_valid
    end
    it 'should be less than 140 characters' do
      gorated.description = 'x' * 141
      expect(gorated).to_not be_valid
    end

    it 'should be a maximum of 140 characters' do
      gorated.description = 'x' * 140
      expect(gorated).to be_valid
    end

    it 'should have default placeholder value' do
      expect(place.description.blank?).to be(false)
      expect(place.description).to eq(Place::DEFAULT_DESCRIPTION)
    end

  end
end
