require 'rails_helper'

RSpec.describe Photo, type: :model do
  let!(:current_user) { create(:user) }
  let!(:place) { create(:place) }
  let!(:image) { File.open(Rails.root.join("spec", "files", "image.jpg")) }

  describe "User photo" do
    it "Adds a photo of user with valid position" do
      expect{current_user.photos.create(image: image, position: 0)}.to change{current_user.photos.count}.from(0).to(1)
    end

    it "Does not adds a photo of user without position" do
      expect{current_user.photos.create(image: image)}.to_not change(current_user.photos, :count)
    end

    it "Does not adds a photo of user when position is already been taken" do
      current_user.photos.create(image: image, position: 0)
      expect{current_user.photos.create(image: image, position: 0)}.to_not change(current_user.photos, :count)
    end

    it "Does not adds a photo of user when position is greater than 4" do
      expect{current_user.photos.create(image: image, position: 6)}.to_not change(current_user.photos, :count)
    end

    it "Does not adds a photo of user when it reaches maximum(5) number of photos" do
      current_user.photos.create(image: image, position: 0)
      current_user.photos.create(image: image, position: 1)
      current_user.photos.create(image: image, position: 2)
      current_user.photos.create(image: image, position: 3)
      current_user.photos.create(image: image, position: 4)
      expect{current_user.photos.create(image: image, position: 6)}.to_not change(current_user.photos, :count)
    end
  end

  describe "Place photo" do
    it "Adds a photo of place with valid position" do
      place.photos.create(image: image, position: 7)
      expect(place.photos.count).to eq(1)
    end

    it "Does not adds a photo of place without position" do
      expect{place.photos.create(image: image)}.to_not change(place.photos, :count)
    end

    it "Does not adds a photo of place when position is already been taken" do
      place.photos.create(image: image, position: 0)
      expect{place.photos.create(image: image, position: 0)}.to_not change(place.photos, :count)
    end
  end
end
