require 'rails_helper'

RSpec.describe Device, type: :model do

  let!(:user) { create(:user) }

  it { should validate_presence_of :token }

  it { should validate_presence_of :platform }

  it { should validate_presence_of :udid }

  it { should validate_uniqueness_of(:token).scoped_to(:user_id) }

  it { should validate_uniqueness_of(:udid).scoped_to(:user_id) }

  it { should validate_inclusion_of(:platform).in_array(%w[ android ios ]) }

  describe "create object device" do
    context "with 'is_enabled' == false and 'platform' == ios" do

      it "should get ios platforms" do
        Device.create(user_id: user.id, udid: "1a2s3f",token: "123",platform: "ios", name: "Iphone Edge", is_enabled: false)

        expect(Device.ios.count).to eq(1)
      end

      it "should get disabled devices" do
        Device.create(user_id: user.id, udid: "1a2s3f",token: "123",platform: "ios", name: "Iphone Edge", is_enabled: false)

        expect(Device.disabled.count).to eq(1)
      end

      it "should get android platforms" do
        Device.create(user_id: user.id, udid: "1a2s3f",token: "123",platform: "android", name: "Iphone Edge", is_enabled: false)

        expect(Device.android.count).to eq(1)
      end

      it "should get enabled devices" do
        Device.create(user_id: user.id, udid: "1a2s3f",token: "123",platform: "ios", name: "Iphone Edge")

        expect(Device.enabled.count).to eq(1)
      end

    end
  end

end
