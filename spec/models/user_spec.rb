require 'rails_helper'

RSpec.describe User, type: :model do
  let(:current_user) { create(:user) }
  let(:place)        { create(:place) }
  let!(:product)     { create(:product) }
  lgbt = %w{straight_male gay_male bi_male straight_female lesbian_female bi_female}
  lgbt.each_with_index do |g,i|
    let!(g) { create(:user, email: "#{g}@yahoo.com", gender: (i > 2 ? 2 : 1)) }
  end

  it "Display current user placed followed" do
    place.follow_by(current_user)
    expect(current_user.place_followed).to be_truthy
  end

  def get_users(str)
    case str
    when "s_m" 
      straight_male.update_attributes(prefers_men: false, prefers_women: true, gender: 1)
      straight_male
    when "s_f" 
      straight_female.update_attributes(prefers_men: true, prefers_women: false, gender: 2)
      straight_female
    when "b_f" 
      bi_female.update_attributes(prefers_men: true, prefers_women: true, gender: 2)
      bi_female
    when "b_m" 
      bi_male.update_attributes(prefers_men: true, prefers_women: true, gender: 1)
      bi_male
    when "g_m" 
      gay_male.update_attributes(prefers_men: true, prefers_women: false, gender: 1)
      gay_male
    when "l_f" 
      lesbian_female.update_attributes(prefers_men: false, prefers_women: true, gender: 2)
      lesbian_female
    else
      nil
    end
  end

  describe 'male' do
    context 'straight' do
      it 'should prefer straight or bi female' do
        expect(get_users("s_m").prefers?([get_users("s_f")])).not_to be_empty
        expect(get_users("s_m").prefers?([get_users("b_f")])).not_to be_empty
      end
      it 'should not prefer lesbian female' do
        expect(get_users("s_m").prefers?([get_users("l_f")])).to be_empty #return nil
      end
      it 'should not prefer male' do
        expect(get_users("s_m").prefers?([get_users("g_m")])).to be_empty
        expect(get_users("s_m").prefers?([get_users("s_m")])).to be_empty
        expect(get_users("s_m").prefers?([get_users("b_m")])).to be_empty
      end
    end
    context 'gay' do
      it 'should not prefer female' do
        expect(get_users("g_m").prefers?([get_users("s_f")])).to be_empty
        expect(get_users("g_m").prefers?([get_users("l_f")])).to be_empty
        expect(get_users("g_m").prefers?([get_users("b_f")])).to be_empty
      end
      it 'should not prefer straight male' do
        expect(get_users("g_m").prefers?([get_users("s_m")])).to be_empty
      end
      it 'should prefer gay or bi male' do
        expect(get_users("g_m").prefers?([get_users("g_m")])).not_to be_empty
        expect(get_users("g_m").prefers?([get_users("b_m")])).not_to be_empty
      end
    end
    context 'bi' do
      it 'should prefer straight or bi female' do
        expect(get_users("b_m").prefers?([get_users("s_f")])).not_to be_empty
        expect(get_users("b_m").prefers?([get_users("b_f")])).not_to be_empty
      end
      it 'should not prefer lesbian female' do
        expect(get_users("b_m").prefers?([get_users("l_f")])).to be_empty
      end
      it 'should not prefer straight male' do
        expect(get_users("b_m").prefers?([get_users("s_m")])).to be_empty
      end
      it 'should prefer gay or bi male' do
        expect(get_users("b_m").prefers?([get_users("g_m")])).not_to be_empty
        expect(get_users("b_m").prefers?([get_users("b_m")])).not_to be_empty
      end
    end
  end

  describe 'female' do
    context 'straight' do
      it 'should prefer straight or bi male' do
        expect(get_users("s_f").prefers?([get_users("s_m")])).not_to be_empty
        expect(get_users("s_f").prefers?([get_users("b_m")])).not_to be_empty
      end
      it 'should not prefer gay male' do
        expect(get_users("s_f").prefers?([get_users("g_m")])).to be_empty
      end
      it 'should not prefer female' do
        expect(get_users("s_f").prefers?([get_users("l_f")])).to be_empty
        expect(get_users("s_f").prefers?([get_users("s_f")])).to be_empty
        expect(get_users("s_f").prefers?([get_users("b_f")])).to be_empty
      end
    end
    context 'lesbian' do
      it 'should not prefer male' do
        expect(get_users("l_f").prefers?([get_users("s_m")])).to be_empty
        expect(get_users("l_f").prefers?([get_users("g_m")])).to be_empty
        expect(get_users("l_f").prefers?([get_users("b_m")])).to be_empty
      end
      it 'should not prefer straight female' do
        expect(get_users("l_f").prefers?([get_users("s_f")])).to be_empty
      end
      it 'should prefer lesbian or bi female' do
        expect(get_users("l_f").prefers?([get_users("l_f")])).not_to be_empty
        expect(get_users("l_f").prefers?([get_users("b_f")])).not_to be_empty
      end
    end
    context 'bi' do
      it 'should prefer straight or bi male' do
        expect(get_users("b_f").prefers?([get_users("s_m")])).not_to be_empty
        expect(get_users("b_f").prefers?([get_users("b_m")])).not_to be_empty
      end
      it 'should not prefer gay male' do
        expect(get_users("b_f").prefers?([get_users("g_m")])).to be_empty
      end
      it 'should not prefer straight female' do
        expect(get_users("b_f").prefers?([get_users("s_f")])).to be_empty
      end
      it 'should prefer lesbian or bi male' do
        expect(get_users("b_f").prefers?([get_users("l_f")])).not_to be_empty
        expect(get_users("b_f").prefers?([get_users("b_f")])).not_to be_empty
      end
    end
  end

  describe :likers do
    context "validate query" do
      it "should show user who likes you" do
        straight_female.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
        InteractionHandler.new(place,straight_female.uid,current_user.uid).create_or_update
        InteractionHandler.new(place,straight_female.uid,current_user.uid).create_or_update

        expect(current_user.likers.map(&:sender_id)).to include(straight_female.id)
      end

      it "should not show the user who already had a conversation with the current_user" do
        straight_female.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
        current_user.transactions.create(product_id: product.id, transaction_id: 'transaction2', receipt: {valid: true})
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        InteractionHandler.new(place,straight_female.uid,current_user.uid).create_or_update
        InteractionHandler.new(place,straight_female.uid,current_user.uid).create_or_update
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        current_user.interactions.first.conversation.messages.create(sender_id: current_user.id, body: "test message")

        expect(current_user.likers.map(&:sender_id)).not_to include(straight_female.id)
      end
    end
  end

  # TODO: Move these onto own service and service spec
  describe '#credits_left' do
    context 'has no products purchased' do
      before { place.check_in(current_user, 1, place.lat, place.lng) }
      it 'should still credit credits won via check-in' do
        expect(current_user.credits_left).to eq('6')
      end
      it 'should still credit credits won via share' do
        current_user.check_ins.availability.last.update(shared_likes: 7)
        expect(current_user.credits_left).to eq('13')
      end
    end

    context 'has non-unlimited products purchased' do
      it 'should credit purchased product credits' do
        current_user.transactions.create!(product_id: product.id, transaction_id: 'transation', receipt: {valid: true})
        expect(current_user.credits_left).to eq((product.value + User::DEFAULT_DRINK_CREDITS).to_s)
      end
    end

    context 'has monthly-subscription product purchased' do
      let(:unlimited_product) { create(:unlimited_product) }
      it 'should return unlimited if subscribed' do
        current_user.transactions.create!(product_id: unlimited_product.id, expires_at: DateTime.now.next_month, transaction_id: 'transation', receipt: {valid: true})
        expect(current_user.credits_left).to eq('Unlimited')
      end
    end
  end

  describe '#has_credits?' do
    context 'has no products purchased' do
      it 'should return true since there are 5 free drink credits' do
        expect(current_user.has_credits?).to eq(true)
      end
      it 'should return true if user has checkin credits' do
        place.check_in(current_user, 1, place.lat, place.lng) 
        expect(current_user.has_credits?).to eq(true)
      end
    end

    context 'has non-unlimited products purchased' do
      it 'should return true' do
        current_user.transactions.create!(product_id: product.id, transaction_id: 'transation', receipt: {valid: true})
        expect(current_user.has_credits?).to eq(true)
      end
    end

    context 'has monthly-subscription product purchased' do
      let(:unlimited_product) { create(:unlimited_product) }
      it 'should return true' do
        current_user.transactions.create!(product_id: product.id, transaction_id: 'transation', receipt: {valid: true})
        expect(current_user.has_credits?).to eq(true)
      end
    end
  end


  describe '#instagram_uid' do
    let(:user){ create(:user)}
    context 'has instagram identity' do
      let(:identity_options){ {uid: '123456', provider: 'instagram', token: 'MuhToken'} }
      let(:identity_handler){ IdentityHandler.new(user, identity_options) }
      before do
        identity_handler.create_or_update
      end
      it 'should return the instagram uid' do
        expect(user.instagram_uid).to eq('123456')
      end
    end
    context 'has no instagram identity' do
      it 'should return nil' do
        expect(user.instagram_uid).to eq(nil)
      end
    end
  end

  describe '#facebook_uid' do
    let(:user){ create(:user)}
    context 'has facebook identity' do
      let(:identity_options){ {uid: '123456', provider: 'facebook', token: 'MuhToken'} }
      let(:identity_handler){ IdentityHandler.new(user, identity_options) }
      before do
        identity_handler.create_or_update
      end
      it 'should return the facebook uid' do
        expect(user.facebook_uid).to eq('123456')
      end
    end
    context 'has no facebook identity' do
      it 'should return nil' do
        expect(user.facebook_uid).to eq(nil)
      end
    end
  end

  describe '#credits_left_count' do
    let(:unlimited_product) { create(:unlimited_product) }

    around :each do |example|
      place.check_in(current_user, 3, place.lat, place.lng)
      example.run
    end

    context 'has earned credits via check-in and purchase' do
      before do
        current_user.transactions.create!(product_id: product.id, transaction_id: 'transation', receipt: {valid: true})
      end

      it 'should decrease user\'s credits count' do
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        InteractionHandler.new(place,straight_female.uid,current_user.uid).create_or_update
        expect(current_user.credits_left_count).to eq((product.value + 3 + User::DEFAULT_DRINK_CREDITS) - 1)
      end
    end

    context 'has unlimited credits' do
      before do
        current_user.transactions.create!(product_id: unlimited_product.id, expires_at: DateTime.now.next_month, transaction_id: 'transaction', receipt: {valid: true})
      end

      it 'should not decrease user\'s credits count' do
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        InteractionHandler.new(place,straight_female.uid,current_user.uid).create_or_update
        expect(current_user.credits_left_count).to eq(3 + User::DEFAULT_DRINK_CREDITS)
      end
    end

    context 'has expired unlimited credits' do
      before do
        current_user.transactions.create!(product_id: unlimited_product.id, expires_at: DateTime.now.last_month, transaction_id: 'transaction', receipt: {valid:true})
      end

      it 'should decrease user\'s credits count' do
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        InteractionHandler.new(place,straight_female.uid,current_user.uid).create_or_update
        expect(current_user.credits_left_count).to eq(2 + User::DEFAULT_DRINK_CREDITS)
      end
    end

    describe "#has_read? message" do
      before do
        straight_female.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid:true})
        current_user.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid:true})
        InteractionHandler.new(place,current_user.uid,straight_female.uid).create_or_update
        InteractionHandler.new(place,straight_female.uid,current_user.uid).create_or_update
        @message = current_user.interactions.first.conversation.messages.create(sender_id: current_user.id, body: "test message")
      end

      it 'should return true if sender is the same as caller' do
        expect(current_user.has_read?(@message)).to be(true)
      end

      it 'should return true read status of message if read' do
        expect(straight_female.has_read?(@message)).to be(false)
      end

      it 'should return false read status of message if unread' do
        @message.mark_as_read! for: straight_female
        expect(straight_female.has_read?(@message)).to be(true)
      end
    end

    describe '#mutual_friends_with(user)' do
      it 'should return empty array if either user has no identity' do
        expect(current_user.mutual_friends_with(straight_female)).to be_empty
      end
    end
  end
end
