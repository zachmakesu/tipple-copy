require 'rails_helper'

RSpec.describe CheckIn, type: :model do
  let(:current_user) { create(:user) }
  let(:place)        { create(:place) }

  before(:each) do
    CheckIn.create(user_id: current_user.id, place_id: place.id, random_likes: 1)
  end

  describe "CheckIn" do
    context "If missing attributes" do
      it "does not create CheckIn" do
        expect{CheckIn.new(user_id: current_user.id).save!}.to raise_error(ActiveRecord::RecordInvalid)
      end
    end

    context "If valid attributes" do
      it "create CheckIn" do
        expect{CheckIn.create(user_id: current_user.id, place_id: place.id, random_likes: 3)}.to change{CheckIn.count}.from(1).to(2)
      end
    end
  end

  describe :shareable? do
    context "validate condition" do
      it "should return true if shared_likes is == 0" do
        expect(CheckIn.first.shareable?).to be_truthy
      end

      it "should return false if shared_likes is <= 10" do
        CheckIn.first.update(shared_likes: 10)
        expect(CheckIn.first.shareable?).to be_falsey
      end
    end 
  end

  describe ".valid_share_count?" do
    context "validate condition" do
      it "should return true if random_likes is <= 10" do
        expect(CheckIn.valid_share_count?(1)).to be_truthy
      end

      it "should return false if random_likes is > 10" do
        expect(CheckIn.valid_share_count?(11)).to be_falsey
      end
    end 
  end

end
