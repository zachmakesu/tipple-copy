require 'rails_helper'

RSpec.describe Interaction, type: :model do
  let!(:boy)    { create(:user) }
  let!(:girl)   { create(:user) }
  let!(:place)  { create(:place) }
  context "with valid interaction property values" do
    before(:each) do
      @interaction = Interaction.create!({
        initiator: boy,
        initiatee: girl,
        place: place
      })
    end

    it "has initial state of like" do
      expect(@interaction.like?).to be true
    end

    it "has final state of cheers" do
      @interaction.like_back!
      expect(@interaction.cheers?).to be true
    end

    it "validates that final state is cheers" do
      @interaction.like_back!
      begin
        @interaction.refuse!
      rescue Workflow::NoTransitionAllowed
        nil
      end
      expect(@interaction.cheers?).to be true
    end

    it "validate refuse state to have like as next event action" do
      @interaction.drink!
      @interaction.refuse!
      expect(@interaction.like_back!).to be true
    end

    describe '.active' do
      it 'should return active items' do
        expect(Interaction.active).to include(@interaction)
      end
    end

    describe '.inactive' do
      it 'should return inactive items' do
        @interaction.update(active: false)
        expect(Interaction.inactive).to include(@interaction)
      end
    end
  end

  context "with same interaction initiator and initiatee" do
    it "should be invalid" do
      @interaction = Interaction.create({
        initiator: boy,
        initiatee: boy,
        place: place
      })
      expect(@interaction).to be_invalid
    end
  end

end
