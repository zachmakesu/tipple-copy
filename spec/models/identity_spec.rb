require 'rails_helper'

RSpec.describe Identity, type: :model do

  let!(:user) { create(:user) }

  def identity_params
    {
      token: "test123", 
      uid: "test123123", 
      provider: "facebook"
    }
  end

  it { should validate_inclusion_of(:provider).in_array(%w{ facebook instagram }) }

  describe "Create Identities" do
    context "with new and valid params" do

      before(:each) do
        @identity = user.identities.build(identity_params)
      end

      it "should save identities" do
        expect{@identity.save}.to change{user.identities.count}.from(0).to(1)
      end

      it "validates uniqueness of user scoped to uid and provider" do
        @identity.save

        expect(user.identities.build(identity_params).save).to be_falsey
      end

    end
  end

end
