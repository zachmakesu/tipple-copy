require 'rails_helper'

RSpec.describe Product, type: :model do

  let!(:share1) {FactoryGirl.create :product, published_at: DateTime.now - 2.hours, duration: 0, value: 5, ability: 1, description: "Like tipple on facebook"}
  let!(:product1) {FactoryGirl.create :product, published_at: DateTime.now - 8.hours, duration: 0, value: 10, price: 1.99, ability: 0, name: "Baby Tippler"}
  
  it "Display share list" do
    list = Product.can_be_share
    expect(list.count).to eq(1)
  end

   it "Display product list" do
    product = Product.can_be_purchase
    expect(product.count).to eq(1)
  end

end
