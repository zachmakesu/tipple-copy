require 'rails_helper'

RSpec.describe BlockList, type: :model do
  let!(:current_user) { create(:user) }
  let!(:place)        { create(:place, {name: "Go Rated Ninjas", address: "Unit 20, Ipark Center,401 Eulogio Amang Rodriguez Ave,Pasig,1611 Metro Manila, Pilipinas", lat: 14.6030568, lng: 121.0917855 } ) }
  let!(:place_b)      { create(:place, {name: "Chooks to Go", address: "403 Eulogio Amang Rodriguez Ave,Mangahan, Pasig,1611 Metro Manila,Pilipinas", lat: 14.6027872, lng: 121.0920404 } ) }

  lgbt = %w{straight_male gay_male bi_male straight_female lesbian_female bi_female}
  c = 0
  lgbt.each_with_index do |g,i|
    let(g) { create(:user, email: "#{g}@yahoo.com", gender: (i > 2 ? 2 : 1)) }
  end

  describe "Block list" do
    context "if params valid " do
      it "creates blocked profile" do
        expect{current_user.block_lists.create(blockable: gay_male)}.to change{BlockList.count}.from(0).to(1)
      end

      it "does not a create duplicate blocked profile" do
        current_user.block_lists.create(blockable: gay_male)
        expect{current_user.block_lists.create(blockable: gay_male)}.not_to change{BlockList.count}
      end
      
      it "creates blocked place" do
        expect{current_user.block_lists.create(blockable: place)}.to change{BlockList.count}.from(0).to(1)
      end

      it "does not a create duplicate blocked place" do
        current_user.block_lists.create(blockable: place)
        expect{current_user.block_lists.create(blockable: place)}.not_to change{BlockList.count}
      end

    end

    context "if params not complete" do
      it "doest not creates blocked profile" do
        expect(current_user.block_lists.new().save).to be_falsey
      end
    end

    context "if params is the user itself" do
      it "doest not creates blocked profile" do
        expect(current_user.block_lists.new(blockable: current_user).save).to be_falsey
        expect{current_user.block_lists.create(blockable: current_user)}.not_to change{BlockList.count}
      end
    end
  end
end
