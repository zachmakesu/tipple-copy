require 'rails_helper'

RSpec.describe ProductTransaction, type: :model do

  let!(:current_user) { create(:user) }
  let!(:shareable_product) {FactoryGirl.create :product, published_at: DateTime.now - 2.hours, duration: 0, value: 5, ability: 1, description: "Like tipple on facebook"}
  let!(:purchaseable_product) {FactoryGirl.create :product, published_at: DateTime.now - 8.hours, duration: 0, value: 10, price: 1.99, ability: 0, name: "Baby Tippler"}

  context 'shareable product' do
    it "is valid without transaction_id" do
      transaction = current_user.transactions.build
      transaction.product = shareable_product
      expect(transaction.valid?).to be(true)
    end
  end

  context 'purchaseable product' do
    it "is invalid without transaction_id" do
      transaction = current_user.transactions.build
      transaction.product = purchaseable_product
      transaction.transaction_id = nil
      expect(transaction.valid?).to be(false)
    end

    it "validates receipt if transaction_id is present" do
      transaction = current_user.transactions.build
      transaction.product = purchaseable_product
      transaction.transaction_id = 'valid_transaction_id'
      expect(transaction.valid?).to be(false)
    end

    it 'is valid if it has transaction_id and receipt' do
      transaction = current_user.transactions.build
      transaction.product = purchaseable_product
      transaction.transaction_id = 'valid_transaction_id'
      transaction.receipt = {valid: true}
      expect(transaction.valid?).to be(true)
    end
  end

end
