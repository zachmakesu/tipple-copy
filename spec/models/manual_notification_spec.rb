require 'rails_helper'

RSpec.describe ManualNotification, type: :model do
  let(:current_user) { create(:user) }
 
  context "If missing attributes" do
    it "does not create ManualNotification" do
      expect{ManualNotification.new(user_id: current_user.id).save!}.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context "If valid attributes" do
    it "create ManualNotification" do
      expect{ManualNotification.create(user_id: current_user.id, message: "Message", send_to: "all_users")}.to change{ManualNotification.count}.from(0).to(1)
    end
  end
end
