# == Schema Information
#
# Table name: users
#
#  birthdate                       :string
#  created_at                      :datetime         not null
#  current_sign_in_at              :datetime
#  current_sign_in_ip              :inet
#  deleted_at                      :datetime
#  email                           :string           default(""), not null
#  encrypted_password              :string           default(""), not null
#  facebook_url                    :string
#  first_name                      :string
#  gender                          :integer          default(0), not null
#  help                            :boolean          default(FALSE)
#  hide_profile_at                 :datetime
#  hide_profile_with_zero_check_in :boolean          default(FALSE)
#  id                              :integer          not null, primary key
#  image_url                       :string
#  last_name                       :string
#  last_sign_in_at                 :datetime
#  last_sign_in_ip                 :inet
#  notify_activity                 :boolean          default(TRUE)
#  notify_cheers                   :boolean          default(TRUE)
#  notify_drinks                   :boolean          default(TRUE)
#  notify_messages                 :boolean          default(TRUE)
#  prefers_men                     :boolean          default(FALSE)
#  prefers_women                   :boolean          default(FALSE)
#  remember_created_at             :datetime
#  reset_password_sent_at          :datetime
#  reset_password_token            :string
#  role                            :integer          default(0), not null
#  sign_in_count                   :integer          default(0), not null
#  uid                             :string           default(""), not null
#  updated_at                      :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

require 'rails_helper'

RSpec.describe UsersController, type: :controller do

end
