require 'rails_helper'

describe IdentityHandler do
  context 'Instagram identities handling' do
    let(:user){ create(:user)}
    let(:identity_options){ {uid: '123456', provider: 'instagram', token: 'MuhToken'} }
    let(:identity_handler){ IdentityHandler.new(user, identity_options) }

    context 'no identity' do
      it 'should create identity' do
        expect{identity_handler.create_or_update}.to change(Identity, :count).by(1)
      end
    end

    context 'existing identity' do
      before do
        identity_handler.create_or_update
      end
      it 'should not create new identity' do
        expect{identity_handler.create_or_update}.to change(Identity, :count).by(0)
      end
      it 'should just update token if provided' do
        updated_options = identity_options.tap do |option|
          option[:token] = 'TokenMuh'
        end
        identity = IdentityHandler.new(user, updated_options).create_or_update
        expect(identity.token).to eq('TokenMuh')
      end
      it 'should not be referenced to another user' do
        new_user = create(:user)
        IdentityHandler.new(new_user, identity_options).create_or_update
        expect(new_user.reload.identities).to be_empty
      end
    end
  end
end
