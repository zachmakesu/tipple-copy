require "rails_helper"

RSpec.describe TransactionHandler do
  describe '::VALID_PLATFORMS' do
    it 'should be an array containing ios and android' do
      expect(TransactionHandler::VALID_PLATFORMS).to eq(['ios', 'android'])
    end
  end
  describe '::INVALID_PLATFORM_MESSAGE' do
    let!(:error_message){ 'Please provide valid platform' }
    it "should return correct error message for invalid platform" do
      expect(TransactionHandler::INVALID_PLATFORM_MESSAGE).to eq(error_message)
    end
  end
  describe '::INVALID_PRODUCT_MESSAGE' do
    let!(:error_message){ 'Please provide valid product' }
    it "should return correct error message for invalid product" do
      expect(TransactionHandler::INVALID_PRODUCT_MESSAGE).to eq(error_message)
    end
  end
  describe '::INVALID_RECEIPT_MESSAGE' do
    let!(:error_message){ 'Please provide valid receipt' }
    it "should return correct error message for invalid receipt" do
      expect(TransactionHandler::INVALID_RECEIPT_MESSAGE).to eq(error_message)
    end
  end
  describe '#valid_platform?' do
    let(:valid_transaction){ {platform: 'ios'} }
    let(:invalid_transaction){ {platform: 'windows'} }
    it 'returns true for valid platform like ios' do
      expect(TransactionHandler.new(valid_transaction).valid_platform?).to be(true)
    end
    it 'returns false for invalid platform like windows' do
      expect(TransactionHandler.new(invalid_transaction).valid_platform?).to be(false)
    end
  end
  describe '#valid_product?' do
    let(:product){ create(:product) }
    let(:shareable_product){ create(:product, ability: 1) }
    let(:valid_transaction){ {platform: 'ios', product_id: product.id} }
    let(:invalid_transaction){ {platform: 'ios', product_id: shareable_product.id} }
    it 'should be valid if product is purchaseable' do
      transaction = TransactionHandler.new(valid_transaction)
      expect(transaction.valid_product?).to be_truthy
    end
    it 'should be invalid if product is shareable' do
      transaction = TransactionHandler.new(invalid_transaction)
      expect(transaction.valid_product?).to be_falsy
    end
    it 'should be invalid if no product' do
      transaction = TransactionHandler.new({})
      expect(transaction.valid_product?).to be_falsy
    end
  end
  describe '#associate_with current_user' do
    let(:current_user){ create(:user) }
    let(:product){ create(:product) }
    let(:unli_product){ create(:product, duration: 3) }
    let(:android_product_params) do
      receipt_data = File.open("#{Rails.root}/spec/files/150-drinks-unconsumed-android.receipt", 'rb') { |file| file.read }.squish
      {platform: 'android', product_id: product.id, receipt_data: receipt_data}
    end
    let(:ios_product_params) do
      receipt_data = File.open("#{Rails.root}/spec/files/10-drinks.receipt", 'rb') { |file| file.read }.squish
      {platform: 'ios', product_id: product.id, receipt_data: receipt_data}
    end
    let(:unli_product_params) do
      receipt_data = File.open("#{Rails.root}/spec/files/unli-drinks.receipt", 'rb') { |file| file.read }.squish
      {platform: 'ios', product_id: unli_product.id, receipt_data: receipt_data}
    end
    context 'for android platform' do
      before do
        VCR.use_cassette 'transactions/android-verification-auth' do
          PlayStoreHandler.instance
        end
      end
      it 'create new transaction for consumable purchase' do
        transaction = VCR.use_cassette 'transactions/android-unconsumed-150drinks' do
          TransactionHandler.new(android_product_params)
        end
        expect{transaction.associate_with(current_user)}.to change{ProductTransaction.count}.by(1)
      end
    end
    context 'for ios platform' do
      it 'create new transaction for consumable purchase' do
        transaction = VCR.use_cassette 'transactions/ios-10-drinks' do
          TransactionHandler.new(ios_product_params)
        end
        expect{transaction.associate_with(current_user)}.to change{ProductTransaction.count}.by(1)
      end
      it 'create new transaction for unlimited subscription' do
        transaction = VCR.use_cassette 'transactions/ios-unli-drinks' do
          TransactionHandler.new(unli_product_params)
        end
        expect{transaction.associate_with(current_user)}.to change{ProductTransaction.count}.by(1)
      end
      it 'does not create new transaction if subscribed to unlimited' do
        transaction = VCR.use_cassette 'transactions/ios-10-drinks' do
          TransactionHandler.new(ios_product_params)
        end
        unli_transaction = VCR.use_cassette 'transactions/ios-unli-drinks' do
          TransactionHandler.new(unli_product_params)
        end
        unli_transaction.associate_with(current_user)
        expect{transaction.associate_with(current_user)}.to change{ProductTransaction.count}.by(0)
      end
    end
  end
end
