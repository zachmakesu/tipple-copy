require "rails_helper"

describe ArrangePhotosHandler do
  let!(:user)     { create(:user) }
  let!(:place)    { create(:place) }
  let!(:photo_a)  { (user.photos << create(:photo, position: 0)).first }
  let!(:photo_b)  { (user.photos << create(:photo, position: 1)).last }
  let!(:photo_c)  { (place.photos << create(:photo, position: 0)).first }
  let!(:photo_d)  { (place.photos << create(:photo, position: 1)).last }

  describe "#arrange_photo" do
    context "arrange photo of user" do
      it "with valid params" do
        ids       = [photo_a.id, photo_b.id]
        positions = [3,0]
        expect{ArrangePhotosHandler.new(user, ids, positions).arrange_photos}.to change{user.photos.first.position}.from(0).to(3)
      end

      it "with missing positions params" do
        ids       = [photo_a.id, photo_b.id]
        positions = [3]
        expect{ArrangePhotosHandler.new(user, ids, positions).arrange_photos}.to_not change(user.photos.first, :position)
      end

      it "with missing ids params" do
        ids       = [photo_a.id]
        positions = [3,0]
        expect{ArrangePhotosHandler.new(user, ids, positions).arrange_photos}.to_not change(user.photos.first, :position)
      end

      it "with duplicate position params" do
        ids       = [photo_a.id, photo_b.id]
        positions = [0,0]
        expect{ArrangePhotosHandler.new(user, ids, positions).arrange_photos}.to_not change(user.photos.first, :position)
      end

      it "with duplicate ids params" do
        ids       = [photo_a.id, photo_a.id]
        positions = [3,0]
        expect{ArrangePhotosHandler.new(user, ids, positions).arrange_photos}.to_not change(user.photos.first, :position)
      end

      it "with no 0 position" do
        ids       = [photo_a.id, photo_a.id]
        positions = [3,1]
        expect{ArrangePhotosHandler.new(user, ids, positions).arrange_photos}.to_not change(user.photos.first, :position)
      end

      it "with position greater than 4" do
        ids       = [photo_a.id, photo_a.id]
        positions = [5,0]
        expect{ArrangePhotosHandler.new(user, ids, positions).arrange_photos}.to_not change(user.photos.first, :position)
      end

    end

    context "arrange photo of place" do
      it "with valid params" do
        ids       = [photo_c.id, photo_d.id]
        positions = [3,0]
        expect{ArrangePhotosHandler.new(place, ids, positions).arrange_photos}.to change{place.photos.first.position}.from(0).to(3)
      end

      it "with missing positions params" do
        ids       = [photo_c.id, photo_d.id]
        positions = [3]
        expect{ArrangePhotosHandler.new(place, ids, positions).arrange_photos}.to_not change(place.photos.first, :position)
      end

      it "with missing ids params" do
        ids       = [photo_c.id]
        positions = [3,0]
        expect{ArrangePhotosHandler.new(place, ids, positions).arrange_photos}.to_not change(place.photos.first, :position)
      end

      it "with duplicate position params" do
        ids       = [photo_c.id, photo_d.id]
        positions = [0,0]
        expect{ArrangePhotosHandler.new(place, ids, positions).arrange_photos}.to_not change(place.photos.first, :position)
      end

      it "with duplicate ids params" do
        ids       = [photo_c.id, photo_c.id]
        positions = [3,0]
        expect{ArrangePhotosHandler.new(place, ids, positions).arrange_photos}.to_not change(place.photos.first, :position)
      end

      it "with no 0 position" do
        ids       = [photo_c.id, photo_d.id]
        positions = [3,1]
        expect{ArrangePhotosHandler.new(place, ids, positions).arrange_photos}.to_not change(place.photos.first, :position)
      end

      it "with position greater than 4" do
        ids       = [photo_c.id, photo_d.id]
        positions = [5,0]
        expect{ArrangePhotosHandler.new(place, ids, positions).arrange_photos}.to change{place.photos.first.position}.from(0).to(5)
      end
    end
  end
end
