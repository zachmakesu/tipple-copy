require "rails_helper"

RSpec.describe PlayStoreHandler do
  let(:handler) do
    VCR.use_cassette 'transactions/android-verification-auth' do
      PlayStoreHandler.instance
    end
  end
  it 'should be a singleton with .instance' do
    expect(handler).to be_a(PlayStoreHandler)
  end

  describe '#verify' do
    it 'should return false for invalid transaction' do
      receipt_data = ''
      expect(handler.verify(receipt_data)).to be_falsy
    end
    it 'should return false for consumed transaction' do
      receipt_data = File.open("#{Rails.root}/spec/files/150-drinks-consumed-android.receipt", 'rb') { |file| file.read }.squish
      VCR.use_cassette 'transactions/android-consumed-150drinks' do
        expect(handler.verify(receipt_data)).to be_falsy
      end
    end
    it 'should return receipt for unconsumed transaction' do
      receipt_data = File.open("#{Rails.root}/spec/files/150-drinks-unconsumed-android.receipt", 'rb') { |file| file.read }.squish
      VCR.use_cassette 'transactions/android-unconsumed-150drinks' do
        expect(handler.verify(receipt_data)).to be_a(CandyCheck::PlayStore::Receipt)
      end
    end
  end
end
