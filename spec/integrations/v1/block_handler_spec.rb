require "rails_helper"

describe BlockHandler do

  let!(:users) { create_list(:user,3) }
  let!(:places) { create_list(:place,3) }
  let!(:product)  { create(:product) }

  describe "#block" do

    context "with valid user" do
      it "should block a user" do
        expect{BlockHandler.new(users.first, users.last).block}.to change{users.first.blocked_profiles.count}.from(0).to(1)
      end

      it "should unblock a user" do
        BlockHandler.new(users.first, users.last).block
        expect{BlockHandler.new(users.first, users.last).unblock}.to change{users.first.blocked_profiles.count}.from(1).to(0)
      end
    end

    context "with invalid user" do
      it "should not block a user" do
        expect{BlockHandler.new(users.first, users.first).block}.not_to change{users.first.blocked_profiles.count}
      end

      it "should not unblock a user" do
        BlockHandler.new(users.first, users.last).block
        expect{BlockHandler.new(users.first, users.second).unblock}.not_to change{users.first.blocked_profiles.count}
      end
    end

    context "with a user already blocked" do
      it "should not block a user" do
        BlockHandler.new(users.first, users.last).block
        expect{BlockHandler.new(users.first, users.last).block}.not_to change{users.first.blocked_profiles.count}
      end
    end

    context "with valid place" do
      it "should block a place" do
        expect{BlockHandler.new(users.first, places.last).block}.to change{users.first.blocked_places.count}.from(0).to(1)
      end

      it "should unblock a place" do
        BlockHandler.new(users.first, places.last).block
        expect{BlockHandler.new(users.first, places.last).unblock}.to change{users.first.blocked_places.count}.from(1).to(0)
      end
    end

    context "with invalid place" do
      it "should not unblock a place" do
        BlockHandler.new(users.first, places.last).block
        expect{BlockHandler.new(users.first, places.second).unblock}.not_to change{users.first.blocked_places.count}
      end
    end

    context "with a place already blocked" do
      it "should not block a place" do
        BlockHandler.new(users.first, places.last).block
        expect{BlockHandler.new(users.first, places.last).block}.not_to change{users.first.blocked_places.count}
      end
    end
  end
end
