require "rails_helper"

describe FacebookSessionHandler do

  let(:user) do
    VCR.use_cassette 'facebook/test_user' do
      Koala::Facebook::TestUsers.new(app_id: ENV["FACEBOOK_KEY"], secret: ENV["FACEBOOK_SECRET"]).create(true)
    end
  end
  let(:access_token) { user["access_token"] }
  let(:handler) do
    VCR.use_cassette 'facebook/me_object' do
      FacebookSessionHandler.new(access_token)
    end
  end

  describe :valid_token? do
    it 'response to valid_token? method' do
      expect(FacebookSessionHandler).to respond_to(:valid_token?).with(1).argument
    end
    it 'returns falsy for invalid fb access token' do
      expect(FacebookSessionHandler.valid_token?('invalid_token')).to be_falsy
    end
  end

  describe :create do
    before { handler }

    context "with valid credentials" do

      it "returns the user that has been created" do
        auth_user = handler.create

        expect(auth_user.identities.find_by(provider: 'facebook').uid).to eq(user['id'])
        expect(auth_user.identities.find_by(uid: user['id']).provider).to eq('facebook')
      end

      it "saves a record of the created user" do
        expect { handler.create }.to change(User, :count)
      end

    end

    context "with invalid email" do
      before do
        allow(handler).to receive(:auth) { { "email" => "", "id" => "123" } }
      end

      it "generates a tmp email for user" do
        auth_user = handler.create

        expect(auth_user.email).to eq "123@facebook.com"
      end
    end
  end

  describe :find do
    it "returns the user from the provided auth" do
      handler.create
      auth_user = handler.find

      expect(auth_user.identities.find_by(provider: 'facebook').uid).to eq(user['id'])
      expect(auth_user.identities.find_by(uid: user['id']).provider).to eq('facebook')
    end

    it "create identities for user without identities but have existing account" do
      User.create(email: "#{handler.auth["id"]}@facebook.com", password: "password")

      expect{handler.find}.to change{User.last.identities.count}.from(0).to(1)
      expect(handler.find).to eq(User.last)
    end
  end

end
