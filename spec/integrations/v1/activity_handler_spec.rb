require "rails_helper"

describe ActivityHandler do

  let!(:boy)      { create(:user) }
  let!(:girl)     { create(:user) }
  let!(:place) 	  { create(:place) }
  let!(:product)  { create(:product) }
  let!(:users)    { create_list(:user,36) }

  describe "#create" do
    context "with source of check_in" do
      it "should create activity notice for the people you cheers with" do
        boy.transactions.create(product_id: product.id,transaction_id: 'transaction', receipt: {valid: true})
        girl.transactions.create(product_id: product.id,transaction_id: 'transaction2', receipt: {valid: true})
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,girl.uid,boy.uid).create_or_update
        expect{place.check_in(boy,3,-61.924169,19.379883)}.to change{girl.reload.activities.count}.by(1)
        expect{place.check_in(girl,3,-61.924169,19.379883)}.to change{boy.reload.activities.count}.by(1)
      end

      it "should not create activity if dont have poeple cheers with" do
        place.check_in(boy,3,-61.924169,19.379883)
        place.check_in(girl,3,-61.924169,19.379883)
        expect(boy.activities).to be_blank
        expect(girl.activities).to be_blank
      end
    end

    context "with source of place" do
      it "should create activity if check_ins is more than 15" do
        users.each do |user|
          place.check_in(user,3,-61.924169,19.379883)
        end

        expect(Activity.count).to eq(1)
      end

      it "should not create activity if check_ins is less than 15" do
        users.each_with_index do |user, index|
          place.check_in(user,3,-61.924169,19.379883) if index <= 13
        end

        expect(Activity.all).to be_blank
      end

      it "should create activity if check_ins is multiple by 15" do
        users.each do |user|
          place.check_in(user,3,-61.924169,19.379883)
          place.reload
          place.activities.first.update(created_at: DateTime.now - 10.hours) unless place.activities.blank?
        end
        expect(Activity.count).to eq(2)
      end
    end
  end
end
