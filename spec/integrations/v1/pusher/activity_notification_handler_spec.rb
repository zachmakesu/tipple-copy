require "rails_helper"

describe Pusher::ActivityNotificationHandler do

  let!(:sender) { create(:user, uid: '123456798') }
  let!(:recipient) { create(:user, uid: '987654321') }
  let!(:place) { create(:place) }
  let!(:product)  { create(:product) }

  describe :deliver do

    before(:each) do
      sender.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
      recipient.transactions.create(product_id: product.id, transaction_id: 'transaction2', receipt: {valid: true})
      InteractionHandler.new(place,sender.uid,recipient.uid).create_or_update
      InteractionHandler.new(place,recipient.uid,sender.uid).create_or_update
      place.check_in(sender, 1, place.lat, place.lng)
      @activity = Activity.last
    end

    it 'should call Pusher with payload' do
      channel_name = "notification-#{recipient.uid}"
      event_name = "new-activity"
      payload = {}

      expect(Pusher).to receive(:trigger).with(channel_name, event_name, payload)
      Pusher::ActivityNotificationHandler.new(@activity.id, recipient.id).deliver
    end

  end
end
