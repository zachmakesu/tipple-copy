require "rails_helper"

describe Pusher::SentMessageHandler do

  let!(:sender) { create(:user, uid: '123456798') }
  let!(:recipient) { create(:user, uid: '987654321') }
  let!(:place) { create(:place) }
  let!(:product)  { create(:product) }

  describe :deliver do

    before(:each) do
      sender.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
      recipient.transactions.create(product_id: product.id, transaction_id: 'transaction2', receipt: {valid: true})
      InteractionHandler.new(place,sender.uid,recipient.uid).create_or_update
      InteractionHandler.new(place,recipient.uid,sender.uid).create_or_update
      InteractionHandler.new(place,sender.uid,recipient.uid).create_or_update
      @interaction = InteractionHandler.new(place,recipient.uid,sender.uid).create_or_update
      @message = MessageHandler.new(@interaction.conversation.id,sender,"Hey").create
      @conversation = @interaction.conversation
    end

    it 'should call Pusher with payload' do
      channel_name = "chat-#{recipient.uid}"
      event_name = "new-message-from-#{sender.uid}"
      payload = {
        body: @message.body,
        sender: { uid: sender.uid },
        sent_at: @message.created_at.iso8601
      }

      expect(Pusher).to receive(:trigger).with(channel_name, event_name, payload)
      Pusher::SentMessageHandler.new(@conversation.id, @message.id).deliver
    end

  end
end
