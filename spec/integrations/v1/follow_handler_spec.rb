require "rails_helper"

describe FollowHandler do

  let!(:current_user) { create(:user) }
  let!(:place)        { create(:place) }

  describe "#follow" do

    context "with place and user params" do

      it "should create follower for place if place exist" do
        FollowHandler.new(current_user,place.id).follow

        expect(place.followers.first).to eq(current_user)
      end

      it "should not create follower for place that doesnt exist" do
        FollowHandler.new(current_user,200).follow

        expect(place.followers.count).to eq(0)
      end

      it "should remove user in the followers of place" do
        FollowHandler.new(current_user,place.id).follow

        expect{FollowHandler.new(current_user,place.id).unfollow}.to change{place.followers.count}.from(1).to(0)
      end
    end

  end

end
