require "rails_helper"

describe InteractionHandler do 
  let!(:boy)      { create(:user) }
  let!(:girl)     { create(:user) }
  let!(:place) 	  { create(:place) }
  let!(:gorated) 	{ create(:place, name: 'Gorated') }
  let!(:fnatic) 	{ create(:place, name: 'Fnatic') }
  let!(:liquid) 	{ create(:place, name: 'Liquid') }
  let!(:execrate)	{ create(:place, name: 'Execrate') }
  let!(:mineski)	{ create(:place, name: 'Mineski') }
  let!(:monster)	{ create(:place, name: 'Monster') }
  let!(:product)  { create(:product) }
  describe :create do 
    context "with drink credits" do
      before do
        boy.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
        girl.transactions.create(product_id: product.id, transaction_id: ' transaction2', receipt: {valid: true})
      end
      it "should save interaction after state of refuse on user who refuse the like " do
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,girl.uid,boy.uid,refuse: true).create_or_update
        expect(Interaction.last.like?).to be true
      end

      it "should save interaction with state of cheers" do
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        service = InteractionHandler.new(place,girl.uid,boy.uid).create_or_update
        expect(service.cheers?).to be true
      end

      it "should generate conversation and members if interaction is in state of cheers" do
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        interaction = InteractionHandler.new(place,girl.uid,boy.uid).create_or_update
        expect(interaction.conversation).to be_truthy
        expect(interaction.conversation.members.size).to eq(2)
      end

      it 'should decrease drinks count on drink' do
        initial_drinks_left = boy.credits_left_count
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        expect(boy.credits_left_count).to eq(initial_drinks_left - 1)
      end

      it 'should only allow sending of drink once' do
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        initial_drinks_left = boy.credits_left_count
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        expect(boy.credits_left_count).to eq(initial_drinks_left - 1)
      end

      it 'should update interactions on other places when cheers' do
        InteractionHandler.new(gorated,boy.uid,girl.uid).create_or_update
        gorated_interaction = InteractionHandler.new(gorated,boy.uid,girl.uid).create_or_update

        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,girl.uid,boy.uid).create_or_update

        expect(gorated_interaction.reload.active).to be(false)
      end

      it 'should not update existing interactions if existing cheers exists' do
        InteractionHandler.new(gorated,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(gorated,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update

        place_interaction = InteractionHandler.new(place,girl.uid,boy.uid).create_or_update
        expect(place_interaction.reload.cheers?).to be(true)

        InteractionHandler.new(gorated,girl.uid,boy.uid).create_or_update

        gorated_interaction = Interaction.where("place_id = :place AND ((initiator_id = :initiator AND initiatee_id = :initiatee) OR (initiator_id = :initiatee AND initiatee_id = :initiator))", initiator: boy.id, initiatee: girl.id, place: gorated.id).first
        expect(gorated_interaction.reload.cheers?).to be(false)
        expect(gorated_interaction.reload.drink?).to be(true)
      end
    end

    context 'without drink credits' do
      it 'should not allow like if initiator has no credits left' do
        # Since there are now 5 defaults credits, we need to use them all first
        InteractionHandler.new(fnatic, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(fnatic, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(liquid, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(liquid, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(execrate, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(execrate, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(mineski, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(mineski, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(monster, boy.uid, girl.uid).create_or_update
        InteractionHandler.new(monster, boy.uid, girl.uid).create_or_update

        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        interaction = InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        expect(interaction.like?).to be_truthy
        expect(interaction.drink?).to be_falsy
      end

      it "should save interaction with initial state of like" do
        service = InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        expect(service.like?).to be true
      end

      it "should return interaction record if sending a request in state of cheers" do
        InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        InteractionHandler.new(place,girl.uid,boy.uid).create_or_update
        service = InteractionHandler.new(place,boy.uid,girl.uid).create_or_update
        expect(service).to be_truthy
      end
    end
  end
end
