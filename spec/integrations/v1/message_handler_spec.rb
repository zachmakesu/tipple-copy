require "rails_helper"

describe MessageHandler do

  let!(:users) { create_list(:user,2) }
  let!(:place) { create(:place) }
  let!(:product)  { create(:product) }

  describe :create do

    before(:each) do
      users.first.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
      users.last.transactions.create(product_id: product.id, transaction_id: 'transaction2', receipt: {valid: true})
      InteractionHandler.new(place,users.first.uid,users.last.uid).create_or_update
      InteractionHandler.new(place,users.last.uid,users.first.uid).create_or_update
      InteractionHandler.new(place,users.first.uid,users.last.uid).create_or_update
      @interaction = InteractionHandler.new(place,users.last.uid,users.first.uid).create_or_update
      MessageHandler.new(@interaction.conversation.id,users.first,"Hey").create
      @msg = @interaction.conversation
    end

    context "message with conversation and members" do

      it "should have members of 2" do
        expect(@interaction.conversation.members.count).to eq(2)
      end

      it "recipient should recieve message from sender" do
        expect(@interaction.initiatee.conversations.first.messages.first.id).to equal(@msg.messages.first.id)
      end

    end

  end

end
