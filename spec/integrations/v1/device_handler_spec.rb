require "rails_helper"

describe DeviceHandler do

  def params
    {
      udid:     "123123",
      token:    "123123asdasd",
      platform: "ios",
      name:     "Iphone10"
    }
  end

  let!(:users) {create_list(:user,2) }

  describe ".create" do

    context "with valid params" do

      it "should save device" do 
        expect{DeviceHandler.new(params,users.first).create}.to change{Device.count}.from(0).to(1)
      end

      it "should save device once per user" do 
        DeviceHandler.new(params,users.first).create
        DeviceHandler.new(params,users.first).create

        expect(users.first.devices.count).to eq(1)
      end

      it "should save and unlink device from other user" do
        DeviceHandler.new(params,users.first).create

        expect{DeviceHandler.new(params,users.second).create}.to change{users.first.devices.count}.from(1).to(0)
        expect(users.second.devices.count).to eq(1)
      end

    end

  end

end
