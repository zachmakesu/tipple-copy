require 'rails_helper'
describe API::V1::ShareTipple do
  
  let!(:current_user) { create(:user) }
  let!(:share1) {FactoryGirl.create :product, published_at: DateTime.now - 2.hours, duration: 0, value: 5, ability: 1, description: "Like tipple on facebook"}

  before(:each) do
    http_login(current_user)
  end

  describe 'Share and like tipple' do

    it 'Return share list' do
      get "/api/v1/share_tipple", {}, @env
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json['data']['share_tipple_list'].count).to eq(1)
      expect(response).to match_response_schema("share_tipple") #object>array>object>schema
    end

    it 'updates when shared' do
      post "/api/v1/share_tipple", {id: share1.id}, @env
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json['data']['share_tipple_list'].count).to eq(1)
      expect(json['data']['share_tipple_list'][0]['is_shared']).to be true
      expect(response).to match_response_schema("share_tipple") #object>array>object>schema
    end

  end

end