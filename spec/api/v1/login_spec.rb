require 'rails_helper'
describe API::V1::Login do
  let!(:valid_token){"EAAN1ADiORNcBAK8xC5ZAg8ASyrgKrDSp2jLmoAW8CZAvRZCuEzARoHzFZC0lZAAWeBguA8uA7VpRSZCtWHSnvTRDjknxttZCJZCUyfg8NHchbDBk49Pv5hCI7S3pGFniqxu65vXlq6YDNNvDifLj9HOlfybnUHNe2OH1wKVuEdcVoQZDZD"}
  let!(:invalid_token){ 'iNvAlIdToKeN' }
  it 'should return success for valid token' do
    VCR.use_cassette 'facebook/valid_token' do
      post '/api/v1/login', { access_token: valid_token }
    end
    expect(response.status).to eq(201)
  end
  it 'should return user uid and token on successful login' do
    VCR.use_cassette 'facebook/valid_token' do
      post '/api/v1/login', { access_token: valid_token }
    end
    json = JSON.parse(response.body)
    expect(json['uid']).not_to be_nil
    expect(json['token']).not_to be_nil
  end
  it 'should return 400 for invalid token' do
    post '/api/v1/login', {access_token: invalid_token}
    expect(response.status).to eq(400)
  end
  it 'should return correct error message for invalid token' do
    post '/api/v1/login', {access_token: invalid_token}
    json = JSON.parse(response.body)
    expect(json["error"]).to eq(FacebookSessionHandler::INVALID_TOKEN_MESSAGE)
  end
  it 'should return correct error message for deleted user' do
    VCR.use_cassette 'facebook/valid_token_session_handler' do
      user = FacebookSessionHandler.new(valid_token).find_or_create_user
      user.update(deleted_at: 7.years.ago)
    end
    VCR.use_cassette 'facebook/valid_token' do
      post '/api/v1/login', {access_token: valid_token}
    end
    json = JSON.parse(response.body)
    expect(json['error']).to eq(FacebookSessionHandler::INVALID_USER_MESSAGE)
  end
end
