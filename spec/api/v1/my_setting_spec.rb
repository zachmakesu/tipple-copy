require 'rails_helper'
describe API::V1::MySettings do

  let!(:current_user) { create(:user) }
  let!(:place) { create(:place) }
  let!(:blocked_user) { create(:user, email: "blocked_user@gmail.com", first_name: "Hi", last_name: "Bae", gender: 1, uid: 123) }

  lgbt = %w{straight_male gay_male bi_male straight_female lesbian_female bi_female}
  lgbt.each_with_index do |g,i|
    let(g) { create(:user, email: "#{g}@yahoo.com", gender: (i > 2 ? 2 : 1)) }
  end

  before(:each) do
    http_login(current_user)
  end

  describe 'User Setting' do
    it 'Retrieve a user setting json' do
      get "/api/v1/my_settings", {}, @env
      expect(response).to be_success
      expect(response).to match_response_schema("my_settings")
    end

    it 'Retrieve user setting json based on methods' do
      ["prefers_men", "prefers_women", "notify_drinks", "notify_cheers", "notify_messages", "notify_activity", "hide_profile_with_zero_check_in", "hide_profile_at"].each do |t|
        post "/api/v1/my_settings/#{t}", {}, @env
        json = JSON.parse(response.body)
        expect(response).to be_success
        if (t == "hide_profile_at")
          expect(json["data"][t]).to be_truthy
        else
          expect(json["data"][t]).to be_in([true, false])
          expect(response).to match_response_schema("my_settings")
        end
      end
    end

    it 'does not retrieve any json' do 
      post "/api/v1/my_settings/any_invalid_methods", {}, @env
      expect(response).not_to be_success
    end
  end

  describe "User's blocked list" do
    it "should retrieve user's blocked list in json" do
      current_user.block_lists.create(blockable: blocked_user)
      get "/api/v1/my_settings/blacklist", {}, @env
      expect(response).to be_success
      expect(json['data'].length).to eq(1)
      expect(response).to match_response_schema("blocked_list")
    end

    context "when unblock" do
      it "should unblock a profile" do
        current_user.block_lists.create(blockable: blocked_user)
        delete "/api/v1/my_settings/blacklist/#{blocked_user.uid}", {}, @env
        expect(response).to be_success
        expect(json['data']).to be_empty
      end
    end

    context "when block" do
      it "should block a profile" do 
        post "/api/v1/my_settings/blacklist/#{blocked_user.uid}", {}, @env
        expect(json['data'].length).to eq(1)
        expect(response).to match_response_schema("blocked_list")
      end

      it "should not block if current_user profile uid" do 
        post "/api/v1/my_settings/blacklist/#{current_user.uid}", {}, @env
        expect(response.status).to eq(400)
      end
    end
  end

end
