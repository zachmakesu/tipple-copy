require 'rails_helper'
describe API::V1::Places do
  let!(:current_user) { create(:user) }
  let!(:boy) { create(:user) }
  let!(:girl) { create(:user) }
  let!(:place) { create(:place) }
  let!(:gorated) { create(:place, name: 'Gorated') }

  before(:each) do
    http_login(current_user)
    current_user.check_ins.create(place_id: place.id, random_likes: 3)
    girl.check_ins.create(place_id: place.id, random_likes: 3)
  end

  context 'user initiates drink interaction with you on a place' do
    before do
      InteractionHandler.new(place,girl.uid,current_user.uid).create_or_update
      InteractionHandler.new(place,girl.uid,current_user.uid).create_or_update
    end
    it 'returns a single liker for said place' do
      get '/api/v1/likers', {}, @env
      expect(response).to be_success
      expect(json['data'].length).to eq(1)
      expect(response).to match_response_schema("likers")
    end
    it 'returns unique liker after resending drink' do
      InteractionHandler.new(place,current_user.uid,girl.uid, refuse: true).create_or_update
      InteractionHandler.new(place,girl.uid,current_user.uid).create_or_update
      get '/api/v1/likers', {}, @env
      expect(response).to be_success
      expect(json['data'].length).to eq(1)
      expect(response).to match_response_schema("likers")
    end
    context 'user initiates drink interaction on another place' do
      before do
        InteractionHandler.new(gorated,girl.uid,current_user.uid).create_or_update
        InteractionHandler.new(gorated,girl.uid,current_user.uid).create_or_update
      end
      it 'returns multiple likers' do
        get '/api/v1/likers', {}, @env
        expect(response).to be_success
        expect(json['data'].length).to eq(2)
        expect(response).to match_response_schema("likers")
      end
      it 'returns empty likers if you had cheers' do
        InteractionHandler.new(place,current_user.uid,girl.uid).create_or_update
        get '/api/v1/likers', {}, @env
        expect(response).to be_success
        expect(json['data'].length).to eq(0)
        expect(response).to match_response_schema("likers")
      end
    end
  end

  context 'you had a cheers interaction with another user' do
    before do
      InteractionHandler.new(place,current_user.uid,girl.uid).create_or_update
      InteractionHandler.new(place,girl.uid,current_user.uid).create_or_update 
    end
    it 'should not return liker even if you do not send a message yet' do

      get '/api/v1/likers', {}, @env
      expect(response).to be_success
      expect(json['data'].length).to eq(0)
    end

    it 'should not return liker if you sent a message' do
      current_user.conversations.first.messages.create(sender_id: current_user.id, body: "my message to girl")
      get '/api/v1/likers', {}, @env
      expect(response).to be_success
      expect(json['data'].length).to eq(0)
    end
  end
end

