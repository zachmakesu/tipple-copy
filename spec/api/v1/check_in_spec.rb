require 'rails_helper'

describe API::V1::CheckIns do

  let!(:place)        { create(:place, {name: "Go Rated Ninjas", lat: 14.6030568, lng: 121.0917855 } ) }
  let!(:current_user) { create(:user) }

  before(:each) do
    http_login(current_user)
  end

  context "if params valid" do
    it 'check in to place' do
      post "/api/v1/places/#{place.id}/check_in", { random_likes: 2, lat: 14.6030568, lng: 121.091870 }, @env
      expect(response).to be_success
    end
  end

  context "if params missing" do
    it 'does not check in to place' do
      post "/api/v1/places/#{place.id}/check_in", { random_likes: 2, lng: 121.091870 }, @env
      json = JSON.parse(response.body)
      expect(json['error']).to match("Cannot check-in to place")
    end
  end

  context "if random_likes is greater than 10" do
    it 'does not check in to place' do
      post "/api/v1/places/#{place.id}/check_in", { random_likes: 20, lat: 14.6030568, lng: 121.091870 }, @env
      json = JSON.parse(response.body)
      expect(json['error']).to match("Cannot check-in to place")
    end
  end

  context "if shared_likes is > 10" do
    it 'does not earn shared_likes' do
      post "/api/v1/places/#{place.id}/shared", { shared_likes: 20 }, @env
      json = JSON.parse(response.body)
      expect(json['error']).to match("Cannot share check-in on Facebook")
    end
  end

  context "if shared_likes is <= 10" do
    it 'does earn shared_likes' do
      place.check_in(current_user, 1, 14.6030568, 121.091870)
      post "/api/v1/places/#{place.id}/shared", { shared_likes: 10 }, @env
      expect(response).to be_success
    end

    it 'does not allow to spam share' do
      place.check_in(current_user, 2, 14.6030568, 121.091870)
      post "/api/v1/places/#{place.id}/shared", { shared_likes: 10 }, @env
      expect(response).to be_success

      post "/api/v1/places/#{place.id}/shared", { shared_likes: 10 }, @env
      json = JSON.parse(response.body)
      expect(json['error']).to match("Cannot share check-in on Facebook")
    end
  end
end
