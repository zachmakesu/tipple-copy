require 'rails_helper'

describe API::V1::Conversations do

  let!(:current_user) { create(:user) }
  let!(:recipient) { create(:user, first_name: 'Recipient') }
  let!(:product)  { create(:product) }
  let!(:place)        { create(:place, {name: "Go Rated Ninjas", lat: 14.6030568, lng: 121.0917855 } ) }
  let!(:valid_params) { { body: 'Message!' } }

  before(:each) do
    http_login(current_user)
    current_user.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
    recipient.transactions.create(product_id: product.id, transaction_id: 'transaction2', receipt: {valid:true})
    InteractionHandler.new(place,current_user.uid,recipient.uid).create_or_update
    @interaction =  InteractionHandler.new(place,recipient.uid,current_user.uid).create_or_update
    MessageHandler.new(@interaction.conversation.id,current_user,"Hey").create
    @conversation = @interaction.conversation
  end


  it 'returns users conversations' do
    get "/api/v1/messages", nil, @env
    expect(response).to be_success
  end

  it 'returns users conversations\'s messages' do
    get "/api/v1/messages/#{@conversation.id}", nil, @env
    expect(response).to be_success
  end

  context 'valid params' do
    it 'should be successful' do
      post "/api/v1/messages/#{@conversation.id}", valid_params, @env
      expect(response).to be_success
    end
    it 'should create new message record' do
      post "/api/v1/messages/#{@conversation.id}", valid_params, @env
      expect(@conversation.messages.count).to eq(2)
    end
  end

  context 'invalid params' do
    it 'should not be successful' do
      post "/api/v1/messages/#{@conversation.id}", nil, @env
      expect(response).not_to be_success
    end
    it 'should not create new message record' do
      post "/api/v1/messages/#{@conversation.id}", nil, @env
      expect(@conversation.messages.count).to eq(1)
    end
  end

end
