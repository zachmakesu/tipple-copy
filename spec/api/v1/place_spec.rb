require 'rails_helper'
describe API::V1::Places do
  let!(:current_user) { create(:user , gender: 1, uid: 1) }
  let!(:user_a) { create(:user , gender: 2, uid: 2) }
  let!(:user_b) { create(:user , gender: 2, uid: 3) }
  let!(:user_c) { create(:user , gender: 2, uid: 4) }
  let!(:user_d) { create(:user , gender: 1, uid: 5) }

  let!(:place)        { create(:place, {name: "Go Rated Ninjas", address: "Unit 20, Ipark Center,401 Eulogio Amang Rodriguez Ave,Pasig,1611 Metro Manila, Pilipinas", lat: 14.6030568, lng: 121.0917855 } ) }
  let!(:place_b)      { create(:place, {name: "Chooks to Go", address: "403 Eulogio Amang Rodriguez Ave,Mangahan, Pasig,1611 Metro Manila,Pilipinas", lat: 14.6027872, lng: 121.0920404 } ) }
  let!(:place_c)      { create(:place, {name: "Far Far Away Land", address: "10Da-39 Crater Street East Cavern Jupiter", lat: 14.6027872, lng: 121.1990404 } ) }

  before(:each) do
    http_login(current_user)
  end

  describe 'Places' do
    it 'returns a list of places near current_user position' do
      get '/api/v1/places?lat=14.6030568&lng=121.0917855', {}, @env
      expect(response).to be_success
      expect(json['data'].length).to eq(3) 
      expect(response).to match_response_schema("places")
    end

    it 'Returns a list of places match in the search' do
      get "/api/v1/places/search?query=Go", {}, @env
      expect(response).to be_success
      expect(json['data'].length).to eq(2)
      expect(response).to match_response_schema("places")
    end

    context "if search did not match any place name" do
      it 'returns an empty list of places' do
        get "/api/v1/places/search?query=randomstringthatdidnotmatchanyplacename", {}, @env
        expect(response).to be_success
        expect(json['data']).to be_empty
      end
    end

    context "if search is empty string" do
      it 'returns a list of places' do
        get "/api/v1/places/search?query=", {}, @env
        expect(response).to be_success
        expect(json['data'].length).to eq(3) 
        expect(response).to match_response_schema("places")
      end
    end

    context "followers count" do
      it 'should decrease based on blocked_profiles' do
        place.follow_by(current_user)
        place.follow_by(user_a)
        place.follow_by(user_b)
        place.follow_by(user_c)

        current_user.block_lists.create(blockable: user_a) #current_user blocked user_a
        user_c.block_lists.create(blockable: current_user) #user_c blocked current_user

        get '/api/v1/places?lat=14.6030568&lng=121.0917855', {}, @env
        expect(response).to be_success 
        expect(response).to match_response_schema("places")
        expect(json['data'][0]['followers_count']['female']).to eq(1)
      end
    end

    context "currently check ins and total check_ins count" do
      it 'should decrease based on blocked_profiles' do
        place.check_in(current_user, 3 , 14.6030568, 121.0917855)
        place.check_in(user_a, 3 , 14.6030568, 121.0917855)
        place.check_in(user_b, 3 , 14.6030568, 121.0917855)
        place.check_in(user_c, 3 , 14.6030568, 121.0917855)

        current_user.block_lists.create(blockable: user_a) #current_user blocked user_a
        user_c.block_lists.create(blockable: current_user) #user_c blocked current_user

        get '/api/v1/places?lat=14.6030568&lng=121.0917855', {}, @env
        expect(response).to be_success 
        expect(json['data'][0]['followers_count']['female']).to eq(1)
        expect(json['data'][0]['available_check_ins']).to eq(1)
        expect(json['data'][0]['check_ins']).to eq(1)
      end
    end
  end

  describe 'Specific place' do
    it 'Retrieve a specific place' do
      get "/api/v1/places/#{place.id}", {}, @env
      expect(response).to be_success
      expect(json["data"]["id"]).to be(place.id)
      expect(response).to match_response_schema("place")
    end

    context "followers_count" do
      it 'should decrease based on blocked_profiles' do
        place.follow_by(current_user)
        place.follow_by(user_a)
        place.follow_by(user_b)
        place.follow_by(user_c)

        current_user.block_lists.create(blockable: user_a) #current_user blocked user_a
        user_c.block_lists.create(blockable: current_user) #user_c blocked current_user
        get "/api/v1/places/#{place.id}", {}, @env
        expect(response).to be_success 
        expect(response).to match_response_schema("place")
        expect(json['data']['followers_count']['female']).to eq(1)
      end
    end

    context "available_check_ins and total_check_ins" do
      it 'should decrease based on blocked_profiles' do
        place.check_in(current_user, 3 , 14.6030568, 121.0917855)
        place.check_in(user_a, 3 , 14.6030568, 121.0917855)
        place.check_in(user_b, 3 , 14.6030568, 121.0917855)
        place.check_in(user_c, 3 , 14.6030568, 121.0917855)

        current_user.block_lists.create(blockable: user_a) #current_user blocked user_a
        user_c.block_lists.create(blockable: current_user) #user_c blocked current_user
        get "/api/v1/places/#{place.id}", {}, @env
        expect(response).to be_success 
        expect(json['data']['available_check_ins']).to eq(1)
        expect(json['data']['total_check_ins']).to eq(1)
      end
    end

    context 'follower profile' do
      it "should be retrieve if not blocked" do 
        place.follow_by(current_user)
        place.follow_by(user_a)
        get "/api/v1/places/#{place.id}/profile/#{user_a.uid}", {}, @env
        expect(response).to be_success
        expect(json["data"]["place_id"]).to be(place.id)
        expect(response).to match_response_schema("profile")
      end

      it "should not be retrieve if blocked by you" do 
        place.follow_by(current_user)
        place.follow_by(user_a)
        current_user.block_lists.create(blockable: user_a) #current_user blocked user_a
        get "/api/v1/places/#{place.id}/profile/#{user_a.uid}", {}, @env
        expect(json['error']).to match("This profile is currently blocked by you")
      end

      it "should not be retrieve if blocked by you" do 
        place.follow_by(current_user)
        place.follow_by(user_c)
        user_c.block_lists.create(blockable: current_user) #user_c blocked current_user
        get "/api/v1/places/#{place.id}/profile/#{user_c.uid}", {}, @env
        expect(json['error']).to match("This profile is currently blocked you")
      end

      it "should not be retrieve if not yet a follower" do 
        get "/api/v1/places/#{place.id}/profile/#{user_b.uid}", {}, @env
        expect(json['error']).to match("This profile is not yet a follower of this place")
      end

      it "should not be retrieve if not in your preferences" do 
        place.follow_by(user_d)
        get "/api/v1/places/#{place.id}/profile/#{user_d.uid}", {}, @env
        expect(json['error']).to match("This profile is not in your preferences")
      end
    end
  end

  describe "My Places" do
    it "shows current user's followed places" do #follow
      place.follows.build(user: current_user).save
      place_b.follows.build(user: current_user).save
      get "/api/v1/my_places", {}, @env
      expect(response).to be_success
      expect(json['data'].length).to eq(2)
      expect(response).to match_response_schema("my_places")
    end

    it 'Add to my places' do #follow
      post "/api/v1/places/#{place.id}/follows", {}, @env
      expect(response).to be_success
      expect(current_user.place_followed).to include(place)
      expect(response).to match_response_schema("place")
    end

    it 'delete to my places' do #unfollow
      place.follow_by(current_user)
      delete "/api/v1/places/#{place.id}/follows", {}, @env
      expect(response).to be_success
      expect(current_user.place_followed).not_to include(place)
    end

    it 'disable notification' do
      place.follow_by(current_user)
      post "/api/v1/my_places/#{place.id}/unnotify", {}, @env
      expect(response).to be_success
      expect(json['data']['unnotify']).to be true
    end

    it 'enable notification' do
      place.follow_by(current_user)
      current_user.block_lists.create(blockable: place)
      delete "/api/v1/my_places/#{place.id}/unnotify", {}, @env
      expect(response).to be_success
      expect(json['data']['unnotify']).to be false
    end
  end
end
