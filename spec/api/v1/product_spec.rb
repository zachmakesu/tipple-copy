require 'rails_helper'
describe API::V1::Products do

  let!(:current_user) { create(:user) }
  let!(:shareable_product) {FactoryGirl.create :product, published_at: DateTime.now - 2.hours, duration: 0, value: 5, ability: 1, description: "Like tipple on facebook"}
  let!(:purchaseable_product) {FactoryGirl.create :product, published_at: DateTime.now - 8.hours, duration: 0, value: 10, price: 1.99, ability: 0, name: "Baby Tippler"}
  let!(:unli_product) {FactoryGirl.create :product, published_at: DateTime.now - 8.hours, duration: 3, value: 0, price: 15.99, ability: 0, name: "Tipple Addict"}

  before(:each) do
    http_login(current_user)
  end

  describe 'Products' do

    it 'Return share list' do
      get "/api/v1/share_tipple", {}, @env
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json['data']['share_tipple_list'].count).to eq(1)
      expect(response).to match_response_schema("share_tipple") #object>array>object>schema
    end

    it 'Return product list' do
      get "/api/v1/products", {}, @env
      expect(response).to be_success
      expect(json['data']['tipple_products'].count).to eq(2)
      expect(response).to match_response_schema("products") #object>array>object>schema
    end

    context 'transactions with invalid params' do
      it 'returns 400 if no platform is provided' do
        post '/api/v1/products', {}, @env
        expect(response.status).to eq(400)
      end
      it 'returns correct error message if no platform is provided' do
        post '/api/v1/products', {}, @env
        json = JSON.parse(response.body)
        expect(json['error']).to eq(TransactionHandler::INVALID_PLATFORM_MESSAGE)
      end
      it 'returns correct error message if product is invalid' do
        post '/api/v1/products', {platform: 'ios', product_id: shareable_product.id}, @env
        json = JSON.parse(response.body)
        expect(json['error']).to eq(TransactionHandler::INVALID_PRODUCT_MESSAGE)
      end

      context 'for android platform' do
        before do
          VCR.use_cassette 'transactions/android-verification-auth' do
            PlayStoreHandler.instance
          end
        end
        it 'returns successfully with valid params' do
          receipt_data = File.open("#{Rails.root}/spec/files/150-drinks-consumed-android.receipt", 'rb') { |file| file.read }.squish
          VCR.use_cassette 'transactions/android-consumed-150drinks' do
            post '/api/v1/products', {platform: 'android', product_id: purchaseable_product.id, receipt_data: receipt_data}, @env
          end
          json = JSON.parse(response.body)
          expect(json['error']).to eq(TransactionHandler::INVALID_RECEIPT_MESSAGE)
        end
      end

      context 'for ios platform' do
        it 'returns correct error message if receipt is invalid' do
          post '/api/v1/products', {platform: 'ios', product_id: purchaseable_product.id}, @env
          json = JSON.parse(response.body)
          expect(json['error']).to eq(TransactionHandler::INVALID_RECEIPT_MESSAGE)
        end
      end
    end

    context 'transactions with valid params' do
      context 'for android platform' do
        before do
          VCR.use_cassette 'transactions/android-verification-auth' do
            PlayStoreHandler.instance
          end
        end
        it 'returns successfully with valid params' do
          receipt_data = File.open("#{Rails.root}/spec/files/150-drinks-unconsumed-android.receipt", 'rb') { |file| file.read }.squish
          VCR.use_cassette 'transactions/android-unconsumed-150drinks' do
            post '/api/v1/products', {platform: 'android', product_id: purchaseable_product.id, receipt_data: receipt_data}, @env
          end
          expect(response).to be_successful
        end
        it 'increase credits count of user' do
          receipt_data = File.open("#{Rails.root}/spec/files/150-drinks-unconsumed-android.receipt", 'rb') { |file| file.read }.squish

          VCR.use_cassette 'transactions/android-unconsumed-150drinks' do
            post '/api/v1/products', {platform: 'android', product_id: purchaseable_product.id, receipt_data: receipt_data}, @env
          end
          expect(response).to be_successful
          expect(current_user.credits_left_count).to eq(purchaseable_product.value + User::DEFAULT_DRINK_CREDITS)
        end
      end
      context 'for ios platform' do
        it 'returns successfully with valid params' do
          receipt_data = File.open("#{Rails.root}/spec/files/10-drinks.receipt", 'rb') { |file| file.read }.squish

          VCR.use_cassette 'transactions/ios-10-drinks' do
            post '/api/v1/products', {platform: 'ios', product_id: purchaseable_product.id, receipt_data: receipt_data}, @env
          end
          expect(response).to be_successful
        end
        it 'increase credits count of user' do
          receipt_data = File.open("#{Rails.root}/spec/files/10-drinks.receipt", 'rb') { |file| file.read }.squish

          VCR.use_cassette 'transactions/ios-10-drinks' do
            post '/api/v1/products', {platform: 'ios', product_id: purchaseable_product.id, receipt_data: receipt_data}, @env
          end
          expect(response).to be_successful
          expect(current_user.credits_left_count).to eq(purchaseable_product.value + User::DEFAULT_DRINK_CREDITS)
        end
        it 'reflects unlimited credits' do
          receipt_data = File.open("#{Rails.root}/spec/files/unli-drinks.receipt", 'rb') { |file| file.read }.squish
          VCR.use_cassette 'transactions/ios-unli-drinks' do
            post '/api/v1/products', {platform: 'ios', product_id: unli_product.id, receipt_data: receipt_data}, @env
            expect(response).to be_successful
            expect(current_user.credits_left).to eq('Unlimited')
          end
        end
      end
    end
  end
end
