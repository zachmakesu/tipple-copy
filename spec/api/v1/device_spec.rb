require "rails_helper"

describe API::V1::Devices do

  let!(:current_user) { create(:user) }

  before(:each) do
    http_login(current_user)
    post "/api/v1/devices", { udid: "123456789", token: "1a2s3df3", platform: "ios", name: "iphone10"}, @env
  end


  context "Valid params" do
    it "response to success" do
      expect(response).to be_success
    end

    it 'also multiple save response to be successful  on next save attempt' do
      post "/api/v1/devices", { udid: "123456789", token: "1a2s3df3", platform: "ios", name: "iphone10"}, @env

      expect(response).to be_success
    end
  end

end
