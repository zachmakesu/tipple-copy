require 'rails_helper'

describe API::V1::Interactions do
  let!(:current_user) { create(:user) }
  let!(:girl)     { create(:user) }
  let!(:gorated) 	{ create(:place, name: 'Gorated') }
  let!(:fnatic) 	{ create(:place, name: 'Fnatic') }
  let!(:liquid) 	{ create(:place, name: 'Liquid') }
  let!(:execrate)	{ create(:place, name: 'Execrate') }
  let!(:mineski)	{ create(:place, name: 'Mineski') }
  let!(:monster)	{ create(:place, name: 'Monster') }
  let!(:product)  { create(:product) }

  before(:each) do
    http_login(current_user)
  end

  context "with drink credits" do
    before do
      current_user.transactions.create(product_id: product.id, transaction_id: 'transaction', receipt: {valid: true})
      girl.transactions.create(product_id: product.id, transaction_id: 'transaction2', receipt: {valid: true})
      InteractionHandler.new(gorated, current_user.uid, girl.uid).create_or_update
    end
    it 'be successful with valid params' do
      post "/api/v1/interaction", { place: gorated.id, initiatee: girl.uid }, @env
      expect(response).to be_success
    end
    it 'be unsuccessful with invalid params' do
      post "/api/v1/interaction", { place: gorated.id, initiatee: current_user.uid }, @env
      expect(response).to_not be_success
    end
  end

  context "without drink credits" do
    before do
      # Since there are now 5 defaults credits, we need to use them all first
      InteractionHandler.new(fnatic, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(fnatic, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(liquid, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(liquid, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(execrate, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(execrate, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(mineski, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(mineski, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(monster, current_user.uid, girl.uid).create_or_update
      InteractionHandler.new(monster, current_user.uid, girl.uid).create_or_update

      InteractionHandler.new(gorated, current_user.uid, girl.uid).create_or_update
    end
    it 'should not be successful' do
      post "/api/v1/interaction", { place: gorated.id, initiatee: girl.uid }, @env
      expect(response).to_not be_success
    end
    it 'should return correct error message' do
      post "/api/v1/interaction", { place: gorated.id, initiatee: girl.uid }, @env
      data = JSON.parse(response.body)
      expect(data['error']).to eq(InteractionHandler::NO_CREDITS_ERROR_MESSAGE)
    end
  end
end
