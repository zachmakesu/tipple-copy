require 'rails_helper'
describe API::V1::Accounts do
  let(:current_user) { create(:user) }
  let(:ig_endpoint) { '/api/v1/accounts/instagram' }
  let(:valid_params){ {uid: '123456', provider: 'instagram', token: 'MuhToken'} }

  before(:each) do
    http_login(current_user)
  end

  context 'invalid params' do
    it 'should not accept missing params' do
      post ig_endpoint, {}, @env
      expect(response.status).to eq(400)
    end
  end

  context 'valid params' do
    before do
      post ig_endpoint, valid_params, @env
    end
    it 'should have successful response' do
      expect(response).to be_successful
    end
    it 'should correctly assign identity' do
      expect(current_user.identities.where(valid_params).first).not_to be_nil
    end
  end
end
