require 'rails_helper'

describe API::V1::ProfilePhotos do
  let!(:current_user) { create(:user) }
  let!(:photo_a)  { (current_user.photos << create(:photo, position: 0)).first }
  let!(:photo_b)  { (current_user.photos << create(:photo, position: 1)).last }

  before(:each) do
    http_login(current_user)
  end

  describe 'Profile photos' do
    it 'Show all profile photos' do
      get "/api/v1/profile_photos", {}, @env
      expect(response).to be_success
      expect(json['data']['photos'].length).to eq(2)
      expect(response).to match_response_schema("profile_photos")
    end

    context "Valid params" do
      it 'adds a photo' do
        post "/api/v1/profile_photos", { image: "http://i.imgur.com/uZDmIe8.png"  ,position: 2}, @env
        expect(response).to be_success
        expect(json['data']['photos'].length).to eq(3)
        expect(response).to match_response_schema("profile_photos")
      end

      it 'edit the photos position' do
        post "/api/v1/profile_photos/arrange_photos", { id: [photo_b.id,photo_a.id], position: [0,1] }, @env
        expect(response).to be_success
        expect(json['data']['photos'][0]['id']).to eq(photo_b.id)
        expect(response).to match_response_schema("profile_photos")
      end
    end

    context "Invalid params" do
      it 'does not adds photo' do
        post "/api/v1/profile_photos", { image: "https://wordpress.org/plugins/about/readme.txt",position: 2}, @env
        expect(json['error']).to match("Image content type not allowed., Image not allowed.")
      end

      it 'does not upate if position.count != ids.count ' do
        post "/api/v1/profile_photos/arrange_photos", { id: [photo_b.id,photo_a.id], position: [0] }, @env
        expect(json['error']).to match("Invalid given params")
      end

      it 'does not update if ids.count != position.count' do
        post "/api/v1/profile_photos/arrange_photos", { id: [photo_b.id], position: [1,0] }, @env
        expect(json['error']).to match("Invalid given params")
      end

      it 'does not update if positions has duplicates' do
        post "/api/v1/profile_photos/arrange_photos", { id: [photo_b.id,photo_a.id], position: [0,0] }, @env
        expect(json['error']).to match("Invalid given params")
      end

      it 'does not update if some of positions is greater than 4' do
        post "/api/v1/profile_photos/arrange_photos", { id: [photo_b.id,photo_a.id], position: [0,5] }, @env
        expect(json['error']).to match("Invalid given params")
      end
    end

    it 'Deletes a photo' do
      delete "/api/v1/profile_photos", { id: photo_a.id }, @env
      expect(response).to be_success
      expect(json['data']['photos'].count).to eq(1)
    end

  end
end
