FactoryGirl.define do
	factory :place do
		name "london Cafe"
		address "London"
		lat -61.924169
		lng 19.379883
	end
end