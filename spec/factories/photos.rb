# == Schema Information
#
# Table name: photos
#
#  caption            :string
#  created_at         :datetime         not null
#  default            :boolean          default(FALSE)
#  default_type       :integer
#  id                 :integer          not null, primary key
#  image_content_type :string
#  image_file_name    :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  imageable_id       :integer
#  imageable_type     :string
#  position           :integer
#  updated_at         :datetime         not null
#

FactoryGirl.define do
  factory :photo do
    image_file_name { 'test.jpg' }
    image_content_type { 'image/jpg' }
    image_file_size { 8 }
    position 0
  end
end
