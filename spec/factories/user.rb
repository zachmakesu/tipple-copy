FactoryGirl.define do
  sequence :email do |n|
    "email#{n}@factory.com"
  end
  sequence :uid do |n|
    "#{n}#{SecureRandom.hex(16)}"
  end
  factory :user do
    email 
    password "password123"
    first_name "Sample"
    last_name "Sample"
    gender "male"
    role "admin"
    uid
  end
end
