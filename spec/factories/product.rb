FactoryGirl.define do
  factory :product do
    ability "can_be_purchase"
    duration "not_applicable"
    name "Baby Tippler"
    price 1.99
    value 10
  end
  factory :unlimited_product, class: Product do
    ability "can_be_purchase"
    published_at { DateTime.now - 11.hours }
    duration 3
    value 0
    price 15.99
  end
end
